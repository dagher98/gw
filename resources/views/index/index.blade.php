<!doctype html>
<html lang="en">
<head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/vnd.microsoft.icon" href="{{asset('img/cardiology.png')}}">
  <title>Global Wellness</title>
   <link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
  <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/style.css')}}">
  <link href="fonts/stylesheet.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="fonts/icon/flaticon.css">
  <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
</head>
<body>

  <div class="d-flex flex-column flex-md-row navbar navbar-expand-lg align-items-center py-2 bg-white mb-0 ">
    <h5 class="my-0 mr-md-auto font-weight-normal pl-xl-5"><img src="{{asset('img/GlobalWellnesslogo.svg')}}" class="img-fluid nav-logo"/></h5>

    <nav class="my-1 my-md-0 mr-xl-3  nav-font">
      <div class="collapse navbar-collapse" id="navbarsExample03">
        <div class=" navbar-nav bd-navbar-nav ">
          <ul class="navbar-nav mr-auto">
          <a class="p-2 nav-item text-dark active" href="/">Home</a>
          <a class="p-2 nav-item text-dark" href="/patient">Patient</a>
          <a class="p-2 nav-item text-dark" href="/doctor/">Doctors</a>
          <a class="p-2 nav-item text-dark" href="/hospital/">Hospitals</a>
          <a class="p-2 nav-item text-dark" href="#">Pharmacies</a>
          <a class="p-2 nav-item text-dark" href="/tourism/">Tourism</a>
          <a class="p-2 nav-item text-dark pr-xl-5" href="#">Plan a Procedure</a>
        </ul>
      </div>
    </div>
    </nav>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample03" aria-controls="navbarsExample03" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
      @guest
    <a class="btn pr-xl-5" href="{{ route('login') }}"><img src='{{asset('img/user.png')}}' width="30"/></a>
      @else
       <a class="btn pr-xl-5" href="{{ route('logout') }}"onclick="event.preventDefault();document.getElementById('logout-form').submit();"><img src='{{asset('img/useractivate-black.png')}}' width="30"/></a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
                                
       @endguest
  </div>

  <div id="fadoct" style="background : url({{asset('img/banner.png')}}) ;background-position: center;background-size: cover; ">
    <div class=" text-white py-5">
      <div class="py-5 my-5">
        <div class="col-xl-12 py-5 my-5">
          <div class="py-5">
            <div class="row">
              <div class="col-lg-8 col-8 offset-lg-2 offset-2 p-0 ">
                <p class="Fdoc-title text-center">FIND A DOCTOR!</p>
                <div class="input-group mb-3">
                  <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
                  <div class="input-group-append">
                    <button class="btn btn-search px-4" type="button">Search</button>
                  </div>
                </div>
              </div>
              <div class="col-lg-2  col-2">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
   <section class="p-0 my-5">
    <div class="container">
      <div class="row my-5">
        <div class="col-xl-2">
        </div>
        <div class="col-xl-8">
          <div class="row">
            <div class="col-12">
              <p class="text-center doc-title my-3">FIND BY SPECIALIZATION</p>
            </div>
            <div class="col-12">
              <p class="doc-subtitle text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
            </div>
          </div>
        </div>
        <div class="col-xl-2">
        </div>
      </div>
    </div>
    <div class="container-fluid pl-5 pr-5">
      <div class="row">
         @foreach($postspeciality as $key => $data)
        <div class="col-xl-3 py-3">
          <div class=" grey-Back  text-center ">
            <div class="py-5">
              <div style="height:60px">
                <img src="{{asset('storage/images/'.$data->spec_img.'')}}" width="50" />
              </div>
              <span class="Specia-title">{{$data->spec_name}}</span>
            </div>
            <div class="row">
              <div class="col-6 border-white-right">
                <div class="grey-Back ">
                  <span class="Specia-Subtitle">{{$postcountdoc[$key]}} Doctors</span>
                </div>
              </div>
              <div class="col-6 border-white-left">
                <div class="grey-Back ">
                  <span class="Specia-Subtitle">60 Clinics</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endforeach
        
      </div>
    </div>
  </section>
 
  <section id="Hospitals" class="p-0">
    <div class="container py-4">
      <div class="row my-5">
        <div class="col-xl-2">
        </div>
        <div class="col-xl-8">
          <div class="row">
            <div class="col-xl-12">
              <p class="text-center doc-title my-3">HOSPITALS</p>
            </div>
            <div class="col-xl-12">
              <p class="doc-subtitle text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
            </div>
          </div>
        </div>
        <div class="col-xl-2">
        </div>
      </div>
    </div>
    <div class="container-fluid p-0 py-3">
      <div id="maincarousel" class="owl-carousel">
          @foreach($posthos as $key => $data)
        <div class="item ">
          <div class="row my-5">
            <div class="col-xl-12 ">
              <div class="Rounded_Rectangle_hospitals" style="display: -webkit-box">
                <div class="p-xl-5 p-4">
                  <h6 class="card-title m-0">{{$data->hos_city}}</h6>
                  <div class="card-hos-text ">{{$data->hos_name}}</div>
                  <div class="btn BorderDoc"> </div>
                  <div class="row">
                    <div class="col-xl-12 card-title ">
                      <i class="ion-ios-clock-outline "></i> <span class="pl-2 doc-subtitle">{{$data->hos_workhour}}</span>
                      <br>
                      <i class="ion-ios-telephone-outline"></i> <span class="pl-2 doc-subtitle">{{$data->hos_phone}}</span>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6 pr-xl-4 pl-md-0 px-0 ">
                      <div class=" row mx-1 py-2" style="display: -webkit-box">
                        <div class="col-4 col-sm-6 px-2  px-md-3 px-lg-2">
                            <i class="flaticon-hospital icon-lg"></i>
                        </div>
                        <div class="col-8 col-sm-6 px-0 py-2">
                          <div class=" m-0 p-0 text-center hosp-text-lg"><strong>{{$data->hos_room_count}}</strong></div>
                          <div class=" m-0 p-0 text-center hosp-text">ROOMS</div>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6 border-left-hos pr-md-3 pl-md-0 px-0">
                      <div class="row mx-1 py-2" style="display: -webkit-box">
                        <div class="col-sm-6 col-4 px-2  px-lg-2">
                            <i class="flaticon-doctor icon-lg"></i>
                        </div>
                        <div class="col-sm-6 col-8 px-xl-3 px-0 py-2">
                          <div class=" m-0 p-0 text-center hosp-text-lg"><strong>{{$data->hos_doc_count}}</strong></div>
                          <div class=" m-0 p-0 text-center hosp-text">DOCTORS</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="button-hover">
                <a href="hospital/{{$data->hos_id}}">
                <img src="{{asset('img/roundbutton.png')}}" />
                </a>
              </div>
            </div>
          </div>
        </div>
        @endforeach
      
  </section>
  <section class="p-0 mr-xl-5">
    <div class="container">
      <div class="row my-5">
        <div class="col-xl-2">
        </div>
        <div class="col-xl-8">
          <div class="row">
            <div class="col-xl-12">
              <p class="text-center doc-title my-3">DOCTORS </p>
            </div>
            <div class="col-xl-12">
              <p class=" text-center doc-subtitle">Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
            </div>
          </div>
        </div>
        <div class="col-xl-2">
        </div>
      </div>
    </div>
    <div class="container-fluid m-xl-5 px-5">
      <div class="row my-5">
         @foreach($postdoc as $key => $data)
        <div class="col-xl-3 col-lg-4 col-md-5 ">
          <div class="Rounded_Rectangle ">
            <div class="frontside ">
              <div class="card ">
                <div class="card-body text-center py-4">
                  <p class="pb-3 pt-0">
                    <img class="img-fluid" src="{{asset('storage/images/'.$data->doc_img.'')}}" alt="card image">
                  </p>
                  <h4 class="card-title py-0 my-0">{{$data->doc_fullname}}</h4>
                  <div class="card-text ">{{$data->spec_name}}</div>
                  <div class="btn BorderDoc"> </div>
                  <br>
                  <a href="doctor/{{$data->doc_id}}" class="btn btn-read-hos btn-sm"><span class="btn-text">Read More<span></a>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endforeach
      
      </div>
      <div class="col-xl-12 ">
        <p class="text-center ">
          <a href="doctor/" class=" btn-view btn-lg px-5">View All</a>
        </p>
      </div>
    </div>
  </section>

  <div class="copyright">
    <div class="container">
      <div class="row">
        <div class="col-12 text-center py-4">
          <p>Copyright_©_globalwellness_2018</p>
        </div>
      </div>
    </div>
  </div>
  <!-- Team -->
  <script src="https://code.jquery.com/jquery-2.2.4.js"
  integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
  crossorigin="anonymous"></script>


  <script src="css/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  <script src="js/owl.carousel.min.js"></script>

  <script>
    $(document).ready(function() {
      $('.card').hover(function() {
            $(this).toggleClass('shadow-lg');
        });
        $('.Rounded_Rectangle_hospitals').hover(function() {
              $(this).toggleClass('shadow-lg');
          });

      $('#maincarousel').owlCarousel({
        loop: false,
        stagePadding: 80,
        dots: true,
        margin: 40,
        nav: false,
        responsive: {
          0: {
            stagePadding: 50,

            items: 1
          },
          600: {
            items: 2
          },
          800: {
            items: 3
          },
          1600: {
            items: 4
          },
          2560: {
            items: 6
          }
        }
      });
    });
  </script>
</body>
</html>
