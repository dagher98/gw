
@extends('layout.main')

    @section('content')
  
      <section class="mr-xl-5 pl-4 py-0 ">
        <div class="container-fluid pl-0">
          <div class="row row-eq-height">
            <div class="col-xl-9 p-0 pr-3">
              <div class="card-mail">
                <h5 class="card-header">Chat Room</h5>
                <div class="card-body-chat px-3">
                  <div class="scroll-chatbox ">
                  {!! Form::open(['action' => 'PatientController@store','method'=>"POST",'enctype'=>'multipart/form-data']) !!}
                   @guest
                    
                      <div class="row">
                        <div class="col-lg-11 pt-2">           
                        <p class="card-text-inbox">please <a href="{{"/login"}}"><b>Sign in</b></a> To view Content</p>
                        </div>
                      </div>
                   
                    @else
                    @if(count($databaseConv)==0)
                    <div class="card-body-inbox px-3">
                      <div class="row">
                        <div class="col-lg-11 pt-2">           
                        <p class="card-text-inbox">No Converstaion Started</p>
                        </div>
                      </div>
                    </div> 
                    @else
                     @foreach($databaseConv as $key => $data)
                     
                      <div class="row">
                        <div class="col-lg-12 pt-2">           
                        <p class="card-text-inbox">Started At {{$data->conv_DateTime}}</p>
                        </div>
                      </div>
                  
                 @php $twoUserConv=false;
                      $twoSendConv='';
                 @endphp
                    @foreach($databaseMsg as $key => $dataMsg)
                      @if($dataMsg->msg_desc=="")
                      @else 
                      @if($dataMsg->msg_type==3)
                           <div class="row my-4 text-center">
                              <div class="col-6 offset-3 ">
                                 <div class="card-text-chat  p-1 " ><b> {{$dataMsg->msg_desc}} By {{$dataMsg->name}} </b></div>                              
                              </div>
                          </div>
                            @php $twoSendConv="" @endphp
                      @else
                      @if($dataMsg->msg_user_id==$auth_id)
                       @php $twoSendConv=$dataMsg->msg_user_id @endphp
                        @if($twoUserConv==true)
                          <div class="row ">
                              <div class="col-lg-2 d-none d-lg-block py-0">
                              </div>
                              <div class="col-lg-10 ">
                              <hr class="m-0">
                                <div class="card-text-chat active px-3 pt-3 pb-0 " >{{$dataMsg->msg_desc}}</div>                              
                                  <div class="col-12 card-text-chat active">
                                    <div class="text-right msg-send">
                                    <small>{{$dataMsg->msg_datetime}}</small>
                                    </div>
                                </div>
                              </div>
                          </div>
                        @else
                        <div class="row  mt-5">
                            <div class="col-lg-2 d-none d-lg-block py-2">
                            </div>
                            <div class="col-lg-10 ">
                              <p class=" pt-3 inbox-active">
                                <img class="img-fluid doc-inbox-icon round-image" src="{{asset('storage/images/'.$dataMsg->img.'')}}" width="5%" alt="card image">
                                <span class="inbox-title px-2">{{$dataMsg->name}}</span>
                              </p>     
                              <div class="card-text-chat active px-3 pt-3 pb-0">{{$dataMsg->msg_desc}}</div>
                                 
                                <div class="col-12 card-text-chat active">
                                  <div class="text-right msg-send">
                                   <small>{{$dataMsg->msg_datetime}}</small>
                                  </div>
                               </div>
                            </div>
                        </div>
                        @php $twoUserConv=true @endphp
                      @endif
                      @else
                       @php $twoUserConv=false @endphp
                        @if($twoSendConv==$dataMsg->msg_user_id)
                         <div class="row ">
                          <div class="col-lg-10 ">
                            <hr class="m-0">
                            <div class="card-text-chat px-3 pt-3 pb-0 ">{{$dataMsg->msg_desc}}</div>
                            <div class="col-12 card-text-chat msg-received ">
                                  <div class="text-right">
                                   <small>{{$dataMsg->msg_datetime}}</small>
                                  </div>
                               </div>
                          </div>
                          <div class="col-lg-2  py-2 d-none d-lg-block">
                          </div>
                        </div>
                        @else
                          <div class="row  mt-5">
                            <div class="col-lg-10 ">
                              <p class=" pt-3">
                                <img class="img-fluid doc-inbox-icon round-image" src="{{asset('storage/images/'.$dataMsg->img.'')}}" width="5%" alt="card image">
                                <span class="inbox-title px-2">{{$dataMsg->name}}</span>
                              </p>
                              <div class="card-text-chat px-3 pt-3 pb-0 ">{{$dataMsg->msg_desc}}</div>
                                <div class="col-12 card-text-chat ">
                                    <div class="text-right msg-received">
                                    <small>{{$dataMsg->msg_datetime}}</small>
                                    </div>
                                </div>
                            </div>
                          <div class="col-lg-2 d-none d-lg-block py-2">
                          </div>
                        </div>
                          @php $twoSendConv=$dataMsg->msg_user_id @endphp
                          @endif
                        @endif  
                       @endif  
                     @endif        
                    @endforeach   
                      </div>
                      <div class="input-group my-3 ">
                    {{Form::textarea('content','',['class'=>'form-control-message ,','placeholder'=>"Enter Your Message Here!!!"])}}
                   {{Form::hidden('convid',$data->conv_id)}}
      
                    <div class="input-group-append">
                      {{ Form::submit('SEND',['class'=>'btn btn-message px-4'])}}
                    </div>
                  </div>
                   @endforeach
                    @endif
                          
                    @endguest
                
              <!-- <div class="row">
                    <div class="col-lg-10 ">
                      <p class=" pt-3">
                        <img class="img-fluid doc-inbox-icon round-image" src="{{asset('img/doctorimg.png')}}" width="5%" alt="card image">
                        <span class="inbox-title px-2">Dr. Andree . S</span>
                      </p>
                      <div class="card-text-chat p-3 mb-5">Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum is simply dummy text of the printing and typesetting industryLorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum
                        is simply dummy text of the printing and typesetting industry Lorem Ipsum is simply dummy text of the printing and typesetting industry</div>
                    </div>
                    <div class="col-lg-2 py-2">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-2 py-2">
                    </div>
                    <div class="col-lg-10 ">
                      <p class=" pt-3 inbox-active">
                        <img class="img-fluid doc-inbox-icon round-image" src="{{asset('img/doctorimg.png')}}" width="5%" alt="card image">
                        <span class="inbox-title px-2">Dr. Andree . S</span>
                      </p>
                      <div class="card-text-chat active p-3 mb-5">Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum is simply dummy text of the printing and typesetting industryLorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum
                        is simply dummy text of the printing and typesetting industry Lorem Ipsum is simply dummy text of the printing and typesetting industry</div>
                    </div>
                  </div> -->              
                </div>
              </div>
            </div>
            <div class="col-xl-3 px-0 py-4 py-xl-0">
              <div class="Rounded_Rectangle ">
                <div class="frontside ">
                  <div class="card-body text-left ">
                   
                      @foreach($doctorsList as $doctor)
                   
                     {{Form::radio('doctors', $doctor->doc_user_id, false,['id'=>'doc'.$doctor->doc_user_id])}}
                
                      <p><label  for="doc{{$doctor->doc_user_id}}" class="m-0 dot doc{{$doctor->doc_user_id}} "></label><span class="inbox-title px-3">{{$doctor->doc_fullname}}</span></p>
                    @endforeach
                     <div class="col-12 text-center">  
                      {{ Form::submit('Add a Doctor',['class'=>'btn btn-inbox btn-sm btn-inbox-text py-2 px-3'])}}                           
                      </div>
                  </div>
                </div>
              </div>
              <div class="Rounded_Rectangle ">
                <div class="frontside ">
                  <div class="card-body p-md-5   ">
                    <div class="row">
                        @foreach($databaseMsg as $key => $dataMsg)
                          @if($dataMsg->msg_attach_tags<>'')
                              @php $ext = pathinfo(asset('storage/'.$dataMsg->msg_attach_tags), PATHINFO_EXTENSION) @endphp
                           
                              <div class="col-6 ">
                                <a href="{{asset('storage/'.$dataMsg->msg_attach_tags)}}" download>   
                                  <img src="{{asset('img/'.$ext.'logo.png')}}" width="80" />
                                <p class="inbox-title py-2" style="overflow:hidden">{{$dataMsg->msg_attach_name}}</p>
                                </a>
                              </div>
                           
                            @else
                            @endif
                          
                        @endforeach
                   
                    </div>
                    <div class="row">
                      <div class="col-12 text-center">
                          {{ Form::file('uploadfile',['id'=>'files','class'=>'d-none'])}}                    
                        <label href="#" for="files" class="btn btn-inbox btn-sm"><span class="btn-inbox-text py-2 px-3">Choose a File<span></label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
    {!! Form::close() !!}
    </section>

@endsection