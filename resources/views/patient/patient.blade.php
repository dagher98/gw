
@extends('layout.main')

    @section('content')
       
  

      <section class="mx-2 mx-md-4 p-0">
        <div class="container-fluid">
          <div class="row">
            <div class="col-xl-10 p-0">
              <div class="card-mail">
                <h5 class="card-header">Inbox</h5>
                  @guest
                    <div class="card-body-inbox px-3">
                      <div class="row">
                        <div class="col-lg-11 pt-2">           
                        <p class="card-text-inbox">please <a href="{{"/login"}}"><b>Sign in</b></a> To view Content</p>
                        </div>
                      </div>
                    </div> 
                    @else
                    @if($converstation==0)
                     @if($usertype==1)
                        @foreach($databaseDoc as $key => $data)
                        <div class="card-body-inbox px-3">
                          <div class="row">
                            <div class="col-lg-11 ">
                              <div class="card-title ">
                                <p class=" pt-2">
                                  <img class="img-fluid doc-inbox-icon round-image" src="{{asset('storage/images/'.$data->doc_img.'')}}" width="5%" alt="card image">
                                <span class="inbox-title px-2">{{$data->doc_fullname}}</span>
                                <span class="inbox-title px-5">  <a href="{{ action('PatientController@edit',$data->doc_user_id) }}"class="btn  btn-doc btn-sm px-3">Start</a></span>
                                </p>
                              </div>
                              <p class="card-text-inbox">Start Conv</p>
                            </div>
                            <div class="col-lg-1 py-2">
                              <span class="card-time-inbox">2 hours</span>
                            </div>
                          </div>
                        </div>
                        @endforeach
                     @else
                      @foreach($databasePat as $key => $data)
                        <div class="card-body-inbox px-3">
                          <div class="row">
                            <div class="col-lg-11 ">
                              <div class="card-title ">
                                <p class=" pt-2">
                                  <img class="img-fluid doc-inbox-icon round-image" src="{{asset('storage/images/'.$data->pat_img.'')}}" width="5%" alt="card image">
                                <span class="inbox-title px-2">{{$data->pat_fullname}}</span>
                                <span class="inbox-title px-5">  <a href="{{ action('PatientController@edit',$data->pat_user_id) }}"class="btn  btn-doc btn-sm px-3">Start</a></span>
                                </p>
                              </div>
                              <p class="card-text-inbox">Start Conv</p>
                            </div>
                            <div class="col-lg-1 py-2">
                              <span class="card-time-inbox"></span>
                            </div>
                          </div>
                        </div>
                        @endforeach
                     @endif
                       
                    @else
                     @php $i=0; @endphp
                    @foreach($databaseConv as $key => $dataConv)
                        <div class="card-body-inbox px-3">
                          <div class="row">
                            <div class="col-lg-11 ">
                              <div class="card-title ">
                                <p class=" pt-2">     
                               @php  $imagearray=explode(',',$ImgArray[$i]); @endphp 
                               
                                @if(count($imagearray)==1)
                                   <img class="img-fluid doc-inbox-icon round-image" src="{{asset('storage/images/'.$ImgArray[$i].'')}}" width="5%" alt="card image"> 
                                @else
                               
                                    @for($x=0;$x<count($imagearray);$x++)
                                    <img class="img-fluid doc-inbox-icon round-image" src="{{asset('storage/images/'.$imagearray[$x].'')}}" width="5%" style="right:{{$x*15}}px" alt="card image">  
                                    @endfor 
                                 
                                @endif
                                
                                <span class="inbox-title ">{{$NameArray[$i]}}</span>
                                <span class="inbox-title px-5">  <a href="{{ action('PatientController@show',$dataConv->conv_id) }}"class="btn  btn-doc btn-sm px-3">Read More</a></span>
                                </p>
                              </div>
                              <p class="card-text-inbox">{{$lastMsg[$i]}}</p>
                            </div>
                            <div class="col-lg-1 py-2">
                            <span class="card-time-inbox">{{$lastMsgTime[$i]}}</span>
                            </div>
                          </div>
                        </div>
                  
                     @php $i=$i+1; @endphp
                    @endforeach
                      <div class="card-body-inbox px-3">
                        <div class="row">
                         <div class="col-lg-12 pt-2">           
                           <p class="card-text-inbox text-center">See All</p>
                        </div>
                      </div>
                    </div> 
                      @if($usertype==1)
                        @foreach($databaseDoc as $key => $data)
                        <div class="card-body-inbox px-3">
                          <div class="row">
                            <div class="col-lg-11 ">
                              <div class="card-title ">
                                <p class=" pt-2">
                                  <img class="img-fluid doc-inbox-icon round-image" src="{{asset('storage/images/'.$data->doc_img.'')}}" width="5%" alt="card image">
                                <span class="inbox-title px-2">{{$data->doc_fullname}}</span>
                                <span class="inbox-title px-5">  <a href="{{ action('PatientController@edit',$data->doc_user_id) }}"class="btn  btn-doc btn-sm px-3">Start</a></span>
                                </p>
                              </div>
                              <p class="card-text-inbox">Start Conv</p>
                            </div>
                            
                          </div>
                        </div>
                        @endforeach
                     @else
                      @foreach($databasePat as $key => $data)
                        <div class="card-body-inbox px-3">
                          <div class="row">
                            <div class="col-lg-11 ">
                              <div class="card-title ">
                                <p class=" pt-2">
                                  <img class="img-fluid doc-inbox-icon round-image" src="{{asset('storage/images/'.$data->pat_img.'')}}" width="5%" alt="card image">
                                <span class="inbox-title px-2">{{$data->pat_fullname}}</span>
                                <span class="inbox-title px-5">  <a href="{{ action('PatientController@edit',$data->pat_user_id) }}"class="btn  btn-doc btn-sm px-3">Start</a></span>
                                </p>
                              </div>
                              <p class="card-text-inbox">Start Conv</p>
                            </div>
                            
                          </div>
                        </div>
                        @endforeach
                     @endif
                    @endif
                          
                    @endguest
              </div>
            </div>
            <div class="col-xl-2 py-3 py-xl-0 px-0 px-xl-2">
              <div class="ml-3 mb-3 square">
                <img src='{{asset('img/ad.png')}}' class="img-fluid"/>
              </div>
              <div class="ml-3 mb-3 square">
                <img src='{{asset('img/ad.png')}}' class="img-fluid"/>
              </div>
              <div class="ml-3 mb-3 square">
                <img src='{{asset('img/ad.png')}}' class="img-fluid"/>
              </div>
            </div>
          </div>
        </div>
      </section>
    @endsection