
@extends('layout.main')

    @section('content')

      <section class="p-0 ">
        <div class="">
          <div id="maincarousel" class="owl-carousel owl-theme">
             @foreach($database as $key => $data)
            <div class="item ">
              <div class="row my-0">
                <div class="frontside ">
                  <div class="card cardwidth " >
                    <div class="card-body text-center py-4">
                      <p class="pb-3 pt-0">
                        <img class="img-fluid images " src="{{asset('storage/images/'.$data->doc_img.'')}}" alt="card image">
                      </p>
                      <h4 class="card-title py-0 my-0">{{$data->doc_fullname}}</h4>
                      <div class="card-text-active">{{$data->spec_name}}</div>
                      <div class="btn BorderDoc-active"> </div>
                      <p class="card-text text-left px-3">
                      
                      {!!  (strlen($data->doc_brief) > 100) ? substr($data->doc_brief,0,99).'...' : $data->doc_brief;!!}
                     
                      </p>
                      <br>
                      <div class="row">
                        <div class="col-5">
                          <a class="btn  btn-doc btn-sm px-3" href="/doctor/{{$data->doc_id}}">Read More</a>
                        </div>
                        <div class="offset-2 col-5">
                          <a href="{{action('PatientController@edit',$data->doc_user_id) }}" class="btn btn-consult btn-sm"><span class="btn-text">Consult<span></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          @endforeach
           
            
          </div>
        </div>
      </section>
   @endsection
   
   @section('scripts')
    <script>

    $('.owl-carousel .item').each(function(index) {
      if (index % 2 == 0) { // wrap by 2 items
        $(this).add($(this).next('.item')).wrapAll('<div class="item__col" />');
      }
    });
    $('.card').hover(function() {
          $(this).toggleClass('shadow-lg');
      });
  </script>
  <script>
    $(document).ready(function() {
      $('#maincarousel').owlCarousel({
        loop: false,
        stagePadding: 40,
        dots: true,
        margin: 50,
        responsive: {
          0: {
            items: 0.75
          },
          350: {
            items: 1
          },
          600: {
            items: 1
          },
          800: {
            items: 3
          },
          1600: {
            items: 4
          },
          2560: {
            items: 6
          }
        }
      });
    });
  </script>
   @endsection