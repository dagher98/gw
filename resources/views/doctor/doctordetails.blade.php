
@extends('layout.main')

    @section('content')
     
      <section class="px-md-2 mx-md-3 px-0 py-0 ">
        <div class="container-fluid">
          <div class="row">
            <div class="col-xl-8">
              <div class="row">
                <div class="col-xl-6 p-0">
              
                  <img src="{{asset('storage/images/'.$database->doc_img.'')}}" class="img-fluid" />
                </div>
                <div class="col-xl-6 pl-md-5 px-0">
                  <div class=" py-0">
                    <h4 class="card-doc-title py-2 py-lg-3 py-xl-0 my-0">{{$database->doc_fullname}}</h4>
                    <div class="card-doc-active">{{$database->spec_name}}</div>
                    <div class="btn BorderDoc-active-detail"> </div>
                    <h5 class="card-title pb-2">{{$database->doc_yoe}} years of experience</h5>
                    <p class="card-text text-left ">{!!$database->doc_brief!!}</p>
                    <div class="row">
                      <div class="col-xl-12 card-title ">
                        <div class="container-fluid">
                          <div class="row ">
                              <div class=" pr-0  pl-lg-2 text-right" >  <i class="flaticon-pin icon-blue"></i></div>
                              <div class="col-sm-6 px-1 px-lg-2" >
                                <div class="short-div" >   <span class="pl-3 doc-marker">{{$database->doc_add_1}}</span>  </div>
                                <div class="short-div" >  <span class="pl-3 doc-marker">{{$database->doc_add_2}}</span></div>
                              </div>
                              <div class="col-sm-2 my-2">
                                  <a class="btn  btn-hos btn-sm px-4 py-1 " href="doctor-detail.html"><span class="align-middle">View on map</span></a>
                              </div>
                          </div>
                          <div class="row my-3">
                            <div class="pr-0  pl-lg-2 text-right" >  <i class="flaticon-phone-call icon-blue"></i></div>
                            <div class="col-8 px-1 px-lg-2" >
                              <div class="short-div" >   <span class="pl-3 doc-marker">{{$database->doc_phone}}</span>  </div>
                              <div class="short-div" >  <span class="pl-3 doc-marker">{{$database->doc_fax}}</span></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="container-fluid px-0 py-2">
                <div class="row">
                  <div class="col-12 p-0">
                    <div class="card-hos-text p-0"> <span class="pr-3 m-0"><i  class="flaticon-doctor icon-title"></i></span> Profesional Statement</div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 p-0">
                    <p class="card-text py-3">{{$database->doc_ps}}</p>
                      @if( $database->doc_specia_tags =="")
                      @else
                    <h5 class="card-title "><b>Specialization</b></h5>
                    <div class=" BorderDoc-active-detail"> </div>
                    <br> 
                    
                       
                        @foreach(explode(',', $database->doc_specia_tags ) as $info)
                    <p><img src="{{asset('img/checkbox.gif')}}" /> <span class="px-2">{{$info}}<span></p>
                      @endforeach
                     @endif     
                  </div>
                </div>
              </div>
              <div class="container-fluid px-0 py-2">
                <div class="row">
                  <div class="col-12 p-0">
                        <div class="card-hos-text p-0">  <span class="pr-3"><i class="flaticon-mortarboard icon-title"></i></span> Education</div>
                </div>
              </div>
              <div class="row">
                <div class="col-12 p-0">
                  <p class="card-text ">{{$database->doc_edu}}</p>
                   @if( $database->doc_curri_tags =="")

                  @else
                  <h5 class="card-title "><b>Curricilum</b></h5>
                  <div class=" BorderDoc-active-detail"> </div>
                  <br>
                 
                  @foreach(explode(',', $database->doc_curri_tags ) as $info)
                  <p><i class="flaticon-mortarboard icon-xs"></i> <span class="px-2">{{$info}}<span></p>
                        @endforeach
                   @endif     
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-4">
              <div class="Rounded_Rectangle_cons ml-md-5 ml-1 mb-2">
                <div class="frontside ">
                  <a href="{{ action('PatientController@edit',$database->doc_user_id) }}">
                    <div class=" text-center py-4">
                        <h4 class="card-doc-title py-0 my-0 text-light">CONSULT NOW</h4>
                        <h4 class="card-title py-0 my-0 text-light ">FOR {{$database->doc_price}}</h4>
                    </div>
                  </a>
                </div>
              </div>
              <div class="Rounded_Rectangle ml-md-5 ml-1">
                <div class="frontside ">
                    <div class="card  ">
                        <div class="card-body text-left py-4">
                          <h6 class="card-title ">Hospitals</h6>
                          <div class=" BorderDoc-active-detail"> </div>
                            <br>
                           
                             @if(count($posthosp)==0)

                             @else
                            @foreach($posthosp as $data)  
                        
                        <p><i class="flaticon-hospital icon-xs-black"></i> <span class="px-2">{{$data->hos_name}}<span></p>
                             @endforeach
                             @endif
                           
                        </div>
                    </div>
                </div>
              </div>
              <div class="Rounded_Rectangle ml-md-5 ml-1">
                <div class="frontside ">
                    <div class="card  ">
                        <div class="card-body text-left py-4">
                          <h6 class="card-title ">Certificates</h6>
                          <div class=" BorderDoc-active-detail"> </div>
                            <br>
                            
                             @foreach($postcert as $info)   
                                 @if($info != "")
                            <p><i class="flaticon-medal icon-xs-black"></i> <span class="px-2">{{$info->hos_name}}<span></p>
                                @endif
                            @endforeach
                           
                        </div>
                    </div>
                </div>
              </div>
      </section>
     
         @endsection

         @section('scripts')
           <script>

        $('.owl-carousel .item').each(function(index) {
          if (index % 2 == 0) { // wrap by 2 items
            $(this).add($(this).next('.item')).wrapAll('<div class="item__col" />');
          }
        });
      </script>
      <script>
        $(document).ready(function() {
          $('#maincarousel').owlCarousel({
            loop: false,
            stagePadding: 40,
            dots: true,
            margin: 50,
            responsive: {
              0: {
                stagePadding: 50,
                margin: 5,
                items: 1
              },
              600: {
                items: 2
              },
              800: {
                items: 3
              },
              1600: {
                items: 4.15
              },
              2560: {
                items: 6
              }
            }
          });
        });
      </script>
      @endsection
