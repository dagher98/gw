
@extends('layout.main')

    @section('content')
      <section class="p-0 ">
        <div class="">
          <div id="maincarousel" class="owl-carousel owl-theme">
            @foreach($database as $key => $data)
           
            <div class="item ">
              <div class="row my-5">
                <div class="col-xl-12 p-0">
                  <div class="Rounded_Rectangle" style="display: -webkit-box">
                    <div class="px-xl-5  py-xl-4 py-md-4  px-4 py-3">
                      <h6 class="card-title m-0">{{$data->hos_city}}</h6>
                      <div class="card-hos-text ">{{$data->hos_name}}</div>
                      <div class="btn BorderDoc"> </div>
                      <div class="row">
                        <div class="col-xl-12 card-title ">
                          <i class="ion-ios-clock-outline "></i> <span class="pl-2 doc-subtitle">{{$data->hos_workhour}}</span>
                          <br>
                          <i class="ion-ios-telephone-outline"></i> <span class="pl-2 doc-subtitle">{{$data->hos_phone}}</span>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-6 pr-xl-4 pl-md-0 px-0 ">
                          <div class=" row mx-1 py-2" style="display: -webkit-box">
                            <div class="col-4 col-sm-6 px-2  px-md-3 px-lg-2">
                              <i class="flaticon-hospital icon-lg"></i>
                            </div>
                            <div class="col-8 col-sm-6 px-0 py-2">
                              <div class=" m-0 p-0 text-center hosp-text-lg"><strong>{{$data->hos_room_count}}</strong></div>
                              <div class=" m-0 p-0 text-center hosp-text">ROOMS</div>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-6 border-left-hos pr-md-3 pl-md-0 px-0">
                          <div class="row mx-1 py-2" style="display: -webkit-box">
                            <div class="col-sm-6 col-4 px-2  px-lg-2">
                              <i class="flaticon-doctor icon-lg"></i>
                            </div>
                            <div class="col-sm-6 col-8 px-xl-3 px-0 py-2">
                              <div class=" m-0 p-0 text-center hosp-text-lg"><strong>{{$data->hos_doc_count}}</strong></div>
                              <div class=" m-0 p-0 text-center hosp-text">DOCTORS</div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="button-hover">
                    <a href="/hospital/{{$data->hos_id}}"> <img src="{{asset('img/roundbutton.png')}}" /></a>
                  </div>
                </div>
              </div>
            </div>
            @endforeach



          </div>
        </div>
      </section>
   @endsection
   
@section('scripts')
     <script>

  var divs = $("div > .item");
  for(var i = 0; i < divs.length; i+=3) {
    divs.slice(i, i+3).wrapAll("<div class='new'></div>");
  }
  $('.Rounded_Rectangle').hover(function() {
        $(this).toggleClass('shadow-lg');
    });


  </script>
  <script>
    $(document).ready(function() {
      $('#maincarousel').owlCarousel({
        loop: false,
        stagePadding: 40,
        dots: true,
        margin: 50,
        responsive: {
          0: {
            items: 0.75
          },
          350: {
            items: 1
          },
          600: {
            items: 2
          },
          800: {
            items: 2
          },
          1200:{
             items: 3
          },
          1600: {
            items: 4
          },
          2560: {
            items: 4
          }
        }
      });
    });
  </script>
  @endsection