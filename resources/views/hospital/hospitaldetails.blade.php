
@extends('layout.main')

    @section('content')
    
          <section class="mr-xl-5 pl-xl-3 py-0 ">
            <div class="container-fluid">
              <div class="row">
                <div class="col-xl-10">
                  <div class="row">
                    <div class="col-xl-8  pr-xl-5">
                      <div class=" py-0 pr-xl-5">
                        <h4 class="card-doc-title py-0 my-0">{{$database->hos_name}}</h4>
                        <div class="card-doc-active">Since {{$database->hos_since}}</div>
                        <div class="btn BorderDoc-active-detail"> </div>
                        <p class="card-text text-left mb-5">{!!$database->hos_brief!!}</p>

                          <div class="col-xl-12 card-title ">
                            <div class="container-fluid px-0">
                              <div class="row ">
                                  <div class=" pr-0  pl-lg-2 text-right" >  <i class="flaticon-pin icon-blue"></i></div>
                                  <div class="col-9 px-1 px-lg-2" >
                                    <div class="short-div" >   <span class="pl-3 doc-marker">{{$database->hos_add_1}}</span>  </div>
                                    <div class="short-div" >  <span class="pl-3 doc-marker">{{$database->hos_add_2}}</span></div>
                                  </div>
                                  <div class="col-2 my-2">
                                      <a class="btn  btn-hos btn-sm px-4 py-1 " href="doctor-detail.html"><span class="align-middle">View on map</span></a>
                                  </div>
                              </div>
                            <i class="flaticon-phone-call icon-blue py-2"></i> <span class="pl-3 doc-marker">{{$database->hos_phone}}</span>
                            <br>
                            <i class="flaticon-team icon-blue py-2"></i> <span class="pl-3 doc-marker">{{$database->hos_website}}</span>
                          </div>
                        </div>
                      </div>
                    </div>
                     
                    <div class="col-xl-4 py-5">
                     
                       @if(sizeof($postspec)-1==0)
                      @else
                      <h5 class="card-title "><b>Specializations</b></h5>
                      <div class=" BorderDoc-active-detail"> </div>
                      <br>
                   
                     
                      @foreach($postspec as $key=>$data)
                       
                      <p><img src="{{asset('img/checkbox.gif')}}" /> <span class="px-2">{{$data->spec_name}}<span></p>
                        
                       @endforeach
                        @endif
                      
                    </div>
                  </div>
                  <div class="container-fluid px-0 py-2">
                    <div class="row">
                      <div class="col-12 p-0" >
                            <div class="card-hos-text p-0">  <span class="pr-3 m-0" ><i  class="flaticon-gallery icon-title"></i></span> GALLERY</div>
               
                    <div class="row pr-md-5 pr-2 mr-md-5 ">
                      @if($database->hos_gallery_img=="")
                      @else
                       @foreach(explode(',',$database->hos_gallery_img) as $info)
                      <div class="col-lg-4 pr-md-5 py-2 py-xl-0 my-1">
                        <div class="Rounded_Rectangle_hos  ">
                          <img src="{{asset('storage/images/'.$info.'')}}" width="100%" />
                        </div>
                      </div>
                      @endforeach
                    @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 p-0">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-2 p-0">
              <div class="Rounded_Rectangle_hos  mb-2">
                <div class="frontside ">
                  <div class=" text-center py-4">
                    <i class="flaticon-hospital icon-xl py-2 text-light"></i>
                    <h4 class="card-hos-title py-0 my-0 text-light">{{$database->hos_room_count}}</h4>
                    <h4 class="card-hos-sub py-0 my-0 text-light ">ROOMS</h4>
                  </div>
                </div>
              </div>
              <div class="Rounded_Rectangle_hos  mb-2">
                <div class="frontside ">
                  <div class=" text-center py-4">
                    <i class="flaticon-doctor icon-xl py-2 text-light"></i>
                    <h4 class="card-hos-title py-0 my-0 text-light">{{$database->hos_doc_count}}</h4>
                    <h4 class="card-hos-sub py-0 my-0 text-light ">Doctors</h4>
                  </div>
                </div>
              </div>
              <div class="Rounded_Rectangle_hos  mb-2">
                <div class="frontside ">
                  <div class=" text-center py-4">
                    <i class="flaticon-nurse icon-xl py-2 text-light"></i>
                    <h4 class="card-hos-title py-0 my-0 text-light">{{$database->hos_nurses_count}}</h4>
                    <h4 class="card-hos-sub py-0 my-0 text-light ">Nurses</h4>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </section>
  
    @endsection
@section('scripts')
      <script>

    $('.owl-carousel .item').each(function(index) {
      if (index % 2 == 0) { // wrap by 2 items
        $(this).add($(this).next('.item')).wrapAll('<div class="item__col" />');
      }
    });
  </script>
  <script>
    $(document).ready(function() {
      $('#maincarousel').owlCarousel({
        loop: false,
        stagePadding: 40,
        dots: true,
        margin: 50,
        responsive: {
          0: {
            stagePadding: 50,
            margin: 5,
            items: 1
          },
          600: {
            items: 2
          },
          800: {
            items: 3
          },
          1600: {
            items: 4.15
          },
          2560: {
            items: 6
          }
        }
      });
    });
  </script>
  @endsection