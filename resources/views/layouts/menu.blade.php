<li class="{{ Request::is('docs*') ? 'active' : '' }}">
    <a href="{!! route('admin.docs.index') !!}"><i class="fa fa-edit"></i><span>Doctors</span></a>
</li>

<li class="{{ Request::is('hosps*') ? 'active' : '' }}">
    <a href="{!! route('admin.hosps.index') !!}"><i class="fa fa-edit"></i><span>Hospitals</span></a>
</li>

<li class="{{ Request::is('hotels*') ? 'active' : '' }}">
    <a href="{!! route('admin.hotels.index') !!}"><i class="fa fa-edit"></i><span>Hotels</span></a>
</li>

<li class="{{ Request::is('patients*') ? 'active' : '' }}">
    <a href="{!! route('admin.patients.index') !!}"><i class="fa fa-edit"></i><span>Patients</span></a>
</li>

<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('admin.users.index') !!}"><i class="fa fa-edit"></i><span>Users</span></a>
</li>

<li class="{{ Request::is('specialities*') ? 'active' : '' }}">
    <a href="{!! route('admin.specialities.index') !!}"><i class="fa fa-edit"></i><span>Specialities</span></a>
</li>



<li class="{{ Request::is('types*') ? 'active' : '' }}">
    <a href="{!! route('admin.types.index') !!}"><i class="fa fa-edit"></i><span>Type</span></a>
</li>

<li class="{{ Request::is('userTypes*') ? 'active' : '' }}">
    <a href="{!! route('admin.userTypes.index') !!}"><i class="fa fa-edit"></i><span>User Types</span></a>
</li>

<li class="{{ Request::is('staticPages*') ? 'active' : '' }}">
    <a href="{!! route('admin.staticPages.index') !!}"><i class="fa fa-edit"></i><span>Static Pages</span></a>
</li>

