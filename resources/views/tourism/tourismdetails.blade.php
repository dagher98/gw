
@extends('layout.main')

    @section('content')
     @foreach($database as $key => $database)
      <section class="mr-xl-5 pl-4 py-0 ">
        <div class="container-fluid p-0">
          <div class="row">
            <div class="col-lg-3 p-0">
              <div class="card  ">
                <img class="card-img-top" src="{{asset('storage/images/'.$database->hot_img.'')}}" alt="Card image cap">
                <div class="card-body">
                  <h4 class="card-tour-title py-0 my-0">{{$database->hot_name}}</h4>
                  <div class="card-text-active">{{$database->type_name}}</div>
                  <div class="btn BorderDoc-active-detail"> </div>
                  <p class="card-text">{{$database->hot_brief}}</p>
                  <div class="col-xl-12 card-title ">
                    <i class="flaticon-pin icon-blue py-3"></i> <span class="pl-3 doc-marker">{{$database->hot_add_1}}<br>{{$database->hot_add_2}}&nbsp;</span>
                    <br>
                    <i class="flaticon-phone-call icon-blue py-3"></i> <span class="pl-3 doc-marker">{{$database->hot_phone}}<br>{{$database->hot_fax}}</span>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="text-center py-4">
                    <a class="btn-map py-2 px-3">View On Map </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-9 pl-2 pl-lg-5 py-5 py-lg-0">
              <div class="card text-center ">
                <div class="card-header-tourism ">
                  <ul class="nav nav-tabs ">
                    <li class="nav-item">
                      <a class="nav-link nav-link-tourism card-tourism active p-4" href="#">Book your Visit</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link nav-link-tourism  p-4" href="#">General Info</a>
                    </li>
                  </ul>
                </div>
                <div class="card-body card-tourism px-0 px-xl-2">
                  <div class="container-fluid">
                    <div class="row">
                      <div class="col-12">
                        <p class="card-text text-left py-3">{{$database->hot_desc}}</p>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-12">
                        <div class="Rounded_Rectangle_tour py-3 ">
                          <p class="text-left m-0 px-3"><img src="{{asset('img/checkboxwhite.png')}}" width="25" /> <span class="px-2 align-center">Select your time and Date<span></p>
                        </div>
                      </div>
                      <!--  <div class="row">
                        <div class="col-md-8 px-lg-5 mx-lg-5 ">
                            <div class="mini-cal">
                                <div class="calender">
                                  <div class="column_right_grid calender">
                                    <div class="cal1">
                                      <div class="clndr">
                                        <div class="clndr-controls">
                                          <div class="clndr-control-button">
                                            <p class="clndr-previous-button">previous</p>
                                          </div>
                                          <div class="month">September 2015</div>
                                          <div class="clndr-control-button rightalign">
                                            <p class="clndr-next-button">next</p>
                                          </div>
                                        </div>
                                        <table class="clndr-table" border="0" cellspacing="0" cellpadding="0">
                                          <thead>
                                            <tr class="header-days">
                                              <td class="header-day">Su</td>
                                              <td class="header-day">M</td>
                                              <td class="header-day">T</td>
                                              <td class="header-day">W</td>
                                              <td class="header-day">T</td>
                                              <td class="header-day">F</td>
                                              <td class="header-day">S</td>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            <tr>
                                              <td class="day past adjacent-month last-month calendar-day-2015-08-30">
                                                <div class="day-contents">30</div>
                                              </td>
                                              <td class="day past adjacent-month last-month calendar-day-2015-08-31">
                                                <div class="day-contents">31</div>
                                              </td>
                                              <td class="day past calendar-day-2015-09-01">
                                                <div class="day-contents">1</div>
                                              </td>
                                              <td class="day past calendar-day-2015-09-02">
                                                <div class="day-contents">2</div>
                                              </td>
                                              <td class="day past calendar-day-2015-09-03">
                                                <div class="day-contents">3</div>
                                              </td>
                                              <td class="day past calendar-day-2015-09-04">
                                                <div class="day-contents">4</div>
                                              </td>
                                              <td class="day past calendar-day-2015-09-05 ">
                                                <div class="day-contents ">5</div>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td class="day past calendar-day-2015-09-06">
                                                <div class="day-contents">6</div>
                                              </td>
                                              <td class="day past calendar-day-2015-09-07">
                                                <div class="day-contents">7</div>
                                              </td>
                                              <td class="day past calendar-day-2015-09-08">
                                                <div class="day-contents">8</div>
                                              </td>
                                              <td class="day past calendar-day-2015-09-09">
                                                <div class="day-contents">9</div>
                                              </td>
                                              <td class="day past event calendar-day-2015-09-10">
                                                <div class="day-contents">10</div>
                                              </td>
                                              <td class="day past event calendar-day-2015-09-11">
                                                <div class="day-contents">11</div>
                                              </td>
                                              <td class="day past event calendar-day-2015-09-12">
                                                <div class="day-contents">12</div>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td class="day past event calendar-day-2015-09-13">
                                                <div class="day-contents">13</div>
                                              </td>
                                              <td class="day past event calendar-day-2015-09-14">
                                                <div class="day-contents">14</div>
                                              </td>
                                              <td class="day past calendar-day-2015-09-15">
                                                <div class="day-contents">15</div>
                                              </td>
                                              <td class="day past calendar-day-2015-09-16">
                                                <div class="day-contents">16</div>
                                              </td>
                                              <td class="day past calendar-day-2015-09-17">
                                                <div class="day-contents">17</div>
                                              </td>
                                              <td class="day past calendar-day-2015-09-18">
                                                <div class="day-contents">18</div>
                                              </td>
                                              <td class="day past calendar-day-2015-09-19">
                                                <div class="day-contents">19</div>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td class="day past calendar-day-2015-09-20">
                                                <div class="day-contents">20</div>
                                              </td>
                                              <td class="day past event calendar-day-2015-09-21">
                                                <div class="day-contents">21</div>
                                              </td>
                                              <td class="day past event calendar-day-2015-09-22">
                                                <div class="day-contents">22</div>
                                              </td>
                                              <td class="day past event calendar-day-2015-09-23">
                                                <div class="day-contents">23</div>
                                              </td>
                                              <td class="day past calendar-day-2015-09-24">
                                                <div class="day-contents">24</div>
                                              </td>
                                              <td class="day past calendar-day-2015-09-25">
                                                <div class="day-contents">25</div>
                                              </td>
                                              <td class="day today calendar-day-2015-09-26">
                                                <div class="day-contents">26</div>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td class="day calendar-day-2015-09-27">
                                                <div class="day-contents">27</div>
                                              </td>
                                              <td class="day calendar-day-2015-09-28">
                                                <div class="day-contents">28</div>
                                              </td>
                                              <td class="day calendar-day-2015-09-29">
                                                <div class="day-contents">29</div>
                                              </td>
                                              <td class="day calendar-day-2015-09-30">
                                                <div class="day-contents">30</div>
                                              </td>
                                              <td class="day adjacent-month next-month calendar-day-2015-10-01">
                                                <div class="day-contents">1</div>
                                              </td>
                                              <td class="day adjacent-month next-month calendar-day-2015-10-02">
                                                <div class="day-contents">2</div>
                                              </td>
                                              <td class="day adjacent-month next-month calendar-day-2015-10-03">
                                                <div class="day-contents">3</div>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                        </div>
                      </div>-->
                </div>
                <div class="row">
                  <div class="col-12 py-2">
                    <div class="Rounded_Rectangle_tour py-3 ">
                      <p class="text-left m-0 px-3"><img src="{{asset('img/checkboxwhite.png')}}" width="25" /> <span class="px-2 align-center">Select your time and Date<span></p>
                    </div>
                  </div>
                </div>
                <div class="container-fluid px-0  p-xl-4 ">
                  <div class="row">
                    <div class="col-xl-6 px-xl-5 px-0 ">
                      <div class="Rectangle  pt-5" >
                        <div class="col-12 text-left py-2 px-xl-5">
                          <div class="form-inline" >
                          <i class="flaticon-guests icon-lg "></i>
                          <div class="styled-select px-4">
                             <select>
                               <option></option>
                               <option>The second option</option>
                               <option>The thrid option</option>
                             </select>
                            <span class="fa fa-sort-desc fa-angle-down"></span>
                        </div>
                      </div>
                    </div>
                    <div class="col-12 text-left py-3 px-xl-5">
                      <div class="form-inline">
                        <i class="flaticon-door icon-lg"></i>
                        <div class="styled-select px-4">
                          <select>
                                     <option></option>
                                     <option>The second option</option>
                                     <option>The thrid option</option>
                                   </select>
                          <span class="fa fa-sort-desc fa-angle-down"></span>
                        </div>
                      </div>
                    </div>
                    <div class="col-12 text-left py-3 px-xl-5">
                      <div class="form-inline">
                        <i class="flaticon-bed icon-lg "></i>
                        <div class="styled-select px-4">
                          <select>
                                     <option></option>
                                     <option>The second option</option>
                                     <option>The thrid option</option>
                                   </select>
                          <span class="fa fa-sort-desc fa-angle-down"></span>
                        </div>
                      </div>
                    </div>
                    <div class="col-12 text-left py-3 px-xl-5">
                      <div class="form-inline">
                        <i class="flaticon-bathtub icon-lg "></i>
                        <div class="styled-select px-4">
                          <select>
                                     <option></option>
                                     <option>The second option</option>
                                     <option>The thrid option</option>
                                   </select>
                          <span class="fa fa-sort-desc fa-angle-down"></span>
                        </div>
                      </div>
                    </div>
                    <div class="checkAvailibility">
                      <button class="btn btn-message p-4"><h4>Check Availibility</h4></button>
                    </div>
                  </div>
                </div>
                <div class="col-xl-6 px-xl-5  py-4 py-xl-0 px-0">
                  <div class="Rectangle py-5 ">
                    <div class="col-12 text-left py-2 px-xl-5">
                      <div class="form-inline">
                        <i class="flaticon-guests icon-lg "></i>
                        <span class="tourism-title px-4">50$ Per Night</span>
                      </div>
                    </div>
                    <div class="col-12 text-left py-2 px-xl-5">
                      <div class="form-inline">
                        <i class="flaticon-wifi icon-lg "></i>
                        <span class="tourism-title px-4">Wifi</span>
                      </div>
                    </div>
                    <div class="col-12 text-left py-2 px-xl-5">
                      <div class="form-inline">
                        <i class="flaticon-cutlery icon-lg "></i>
                        <span class="tourism-title px-4">Restaurant</span>
                      </div>
                    </div>
                    <div class="col-12 text-left py-2 px-xl-5">
                      <div class="form-inline">
                        <i class="flaticon-swimming-pool icon-lg "></i>
                        <span class="tourism-title px-4">Swimming Pool</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
  </div>
  </div>

  </section>
  @endforeach
   @endsection
