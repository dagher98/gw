
@extends('layout.main')

    @section('content')
      <section class="p-0 ">
        <div class="">
          <div id="maincarousel" class="owl-carousel owl-theme">
          @foreach($database as $key => $data)
            <div class="item ">
              <div class="card card-width my-4">
                <div class="container-hover">
                  <img class="image" src="{{asset('storage/images/'.$data->hot_img.'')}}" alt="Card image cap">
                  <div class="middle">
                    <div class="text"><a href="/tourism/1">READ MORE</a></div>
                  </div>
                </div>
                <div class="card-body">
                  <h4 class="card-tour-title py-0 my-0">{{$data->hot_name}}</h4>
                  <div class="card-text-active">{{$data->type_name}}</div>
                  <div class="btn BorderDoc-active-detail"> </div>
                  <p class="card-text">{{$data->hot_brief}}</p>
                </div>
                <div class="card-footer">
                  <small class="text-muted">
                        <div class="row px-2">
                          <div class="col-6 ">
                        <span class="low-opacity">    <i class="flaticon-pin icon-xs-black py-2 " ></i> View On Map</span>
                          </div>
                          <div class="offset-1 col-5 text-center py-2">
                            <a href="#" class="btn btn-inbox btn-sm"><span class="btn-text">BOOK NOW<span></a>
                          </div>
                        </div>
                  </small>
                </div>
              </div>
            </div>
           @endforeach
          </div>
        </div>
      </section>
    @endsection 

@section('scripts')
    <script>

    $('.owl-carousel .item').each(function(index) {
      if (index % 2 == 0) { // wrap by 2 items
        $(this).add($(this).next('.item')).wrapAll('<div class="item__col" />');
      }
    });
  </script>
  <script>
    $(document).ready(function() {
      $('#maincarousel').owlCarousel({
        loop: false,
        stagePadding: 40,
        dots: true,
        margin: 10,
        responsive: {
          0: {
            stagePadding: 10,
            margin: 300,
            items: 1
          },
          600: {
            margin: 200,
            items: 2
          },
          800: {
            margin: 300,
            items: 3
          },
          1600: {
            items: 4
          },
          2560: {
            margin: 400,
            items: 8
          }
        }
      });
    });
  </script>
  @endsection