@php use App\Models\Admin\Type;

  $items = Type::pluck('type_name','type_id');


@endphp

<!-- Hot Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hot_name', 'Hot Name:') !!}
    {!! Form::text('hot_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Hot Type Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hot_type_id', 'Hot Type Id:') !!}
    {!! Form::select('hot_type_id',  $items) !!}
</div>

<!-- Hot Add 1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hot_add_1', 'Hot Add 1:') !!}
    {!! Form::text('hot_add_1', null, ['class' => 'form-control']) !!}
</div>

<!-- Hot Add 2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hot_add_2', 'Hot Add 2:') !!}
    {!! Form::text('hot_add_2', null, ['class' => 'form-control']) !!}
</div>

<!-- Hot City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hot_city', 'Hot City:') !!}
    {!! Form::text('hot_city', null, ['class' => 'form-control']) !!}
</div>

<!-- Hot Country Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hot_country', 'Hot Country:') !!}
    {!! Form::text('hot_country', null, ['class' => 'form-control']) !!}
</div>

<!-- Hot Long Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hot_long', 'Hot Long:') !!}
    {!! Form::text('hot_long', null, ['class' => 'form-control']) !!}
</div>

<!-- Hot Lat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hot_lat', 'Hot Lat:') !!}
    {!! Form::text('hot_lat', null, ['class' => 'form-control']) !!}
</div>

<!-- Hot Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hot_phone', 'Hot Phone:') !!}
    {!! Form::text('hot_phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Hot Mobile Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hot_mobile', 'Hot Mobile:') !!}
    {!! Form::text('hot_mobile', null, ['class' => 'form-control']) !!}
</div>

<!-- Hot Fax Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hot_fax', 'Hot Fax:') !!}
    {!! Form::text('hot_fax', null, ['class' => 'form-control']) !!}
</div>

<!-- Hot Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hot_email', 'Hot Email:') !!}
    {!! Form::text('hot_email', null, ['class' => 'form-control']) !!}
</div>

<!-- Hot Website Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hot_website', 'Hot Website:') !!}
    {!! Form::text('hot_website', null, ['class' => 'form-control']) !!}
</div>

<!-- Hot Brief Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('hot_brief', 'Hot Brief:') !!}
    {!! Form::textarea('hot_brief', null, ['class' => 'form-control']) !!}
</div>

<!-- Hot Img Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hot_img', 'Hot Img:') !!}
    {!! Form::file('hot_img') !!}
</div>

<!-- Hot Desc Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('hot_desc', 'Hot Desc:') !!}
    {!! Form::textarea('hot_desc', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.hotels.index') !!}" class="btn btn-default">Cancel</a>
</div>
