@php use App\Models\Admin\Type; @endphp

<!-- Hot Id Field -->
<div class="form-group">
    {!! Form::label('hot_id', 'Hot Id:') !!}
    <p>{!! $hotels->hot_id !!}</p>
</div>

<!-- Hot Name Field -->
<div class="form-group">
    {!! Form::label('hot_name', 'Hot Name:') !!}
    <p>{!! $hotels->hot_name !!}</p>
</div>

<!-- Hot Type Id Field -->
<div class="form-group">
    {!! Form::label('hot_type_id', 'Hot Type Id:') !!}
    <p>{!!  Type::where('type_id', '=', $hotels->hot_type_id)->first()->type_name!!}</p>
</div>

<!-- Hot Add 1 Field -->
<div class="form-group">
    {!! Form::label('hot_add_1', 'Hot Add 1:') !!}
    <p>{!! $hotels->hot_add_1 !!}</p>
</div>

<!-- Hot Add 2 Field -->
<div class="form-group">
    {!! Form::label('hot_add_2', 'Hot Add 2:') !!}
    <p>{!! $hotels->hot_add_2 !!}</p>
</div>

<!-- Hot City Field -->
<div class="form-group">
    {!! Form::label('hot_city', 'Hot City:') !!}
    <p>{!! $hotels->hot_city !!}</p>
</div>

<!-- Hot Country Field -->
<div class="form-group">
    {!! Form::label('hot_country', 'Hot Country:') !!}
    <p>{!! $hotels->hot_country !!}</p>
</div>

<!-- Hot Long Field -->
<div class="form-group">
    {!! Form::label('hot_long', 'Hot Long:') !!}
    <p>{!! $hotels->hot_long !!}</p>
</div>

<!-- Hot Lat Field -->
<div class="form-group">
    {!! Form::label('hot_lat', 'Hot Lat:') !!}
    <p>{!! $hotels->hot_lat !!}</p>
</div>

<!-- Hot Phone Field -->
<div class="form-group">
    {!! Form::label('hot_phone', 'Hot Phone:') !!}
    <p>{!! $hotels->hot_phone !!}</p>
</div>

<!-- Hot Mobile Field -->
<div class="form-group">
    {!! Form::label('hot_mobile', 'Hot Mobile:') !!}
    <p>{!! $hotels->hot_mobile !!}</p>
</div>

<!-- Hot Fax Field -->
<div class="form-group">
    {!! Form::label('hot_fax', 'Hot Fax:') !!}
    <p>{!! $hotels->hot_fax !!}</p>
</div>

<!-- Hot Email Field -->
<div class="form-group">
    {!! Form::label('hot_email', 'Hot Email:') !!}
    <p>{!! $hotels->hot_email !!}</p>
</div>

<!-- Hot Website Field -->
<div class="form-group">
    {!! Form::label('hot_website', 'Hot Website:') !!}
    <p>{!! $hotels->hot_website !!}</p>
</div>

<!-- Hot Brief Field -->
<div class="form-group">
    {!! Form::label('hot_brief', 'Hot Brief:') !!}
    <p>{!! $hotels->hot_brief !!}</p>
</div>

<!-- Hot Img Field -->
<div class="form-group">
    {!! Form::label('hot_img', 'Hot Img:') !!}
    <p>{!! $hotels->hot_img !!}</p>
</div>

<!-- Hot Desc Field -->
<div class="form-group">
    {!! Form::label('hot_desc', 'Hot Desc:') !!}
    <p>{!! $hotels->hot_desc !!}</p>
</div>

