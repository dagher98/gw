@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Hotels
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($hotels, ['route' => ['admin.hotels.update', $hotels->hot_id], 'method' => 'patch','enctype'=>'multipart/form-data']) !!}

                        @include('admin.hotels.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection