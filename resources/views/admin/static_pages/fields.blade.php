<!-- St Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('st_type', 'St Type:') !!}
    {!! Form::text('st_type', null, ['class' => 'form-control']) !!}
</div>

<!-- St Desc Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('st_desc', 'St Desc:') !!}
    {!! Form::textarea('st_desc', null, ['class' => 'form-control']) !!}
</div>

<!-- St Img Field -->
<div class="form-group col-sm-6">
    {!! Form::label('st_img', 'St Img:') !!}
    {!! Form::text('st_img', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.staticPages.index') !!}" class="btn btn-default">Cancel</a>
</div>
