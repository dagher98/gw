<!-- St Id Field -->
<div class="form-group">
    {!! Form::label('st_id', 'St Id:') !!}
    <p>{!! $staticPage->st_id !!}</p>
</div>

<!-- St Type Field -->
<div class="form-group">
    {!! Form::label('st_type', 'St Type:') !!}
    <p>{!! $staticPage->st_type !!}</p>
</div>

<!-- St Desc Field -->
<div class="form-group">
    {!! Form::label('st_desc', 'St Desc:') !!}
    <p>{!! $staticPage->st_desc !!}</p>
</div>

<!-- St Img Field -->
<div class="form-group">
    {!! Form::label('st_img', 'St Img:') !!}
    <p>{!! $staticPage->st_img !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $staticPage->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $staticPage->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $staticPage->deleted_at !!}</p>
</div>

