@php  
use App\Models\Admin\User;
@endphp


<!-- Pat Id Field -->
<div class="form-group">
    {!! Form::label('pat_id', 'Pat Id:') !!}
    <p>{!! $patient->pat_id !!}</p>
</div>

<!-- Pat Fullname Field -->
<div class="form-group">
    {!! Form::label('pat_fullname', 'Pat Fullname:') !!}
    <p>{!! $patient->pat_fullname !!}</p>
</div>

<!-- Pat User Id Field -->
<div class="form-group">
    {!! Form::label('pat_user_id', 'Patient User :') !!}
    <p>{!!  User::where('id', '=', $patient->pat_user_id)->first()->name!!}</p>
</div>


<!-- Pat Img Field -->
<div class="form-group">
    {!! Form::label('pat_img', 'Pat Img:') !!}
    <p>{!! $patient->pat_img !!}</p>
</div>

<!-- Pat Bithday Field -->
<div class="form-group">
    {!! Form::label('pat_bithday', 'Pat Bithday:') !!}
    <p>{!! $patient->pat_bithday !!}</p>
</div>

<!-- Pat Country Field -->
<div class="form-group">
    {!! Form::label('pat_country', 'Pat Country:') !!}
    <p>{!! $patient->pat_country !!}</p>
</div>

<!-- Pat City Field -->
<div class="form-group">
    {!! Form::label('pat_city', 'Pat City:') !!}
    <p>{!! $patient->pat_city !!}</p>
</div>

<!-- Pat Phone Field -->
<div class="form-group">
    {!! Form::label('pat_phone', 'Pat Phone:') !!}
    <p>{!! $patient->pat_phone !!}</p>
</div>

<!-- Pat Email Field -->
<div class="form-group">
    {!! Form::label('pat_email', 'Pat Email:') !!}
    <p>{!! $patient->pat_email !!}</p>
</div>

<!-- Pat Mobile Field -->
<div class="form-group">
    {!! Form::label('pat_mobile', 'Pat Mobile:') !!}
    <p>{!! $patient->pat_mobile !!}</p>
</div>

<!-- Pat Fax Field -->
<div class="form-group">
    {!! Form::label('pat_fax', 'Pat Fax:') !!}
    <p>{!! $patient->pat_fax !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $patient->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $patient->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $patient->deleted_at !!}</p>
</div>

