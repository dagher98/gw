@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Patient
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($patient, ['route' => ['admin.patients.update', $patient->pat_id], 'method' => 'patch','enctype'=>'multipart/form-data']) !!}

                        @include('admin.patients.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection