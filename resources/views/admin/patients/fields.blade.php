@php
use App\Models\Admin\User;

  $itemsUser = User::where('usertypeid','=','1')->pluck('name','id');
  
@endphp



<!-- Pat Fullname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pat_fullname', 'Pat Fullname:') !!}
    {!! Form::text('pat_fullname', null, ['class' => 'form-control']) !!}
</div>

<!-- Doc User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pat_user_id', 'Patient User :') !!}
     {!! Form::select('pat_user_id',  $itemsUser) !!}
</div>


<!-- Pat Img Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pat_img', 'Pat Img:') !!}
    {!! Form::file('pat_img') !!}
</div>

<!-- Pat Bithday Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pat_bithday', 'Pat Bithday:') !!}
    {!! Form::date('pat_bithday', null, ['class' => 'form-control']) !!}
</div>

<!-- Pat Country Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pat_country', 'Pat Country:') !!}
    {!! Form::text('pat_country', null, ['class' => 'form-control']) !!}
</div>

<!-- Pat City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pat_city', 'Pat City:') !!}
    {!! Form::text('pat_city', null, ['class' => 'form-control']) !!}
</div>

<!-- Pat Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pat_phone', 'Pat Phone:') !!}
    {!! Form::text('pat_phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Pat Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pat_email', 'Pat Email:') !!}
    {!! Form::text('pat_email', null, ['class' => 'form-control']) !!}
</div>

<!-- Pat Mobile Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pat_mobile', 'Pat Mobile:') !!}
    {!! Form::text('pat_mobile', null, ['class' => 'form-control']) !!}
</div>

<!-- Pat Fax Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pat_fax', 'Pat Fax:') !!}
    {!! Form::text('pat_fax', null, ['class' => 'form-control']) !!}
</div>

<!-- Created At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_at', 'Created At:') !!}
    {!! Form::date('created_at', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_at', 'Updated At:') !!}
    {!! Form::date('updated_at', null, ['class' => 'form-control']) !!}
</div>

<!-- Deleted At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    {!! Form::date('deleted_at', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.patients.index') !!}" class="btn btn-default">Cancel</a>
</div>
