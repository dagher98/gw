@php use App\Models\Admin\Speciality;
use App\Models\Admin\Hosp;
use App\Models\Admin\User;
use App\Models\Admin\Doc;

  $itemsSpec = Speciality::pluck('spec_name','spec_id')->toArray();
   if(isset($hosp->hos_id)){   
    $itemsectected=Hosp::find($hosp->hos_id)->hos_specia_tags;
     $hos_specia_tagss = explode(',', $itemsectected);
       $hos_specia_tags =array();
      
     foreach($hos_specia_tagss as $key=>$data){
         if($data!=""){
                array_push($hos_specia_tags,Speciality::where('spec_id',$data)->first()->spec_id);
         }     
     }
    
    }
    else{
        $hos_specia_tags="";
    }

@endphp




<!-- Hos Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hos_name', 'Hos Name:') !!}
    {!! Form::text('hos_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Hos Since Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hos_since', 'Hos Since:') !!}
    {!! Form::number('hos_since', null, ['class' => 'form-control']) !!}
</div>

<!-- Hos Brief Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('hos_brief', 'Hos Brief:') !!}
    {!! Form::textarea('hos_brief', null, ['class' => 'form-control']) !!}
</div>

<!-- Hos Add 1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hos_add_1', 'Hos Add 1:') !!}
    {!! Form::text('hos_add_1', null, ['class' => 'form-control']) !!}
</div>

<!-- Hos Add 2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hos_add_2', 'Hos Add 2:') !!}
    {!! Form::text('hos_add_2', null, ['class' => 'form-control']) !!}
</div>

<!-- Hos City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hos_city', 'Hos City:') !!}
    {!! Form::text('hos_city', null, ['class' => 'form-control']) !!}
</div>

<!-- Hos State Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hos_state', 'Hos State:') !!}
    {!! Form::text('hos_state', null, ['class' => 'form-control']) !!}
</div>

<!-- Hos Country Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hos_country', 'Hos Country:') !!}
    {!! Form::text('hos_country', null, ['class' => 'form-control']) !!}
</div>

<!-- Hos Long Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hos_long', 'Hos Long:') !!}
    {!! Form::text('hos_long', null, ['class' => 'form-control']) !!}
</div>

<!-- Hos Lat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hos_lat', 'Hos Lat:') !!}
    {!! Form::text('hos_lat', null, ['class' => 'form-control']) !!}
</div>

<!-- Hos Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hos_phone', 'Hos Phone:') !!}
    {!! Form::text('hos_phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Hos Mobile Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hos_mobile', 'Hos Mobile:') !!}
    {!! Form::text('hos_mobile', null, ['class' => 'form-control']) !!}
</div>

<!-- Hos Fax Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hos_fax', 'Hos Fax:') !!}
    {!! Form::text('hos_fax', null, ['class' => 'form-control']) !!}
</div>

<!-- Hos Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hos_email', 'Hos Email:') !!}
    {!! Form::text('hos_email', null, ['class' => 'form-control']) !!}
</div>

<!-- Hos Workhour Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hos_workhour', 'Hos Workhour:') !!}
    {!! Form::text('hos_workhour', null, ['class' => 'form-control']) !!}
</div>

<!-- Hos Website Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hos_website', 'Hos Website:') !!}
    {!! Form::text('hos_website', null, ['class' => 'form-control']) !!}
</div>

<!-- Hos Specia Tags Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hos_specia_tags', 'Hos Specia Tags:') !!}
    {!! Form::select('hos_specia_tags[]',$itemsSpec ,$hos_specia_tags, array('multiple')) !!}
</div>

<!-- Hos Gallery Img Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hos_gallery_img', 'Hos Gallery Img:') !!}
    {!! Form::file('hos_gallery_img[]', ['multiple' => 'multiple','class' => 'form-control']) !!}
</div>

<!-- Hos Doc Count Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hos_doc_count', 'Hos Doc Count:') !!}
    {!! Form::number('hos_doc_count', null, ['class' => 'form-control']) !!}
</div>

<!-- Hos Nurses Count Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hos_nurses_count', 'Hos Nurses Count:') !!}
    {!! Form::number('hos_nurses_count', null, ['class' => 'form-control']) !!}
</div>

<!-- Hos Room Count Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hos_room_count', 'Hos Room Count:') !!}
    {!! Form::number('hos_room_count', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.hosps.index') !!}" class="btn btn-default">Cancel</a>
</div>
