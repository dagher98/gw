@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Hosp
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($hosp, ['route' => ['admin.hosps.update', $hosp->hos_id], 'method' => 'patch','enctype'=>'multipart/form-data']) !!}

                        @include('admin.hosps.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection