@php  
use App\Models\Admin\Speciality;
use App\Models\Admin\User;
use App\Models\Admin\Hosp;
use App\Models\Admin\Doc;
$itemsectected=Hosp::find($hosp->hos_id)->hos_specia_tags;
     $hos_specia_tagsArray = explode(',', $itemsectected);
       $hos_specia_tags =array();
    if($hos_specia_tagsArray <> ""){   
     foreach($hos_specia_tagsArray as $data){
         if($data!=""){
            array_push($hos_specia_tags,Speciality::where('spec_id',$data)->first()->spec_name);
         }
     }

 $hos_specia_tag = implode (", ", $hos_specia_tags);}
@endphp


<!-- Hos Id Field -->
<div class="form-group">
    {!! Form::label('hos_id', 'Hos Id:') !!}
    <p>{!! $hosp->hos_id !!}</p>
</div>

<!-- Hos Name Field -->
<div class="form-group">
    {!! Form::label('hos_name', 'Hos Name:') !!}
    <p>{!! $hosp->hos_name !!}</p>
</div>

<!-- Hos Since Field -->
<div class="form-group">
    {!! Form::label('hos_since', 'Hos Since:') !!}
    <p>{!! $hosp->hos_since !!}</p>
</div>

<!-- Hos Brief Field -->
<div class="form-group">
    {!! Form::label('hos_brief', 'Hos Brief:') !!}
    <p>{!! $hosp->hos_brief !!}</p>
</div>

<!-- Hos Add 1 Field -->
<div class="form-group">
    {!! Form::label('hos_add_1', 'Hos Add 1:') !!}
    <p>{!! $hosp->hos_add_1 !!}</p>
</div>

<!-- Hos Add 2 Field -->
<div class="form-group">
    {!! Form::label('hos_add_2', 'Hos Add 2:') !!}
    <p>{!! $hosp->hos_add_2 !!}</p>
</div>

<!-- Hos City Field -->
<div class="form-group">
    {!! Form::label('hos_city', 'Hos City:') !!}
    <p>{!! $hosp->hos_city !!}</p>
</div>

<!-- Hos State Field -->
<div class="form-group">
    {!! Form::label('hos_state', 'Hos State:') !!}
    <p>{!! $hosp->hos_state !!}</p>
</div>

<!-- Hos Country Field -->
<div class="form-group">
    {!! Form::label('hos_country', 'Hos Country:') !!}
    <p>{!! $hosp->hos_country !!}</p>
</div>

<!-- Hos Long Field -->
<div class="form-group">
    {!! Form::label('hos_long', 'Hos Long:') !!}
    <p>{!! $hosp->hos_long !!}</p>
</div>

<!-- Hos Lat Field -->
<div class="form-group">
    {!! Form::label('hos_lat', 'Hos Lat:') !!}
    <p>{!! $hosp->hos_lat !!}</p>
</div>

<!-- Hos Phone Field -->
<div class="form-group">
    {!! Form::label('hos_phone', 'Hos Phone:') !!}
    <p>{!! $hosp->hos_phone !!}</p>
</div>

<!-- Hos Mobile Field -->
<div class="form-group">
    {!! Form::label('hos_mobile', 'Hos Mobile:') !!}
    <p>{!! $hosp->hos_mobile !!}</p>
</div>

<!-- Hos Fax Field -->
<div class="form-group">
    {!! Form::label('hos_fax', 'Hos Fax:') !!}
    <p>{!! $hosp->hos_fax !!}</p>
</div>

<!-- Hos Email Field -->
<div class="form-group">
    {!! Form::label('hos_email', 'Hos Email:') !!}
    <p>{!! $hosp->hos_email !!}</p>
</div>

<!-- Hos Workhour Field -->
<div class="form-group">
    {!! Form::label('hos_workhour', 'Hos Workhour:') !!}
    <p>{!! $hosp->hos_workhour !!}</p>
</div>

<!-- Hos Website Field -->
<div class="form-group">
    {!! Form::label('hos_website', 'Hos Website:') !!}
    <p>{!! $hosp->hos_website !!}</p>
</div>

<!-- Hos Specia Tags Field -->
<div class="form-group">
    {!! Form::label('hos_specia_tags', 'Hos Specia Tags:') !!}
    <p>{!! $hos_specia_tag !!}</p>
</div>

<!-- Hos Gallery Img Field -->
<div class="form-group">
    {!! Form::label('hos_gallery_img', 'Hos Gallery Img:') !!}
    <p>{!! $hosp->hos_gallery_img !!}</p>
</div>

<!-- Hos Doc Count Field -->
<div class="form-group">
    {!! Form::label('hos_doc_count', 'Hos Doc Count:') !!}
    <p>{!! $hosp->hos_doc_count !!}</p>
</div>

<!-- Hos Nurses Count Field -->
<div class="form-group">
    {!! Form::label('hos_nurses_count', 'Hos Nurses Count:') !!}
    <p>{!! $hosp->hos_nurses_count !!}</p>
</div>

<!-- Hos Room Count Field -->
<div class="form-group">
    {!! Form::label('hos_room_count', 'Hos Room Count:') !!}
    <p>{!! $hosp->hos_room_count !!}</p>
</div>

