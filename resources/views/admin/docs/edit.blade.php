@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Doc
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($doc, ['route' => ['admin.docs.update', $doc->doc_id], 'method' => 'patch','enctype'=>'multipart/form-data']) !!}

                        @include('admin.docs.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection