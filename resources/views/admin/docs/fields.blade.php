@php use App\Models\Admin\Speciality;
use App\Models\Admin\Hosp;
use App\Models\Admin\User;
use App\Models\Admin\Doc;

  $itemsSpec = Speciality::pluck('spec_name','spec_id');
  $itemsUser = User::where('usertypeid','=','2')->pluck('name','id');
  $itemsHosp = Hosp::pluck('hos_name','hos_id')->toArray();
   if(isset($doc->doc_id )){   
    $itemsectected=Doc::find($doc->doc_id)->doc_hos_tags;
    
     $doc_hos_tagss = explode(',', $itemsectected);
       $doc_hos_tags =array();
     foreach($doc_hos_tagss as $key=>$data){
       
         if($data!=""){
           //array_push($doc_hos_tags,Hosp::where('hos_id',$data)->pluck('hos_name','hos_id')->toArray());
              array_push($doc_hos_tags,Hosp::where('hos_id',$data)->first()->hos_id);
         }
     }
    }
    else{
        $doc_hos_tags="";
    }

@endphp


<!-- Doc Spec Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('doc_spec_id', 'Doc Spec Id:') !!}
    {!! Form::select('doc_spec_id',  $itemsSpec) !!}
</div>

<!-- Doc User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('doc_user_id', 'Doc User Id:') !!}
     {!! Form::select('doc_user_id',  $itemsUser) !!}
</div>

<!-- Doc Fullname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('doc_fullname', 'Doc Fullname:') !!}
    {!! Form::text('doc_fullname', null, ['class' => 'form-control']) !!}
</div>

<!-- Doc Img Field -->
<div class="form-group col-sm-6">
    {!! Form::label('doc_img', 'Doc Img:') !!}
    {!! Form::file('doc_img') !!}
</div>

<!-- Doc Yoe Field -->
<div class="form-group col-sm-6">
    {!! Form::label('doc_yoe', 'Doc Yoe:') !!}
    {!! Form::number('doc_yoe', null, ['class' => 'form-control']) !!}
</div>

<!-- Doc Brief Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('doc_brief', 'Doc Brief:') !!}
    {!! Form::textarea('doc_brief', null, ['class' => 'form-control']) !!}
</div>

<!-- Doc Add 1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('doc_add_1', 'Doc Add 1:') !!}
    {!! Form::text('doc_add_1', null, ['class' => 'form-control']) !!}
</div>

<!-- Doc Add 2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('doc_add_2', 'Doc Add 2:') !!}
    {!! Form::text('doc_add_2', null, ['class' => 'form-control']) !!}
</div>

<!-- Doc City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('doc_city', 'Doc City:') !!}
    {!! Form::text('doc_city', null, ['class' => 'form-control']) !!}
</div>

<!-- Doc State Field -->
<div class="form-group col-sm-6">
    {!! Form::label('doc_state', 'Doc State:') !!}
    {!! Form::text('doc_state', null, ['class' => 'form-control']) !!}
</div>

<!-- Doc Country Field -->
<div class="form-group col-sm-6">
    {!! Form::label('doc_country', 'Doc Country:') !!}
    {!! Form::text('doc_country', null, ['class' => 'form-control']) !!}
</div>

<!-- Doc Long Field -->
<div class="form-group col-sm-6">
    {!! Form::label('doc_long', 'Doc Long:') !!}
    {!! Form::text('doc_long', null, ['class' => 'form-control']) !!}
</div>

<!-- Doc Lat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('doc_lat', 'Doc Lat:') !!}
    {!! Form::text('doc_lat', null, ['class' => 'form-control']) !!}
</div>

<!-- Doc Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('doc_phone', 'Doc Phone:') !!}
    {!! Form::text('doc_phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Doc Mobile Field -->
<div class="form-group col-sm-6">
    {!! Form::label('doc_mobile', 'Doc Mobile:') !!}
    {!! Form::text('doc_mobile', null, ['class' => 'form-control']) !!}
</div>

<!-- Doc Fax Field -->
<div class="form-group col-sm-6">
    {!! Form::label('doc_fax', 'Doc Fax:') !!}
    {!! Form::text('doc_fax', null, ['class' => 'form-control']) !!}
</div>

<!-- Doc Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('doc_email', 'Doc Email:') !!}
    {!! Form::text('doc_email', null, ['class' => 'form-control']) !!}
</div>

<!-- Doc Ps Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('doc_ps', 'Doc Ps:') !!}
    {!! Form::textarea('doc_ps', null, ['class' => 'form-control']) !!}
</div>

<!-- Doc Specia Tags Field -->
<div class="form-group col-sm-6">
    {!! Form::label('doc_specia_tags', 'Doc Specia Tags:') !!}
    {!! Form::text('doc_specia_tags', null, ['class' => 'form-control']) !!}
</div>

<!-- Doc Edu Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('doc_edu', 'Doc Edu:') !!}
    {!! Form::textarea('doc_edu', null, ['class' => 'form-control']) !!}
</div>

<!-- Doc Curri Tags Field -->
<div class="form-group col-sm-6">
    {!! Form::label('doc_curri_tags', 'Doc Curri Tags:') !!}
    {!! Form::text('doc_curri_tags', null, ['class' => 'form-control']) !!}
</div>

<!-- Doc Cert Tags Field -->
<div class="form-group col-sm-6">
    {!! Form::label('doc_cert_tags', 'Doc Cert Tags:') !!}
    {!! Form::text('doc_cert_tags', null, ['class' => 'form-control']) !!}
</div>

<!-- Doc Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('doc_price', 'Doc Price:') !!}
    {!! Form::text('doc_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Doc Hos Tags Field -->

<div class="form-group col-sm-6">
    {!! Form::label('doc_hos_tags', 'Doc Hos Tags:') !!}
   
    {!! Form::select('doc_hos_tags[]',$itemsHosp ,$doc_hos_tags, array('multiple')) !!}
</div>

<!-- Created At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_at', 'Created At:') !!}
    {!! Form::date('created_at', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_at', 'Updated At:') !!}
    {!! Form::date('updated_at', null, ['class' => 'form-control']) !!}
</div>

<!-- Deleted At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    {!! Form::date('deleted_at', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.docs.index') !!}" class="btn btn-default">Cancel</a>
</div>
