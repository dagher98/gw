@php  
use App\Models\Admin\Speciality;
use App\Models\Admin\User;
use App\Models\Admin\Hosp;
use App\Models\Admin\Doc;
$itemsectected=Doc::find($doc->doc_id)->doc_hos_tags;
     $doc_hos_tagss = explode(',', $itemsectected);
       $doc_hos_tags =array();
    if($doc_hos_tagss <> ""){   
     foreach($doc_hos_tagss as $data){
         if($data!=""){
           array_push($doc_hos_tags,Hosp::where('hos_id',$data)->first()->hos_name);
         }
     }

 $doc_hos_tag = implode (", ", $doc_hos_tags);}

@endphp

<!-- Doc Id Field -->
<div class="form-group">
    {!! Form::label('doc_id', 'Doc Id:') !!}
    <p>{!! $doc->doc_id !!}</p>
</div>

<!-- Doc Spec Id Field -->
<div class="form-group">
    {!! Form::label('doc_spec_id', 'Doc Spec Id:') !!}
    <p>{!!  Speciality::where('spec_id', '=', $doc->doc_spec_id)->first()->spec_name!!}</p>
</div>

<!-- Doc User Id Field -->
<div class="form-group">
    {!! Form::label('doc_user_id', 'Doc User Id:') !!}
    <p>{!!  User::where('id', '=', $doc->doc_user_id)->first()->name!!}</p>
</div>

<!-- Doc Fullname Field -->
<div class="form-group">
    {!! Form::label('doc_fullname', 'Doc Fullname:') !!}
    <p>{!! $doc->doc_fullname !!}</p>
</div>

<!-- Doc Img Field -->
<div class="form-group">
    {!! Form::label('doc_img', 'Doc Img:') !!}
    <p>{!! $doc->doc_img !!}</p>
</div>

<!-- Doc Yoe Field -->
<div class="form-group">
    {!! Form::label('doc_yoe', 'Doc Yoe:') !!}
    <p>{!! $doc->doc_yoe !!}</p>
</div>

<!-- Doc Brief Field -->
<div class="form-group">
    {!! Form::label('doc_brief', 'Doc Brief:') !!}
    <p>{!! $doc->doc_brief !!}</p>
</div>

<!-- Doc Add 1 Field -->
<div class="form-group">
    {!! Form::label('doc_add_1', 'Doc Add 1:') !!}
    <p>{!! $doc->doc_add_1 !!}</p>
</div>

<!-- Doc Add 2 Field -->
<div class="form-group">
    {!! Form::label('doc_add_2', 'Doc Add 2:') !!}
    <p>{!! $doc->doc_add_2 !!}</p>
</div>

<!-- Doc City Field -->
<div class="form-group">
    {!! Form::label('doc_city', 'Doc City:') !!}
    <p>{!! $doc->doc_city !!}</p>
</div>

<!-- Doc State Field -->
<div class="form-group">
    {!! Form::label('doc_state', 'Doc State:') !!}
    <p>{!! $doc->doc_state !!}</p>
</div>

<!-- Doc Country Field -->
<div class="form-group">
    {!! Form::label('doc_country', 'Doc Country:') !!}
    <p>{!! $doc->doc_country !!}</p>
</div>

<!-- Doc Long Field -->
<div class="form-group">
    {!! Form::label('doc_long', 'Doc Long:') !!}
    <p>{!! $doc->doc_long !!}</p>
</div>

<!-- Doc Lat Field -->
<div class="form-group">
    {!! Form::label('doc_lat', 'Doc Lat:') !!}
    <p>{!! $doc->doc_lat !!}</p>
</div>

<!-- Doc Phone Field -->
<div class="form-group">
    {!! Form::label('doc_phone', 'Doc Phone:') !!}
    <p>{!! $doc->doc_phone !!}</p>
</div>

<!-- Doc Mobile Field -->
<div class="form-group">
    {!! Form::label('doc_mobile', 'Doc Mobile:') !!}
    <p>{!! $doc->doc_mobile !!}</p>
</div>

<!-- Doc Fax Field -->
<div class="form-group">
    {!! Form::label('doc_fax', 'Doc Fax:') !!}
    <p>{!! $doc->doc_fax !!}</p>
</div>

<!-- Doc Email Field -->
<div class="form-group">
    {!! Form::label('doc_email', 'Doc Email:') !!}
    <p>{!! $doc->doc_email !!}</p>
</div>

<!-- Doc Ps Field -->
<div class="form-group">
    {!! Form::label('doc_ps', 'Doc Ps:') !!}
    <p>{!! $doc->doc_ps !!}</p>
</div>

<!-- Doc Specia Tags Field -->
<div class="form-group">
    {!! Form::label('doc_specia_tags', 'Doc Specia Tags:') !!}
    <p>{!! $doc->doc_specia_tags !!}</p>
</div>

<!-- Doc Edu Field -->
<div class="form-group">
    {!! Form::label('doc_edu', 'Doc Edu:') !!}
    <p>{!! $doc->doc_edu !!}</p>
</div>

<!-- Doc Curri Tags Field -->
<div class="form-group">
    {!! Form::label('doc_curri_tags', 'Doc Curri Tags:') !!}
    <p>{!! $doc->doc_curri_tags !!}</p>
</div>

<!-- Doc Cert Tags Field -->
<div class="form-group">
    {!! Form::label('doc_cert_tags', 'Doc Cert Tags:') !!}
    <p>{!! $doc->doc_cert_tags !!}</p>
</div>

<!-- Doc Price Field -->
<div class="form-group">
    {!! Form::label('doc_price', 'Doc Price:') !!}
    <p>{!! $doc->doc_price !!}</p>
</div>

<!-- Doc Hos Tags Field -->
<div class="form-group">
    {!! Form::label('doc_hos_tags', 'Doc Hos Tags:') !!}
    <p>{!! $doc_hos_tag !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $doc->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $doc->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $doc->deleted_at !!}</p>
</div>




