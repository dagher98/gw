<!-- Type Id Field -->
<div class="form-group">
    {!! Form::label('type_id', 'Type Id:') !!}
    <p>{!! $type->type_id !!}</p>
</div>

<!-- Type Name Field -->
<div class="form-group">
    {!! Form::label('type_name', 'Type Name:') !!}
    <p>{!! $type->type_name !!}</p>
</div>

<!-- Type Img Field -->
<div class="form-group">
    {!! Form::label('type_img', 'Type Img:') !!}
    <p>{!! $type->type_img !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $type->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $type->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $type->deleted_at !!}</p>
</div>

