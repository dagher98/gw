<!-- Ut Id Field -->
<div class="form-group">
    {!! Form::label('ut_id', 'Ut Id:') !!}
    <p>{!! $userType->ut_id !!}</p>
</div>

<!-- Ut Name Field -->
<div class="form-group">
    {!! Form::label('ut_name', 'Ut Name:') !!}
    <p>{!! $userType->ut_name !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $userType->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $userType->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $userType->deleted_at !!}</p>
</div>

