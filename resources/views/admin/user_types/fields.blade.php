<!-- Ut Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ut_name', 'Ut Name:') !!}
    {!! Form::text('ut_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.userTypes.index') !!}" class="btn btn-default">Cancel</a>
</div>
