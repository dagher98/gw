<!-- Spec Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('spec_name', 'Spec Name:') !!}
    {!! Form::text('spec_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Spec Img Field -->
<div class="form-group col-sm-6">
    {!! Form::label('spec_img', 'Spec Img:') !!}
    {!! Form::file('spec_img', null, ['class' => 'form-control']) !!}
</div>

<!-- Spec Desc Field -->
<div class="form-group col-sm-6">
    {!! Form::label('spec_desc', 'Spec Desc:') !!}
    {!! Form::text('spec_desc', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.specialities.index') !!}" class="btn btn-default">Cancel</a>
</div>
