<!-- Spec Id Field -->
<div class="form-group">
    {!! Form::label('spec_id', 'Spec Id:') !!}
    <p>{!! $speciality->spec_id !!}</p>
</div>

<!-- Spec Name Field -->
<div class="form-group">
    {!! Form::label('spec_name', 'Spec Name:') !!}
    <p>{!! $speciality->spec_name !!}</p>
</div>

<!-- Spec Img Field -->
<div class="form-group">
    {!! Form::label('spec_img', 'Spec Img:') !!}
    <p>{!! $speciality->spec_img !!}</p>
</div>

<!-- Spec Desc Field -->
<div class="form-group">
    {!! Form::label('spec_desc', 'Spec Desc:') !!}
    <p>{!! $speciality->spec_desc !!}</p>
</div>

