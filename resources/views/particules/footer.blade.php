

<div class="copyright-grey">
  <div class="container-fluid pl-0 ">
    <div class="row">
      <div class="col-1 ">
        <div class="nav-square text-center align-text-middle py-3"><img src="{{asset('img/arrow.png')}}" /></div>
      </div>
      <div class="col-11 pr-0">
        <p class="m-0 py-4 text-center">Copyright_©_globalwellness_2018</p>
      </div>
    </div>
  </div>
</div>

<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<script>
  $(document).ready(function() {
    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;

    jQuery(document).ready(function($){
        if(!isMobile) {
          $('#sidebar').hover(function() {
            $('#sidebar').toggleClass('active');
            $('.nav-square').toggleClass('active');
          });
        }
        else{
          $('#sidebar').toggleClass('active');
          $('.nav-square').toggle('.active');
        }
    });


    $('.navbar-toggler').click(function() {

      if($('#sidebar').css('display') == 'none' ){
          $('#sidebar').show('.active');
      }
      else{
      $('#sidebar').toggleClass('active');
      }
      if($('.nav-square').css('display') == 'none' ){
          $('.nav-square').show('.active');
      }
      else{
        $('.nav-square').toggle('.active');
      }
        //$('#sidebar').toggleClass('active');


    });

  });
</script>
  @yield('scripts')