
@php
$patientactive="";
$doctoractive="";
$accommondationactive="";
$hospitalactive="";
$planactive="";
$tourismactive="";
$outletsactive="";

@endphp
@switch ($pagename)   
    @case("Patient")
   @php $patientactive="active" @endphp
         @break
     
      @case( "Doctor")
         @php   $doctoractive="active"@endphp
        @break;
       @case( "Hospital")
       @php  $hospitalactive="active"@endphp
          @break;
        @case( "Plan")
       @php    $planactive="active"@endphp
          @break;
        @case( "Hotels")
        @php   $tourismactive="active"@endphp
          @break;
        @case( "Outlets")
        @php   $outletsactive="active"@endphp
          @break;

    @default:
           @php  $patientactive="active"@endphp
}

 @endswitch
 

<nav id="sidebar" class="active">
  <ul class="list-unstyled components">
    <li class="{{$patientactive }}">
      <a href="/patient" class=" text-left">
                  <i class="flaticon-patient"></i>
                    <span>Patient</span>
                </a>
    </li>
    <li class="{{ $doctoractive }}">
      <a href="/doctor" class="text-left">
                <i class="flaticon-doctor"></i>
                    <span>Doctors</span>
                </a>
    </li>
    <li class="{{ $hospitalactive }}">
      <a href="/hospital" class="text-left">
                  <i class="flaticon-hospital"></i>
                    <span>Hospitals</span>
                </a>
    </li>
    <li class="{{ $outletsactive }}">
      <a href="#pageSubmenu"  class="text-left">
                <i class="flaticon-medical-drug-pill"></i>
                  <span>Pharmacies</span>
              </a>
    </li>
    <li class="{{ $tourismactive }}">
      <a href="/tourism"  class="text-left">
              <i class="flaticon-airplane-flight-in-circle-around-earth"></i>
                <span>Tourism</span>
            </a>
    </li>
    <li class="{{ $planactive }}">
      <a href="/plan" class="text-left">
            <i class="flaticon-shopping-list"></i>
              <span>Plan</span>
          </a>
    </li>
  </ul>
</nav>
