 
 
@php 
use App\Models\Admin\Speciality;
use App\Models\Admin\Hosp;
use App\Models\Admin\Doc;
$selectLoc='';
$HospLocList=Hosp::pluck('hos_country','hos_country');
foreach($HospLocList as $HospLoc){
  $selectLoc.='<a class="dropdown-item" href="/hospital/'.$HospLoc.'/ShowLoc">'.$HospLoc.'</a>';
}

@endphp
@if($pagetitle<>"") 
<nav class="navbar navbar-expand-lg mb-3 mx-2 mx-md-1">
  <div class="container-fluid">
    <span class="nav-title">  {{$pagename }}/ {{ $pagetitle }}</span>
  </div>
</nav>


@else

  <nav class="navbar navbar-expand-lg mb-3 mx-2 mx-md-4">
    <div class="container-fluid">
      <span class="nav-title">  {{$pagename }}</span>
      <div class="text-right">
        <div class="dropdown d-inline-block" >
           {!! Form::model( ['route' => ['admin.docs.update'], 'method' => 'patch','enctype'=>'multipart/form-data']) !!}
              <span class="nav-title px-3 "  id="dropdownMenuButton" data-toggle="dropdown" >  Location <img src="{{asset('img/downarrow.png')}}"/></span>
             
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  {!!$selectLoc!!}
                </div>
              </div>
              <div class="dropdown d-inline-block" >
                <span class="nav-title px-3 "  id="dropdownMenuButton" data-toggle="dropdown" >   Specialization <img src="{{asset('img/downarrow.png')}}"/></span>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="#">Action</a>
                  <a class="dropdown-item" href="#">Another action</a>
                  <a class="dropdown-item" href="#">Something else here</a>
                </div>
              </div>     
            {!! Form::close() !!}
      </div>
    </div>
  </nav>

@endif
