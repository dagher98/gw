

<div class="d-flex flex-column flex-md-row navbar-expand-lg align-items-center px-md-3 px-lg-5 py-lg-5 py-2 copyright  ">
  <h5 class="my-0 mr-md-auto  pl-xl-5 gwlogo"><a href="/">GW</a></h5>
  <nav class="my-1 my-md-0 mr-md-3 nav-font">
      <!-- Authentication Links -->
                        @guest
                         </nav>
                         <a class="btn pr-xl-5" href="#">
                          <div class="input-group mb-3 mb-md-0">
                            <input type="text" class="form-control-nav" placeholder="Search">
                            <div class="input-group-append">
                              <button class="button-search">
                                  <span class="fa fa-search"></span>
                              </button>
                            </div>
                          </div>
                        </a>
                         <button class="navbar-toggler " type="button" data-toggle="collapse"  aria-expanded="false" aria-label="Toggle navigation">
                          <span class="navbar-toggler-icon"></span>
                        </button>
                        <a class="btn pr-lg-5"  href="{{ route('login') }}"><img src='{{asset('img/user.png')}}' width="30"/></a>
                      </div>
                            
                        @else
                        <a class="p-2 text-light " href="#">   {{ Auth::user()->name }}</a>
                          <a class="p-2 text-light " href="#">  <i class="flaticon-mail" aria-hidden="true"></i></a>
                        </nav>
                        <a class="btn pr-xl-5" href="#">
                          <div class="input-group mb-3 mb-md-0">
                            <input type="text" class="form-control-nav" placeholder="Search">
                            <div class="input-group-append">
                              <button class="button-search">
                                  <span class="fa fa-search"></span>
                              </button>
                            </div>
                          </div>
                        </a>
                        <button class="navbar-toggler " type="button" data-toggle="collapse"  aria-expanded="false" aria-label="Toggle navigation">
                          <span class="navbar-toggler-icon"></span>
                        </button>
                        <a class="btn pr-lg-5"href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><img src='{{asset('img/useractivate.png')}}' width="30"/></a>
                      </div>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                
                        @endguest
    