 <!doctype html>
<html lang="en">

<head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/vnd.microsoft.icon" href="{{asset('img/cardiology.png')}}">
  <title>Global Wellness</title>
  <link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
  <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/style.css')}}">
  <link href="{{asset('fonts/stylesheet.css')}}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{asset('fonts/icon/flaticon.css')}}">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
</head>

<body>
 
@include('particules.header')
  <div class="wrapper">
    <!-- Sidebar  -->
    @include('particules.sidebar')
    <!-- Page Content  -->
    <div id="content" class="ml-1 px-0">
            @include('particules.dir')
            @yield('content')
       </div>
  </div>
@include('particules.footer')

   
<script>
//   $('input:radio').change(function(){
//      var id = $(this).attr('id');
//     if($(this).is(":checked")) {
//       $("."+id).addClass("active");
//     } else {
//        $("."+id).removeClass("active");
//     }
// });

$('input[type=radio]').click(function() {
    $('input[name="' + this.name + '"]').each(function(){
        $("."+this.id).toggleClass('active', this.checked);
    });
});
</script>
</body>

</html>
