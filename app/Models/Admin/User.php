<?php

namespace App\Models\Admin;

use Eloquent as Model;

/**
 * Class User
 * @package App\Models\Admin
 * @version October 12, 2018, 9:11 am UTC
 *
 * @property string name
 * @property string email
 * @property string password
 * @property integer usertypeid
 * @property string remember_token
 */
class User extends Model
{

    public $table = 'users';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'id';

    public $fillable = [
        'name',
        'email',
        'password',
        'usertypeid',
        'remember_token'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'usertypeid' => 'integer',
        'remember_token' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
