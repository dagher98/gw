<?php

namespace App\Models\Admin;

use Eloquent as Model;

/**
 * Class Hosp
 * @package App\Models\Admin
 * @version October 11, 2018, 6:18 am UTC
 *
 * @property string hos_name
 * @property integer hos_since
 * @property string hos_brief
 * @property string hos_add_1
 * @property string hos_add_2
 * @property string hos_city
 * @property string hos_state
 * @property string hos_country
 * @property string hos_long
 * @property string hos_lat
 * @property string hos_phone
 * @property string hos_mobile
 * @property string hos_fax
 * @property string hos_email
 * @property string hos_workhour
 * @property string hos_website
 * @property string hos_specia_tags
 * @property string hos_gallery_img
 * @property integer hos_doc_count
 * @property integer hos_nurses_count
 * @property integer hos_room_count
 */
class Hosp extends Model
{

    public $table = 'gw_hospitals';
    
    public $timestamps = false;


    protected $primaryKey = 'hos_id';

    public $fillable = [
        'hos_name',
        'hos_since',
        'hos_brief',
        'hos_add_1',
        'hos_add_2',
        'hos_city',
        'hos_state',
        'hos_country',
        'hos_long',
        'hos_lat',
        'hos_phone',
        'hos_mobile',
        'hos_fax',
        'hos_email',
        'hos_workhour',
        'hos_website',
        'hos_specia_tags',
        'hos_gallery_img',
        'hos_doc_count',
        'hos_nurses_count',
        'hos_room_count'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'hos_id' => 'integer',
        'hos_name' => 'string',
        'hos_since' => 'integer',
        'hos_brief' => 'string',
        'hos_add_1' => 'string',
        'hos_add_2' => 'string',
        'hos_city' => 'string',
        'hos_state' => 'string',
        'hos_country' => 'string',
        'hos_long' => 'string',
        'hos_lat' => 'string',
        'hos_phone' => 'string',
        'hos_mobile' => 'string',
        'hos_fax' => 'string',
        'hos_email' => 'string',
        'hos_workhour' => 'string',
        'hos_website' => 'string',
        'hos_specia_tags' => 'string',
        'hos_gallery_img' => 'string',
        'hos_doc_count' => 'integer',
        'hos_nurses_count' => 'integer',
        'hos_room_count' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
