<?php

namespace App\Models\Admin;

use Eloquent as Model;

/**
 * Class Patient
 * @package App\Models\Admin
 * @version October 11, 2018, 6:20 am UTC
 *
 * @property string pat_fullname
 * @property string pat_img
 * @property date pat_bithday
 * @property string pat_country
 * @property string pat_city
 * @property string pat_phone
 * @property string pat_email
 * @property string pat_mobile
 * @property string pat_fax
 * @property string|\Carbon\Carbon created_at
 * @property string|\Carbon\Carbon updated_at
 * @property string|\Carbon\Carbon deleted_at
 */
class Patient extends Model
{

    public $table = 'gw_patient';
    
    public $timestamps = false;


    protected $primaryKey = 'pat_id';

    public $fillable = [
        'pat_fullname',
        'pat_img',
        'pat_bithday',
        'pat_user_id',
        'pat_country',
        'pat_city',
        'pat_phone',
        'pat_email',
        'pat_mobile',
        'pat_fax',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'pat_id' => 'integer',
        'pat_fullname' => 'string',
        'pat_img' => 'string',
        'pat_user_id'=>'integer',
        'pat_bithday' => 'date',
        'pat_country' => 'string',
        'pat_city' => 'string',
        'pat_phone' => 'string',
        'pat_email' => 'string',
        'pat_mobile' => 'string',
        'pat_fax' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
