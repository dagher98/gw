<?php

namespace App\Models\Admin;

use Eloquent as Model;

/**
 * Class StaticPage
 * @package App\Models\Admin
 * @version October 12, 2018, 12:56 pm UTC
 *
 * @property string st_type
 * @property string st_desc
 * @property string st_img
 */
class StaticPage extends Model
{

    public $table = 'gw_static';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'st_id';

    public $fillable = [
        'st_type',
        'st_desc',
        'st_img'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'st_id' => 'integer',
        'st_type' => 'string',
        'st_desc' => 'string',
        'st_img' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
