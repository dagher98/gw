<?php

namespace App\Models\Admin;

use Eloquent;
use Elasticquent\ElasticquentTrait;
/**
 * Class Doc
 * @package App\Models\Admin
 * @version October 10, 2018, 4:19 pm UTC
 *
 * @property integer doc_spec_id
 * @property integer doc_user_id
 * @property string doc_fullname
 * @property string doc_img
 * @property integer doc_yoe
 * @property string doc_brief
 * @property string doc_add_1
 * @property string doc_add_2
 * @property string doc_city
 * @property string doc_state
 * @property string doc_country
 * @property string doc_long
 * @property string doc_lat
 * @property string doc_phone
 * @property string doc_mobile
 * @property string doc_fax
 * @property string doc_email
 * @property string doc_ps
 * @property string doc_specia_tags
 * @property string doc_edu
 * @property string doc_curri_tags
 * @property string doc_cert_tags
 * @property string doc_price
 * @property string doc_hos_tags
 * @property string|\Carbon\Carbon created_at
 * @property string|\Carbon\Carbon updated_at
 * @property string|\Carbon\Carbon deleted_at
 */
class Doc extends Eloquent
{
     use ElasticquentTrait;
   

    public $table = 'gw_doctors';
    
    public $timestamps = false;


    protected $primaryKey = 'doc_id';

    public $fillable = [
        'doc_spec_id',
        'doc_user_id',
        'doc_fullname',
        'doc_img',
        'doc_yoe',
        'doc_brief',
        'doc_add_1',
        'doc_add_2',
        'doc_city',
        'doc_state',
        'doc_country',
        'doc_long',
        'doc_lat',
        'doc_phone',
        'doc_mobile',
        'doc_fax',
        'doc_email',
        'doc_ps',
        'doc_specia_tags',
        'doc_edu',
        'doc_curri_tags',
        'doc_cert_tags',
        'doc_price',
        'doc_hos_tags',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

     protected $mappingProperties = array(
    'doc_fullname' => [
      'type' => 'string',
      "analyzer" => "standard",
    ],
    'doc_brief' => [
      'type' => 'string',
      "analyzer" => "standard",
    ],
    'doc_ps' => [
      'type' => 'string',
      "analyzer" => "stop",
    ],
  );
   

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'doc_id' => 'integer',
        'doc_spec_id' => 'integer',
        'doc_user_id' => 'integer',
        'doc_fullname' => 'string',
        'doc_img' => 'string',
        'doc_yoe' => 'integer',
        'doc_brief' => 'string',
        'doc_add_1' => 'string',
        'doc_add_2' => 'string',
        'doc_city' => 'string',
        'doc_state' => 'string',
        'doc_country' => 'string',
        'doc_long' => 'string',
        'doc_lat' => 'string',
        'doc_phone' => 'string',
        'doc_mobile' => 'string',
        'doc_fax' => 'string',
        'doc_email' => 'string',
        'doc_ps' => 'string',
        'doc_specia_tags' => 'string',
        'doc_edu' => 'string',
        'doc_curri_tags' => 'string',
        'doc_cert_tags' => 'string',
        'doc_price' => 'string',
        'doc_hos_tags' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
?>