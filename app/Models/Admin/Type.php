<?php

namespace App\Models\Admin;

use Eloquent as Model;

/**
 * Class Type
 * @package App\Models\Admin
 * @version October 11, 2018, 11:59 am UTC
 *
 * @property string type_name
 * @property string type_img
 */
class Type extends Model
{

    public $table = 'gw_type';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'type_id';

    public $fillable = [
        'type_name',
        'type_img'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'type_id' => 'integer',
        'type_name' => 'string',
        'type_img' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
