<?php

namespace App\Models\Admin;

use Eloquent as Model;

/**
 * Class Hotels
 * @package App\Models\Admin
 * @version October 11, 2018, 11:23 am UTC
 *
 * @property string hot_name
 * @property integer hot_type_id
 * @property string hot_add_1
 * @property string hot_add_2
 * @property string hot_city
 * @property string hot_country
 * @property string hot_long
 * @property string hot_lat
 * @property string hot_phone
 * @property string hot_mobile
 * @property string hot_fax
 * @property string hot_email
 * @property string hot_website
 * @property string hot_brief
 * @property string hot_img
 * @property string hot_desc
 */
class Hotels extends Model
{

    public $table = 'gw_hotels';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'hot_id';

    public $fillable = [
        'hot_name',
        'hot_type_id',
        'hot_add_1',
        'hot_add_2',
        'hot_city',
        'hot_country',
        'hot_long',
        'hot_lat',
        'hot_phone',
        'hot_mobile',
        'hot_fax',
        'hot_email',
        'hot_website',
        'hot_brief',
        'hot_img',
        'hot_desc'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'hot_id' => 'integer',
        'hot_name' => 'string',
        'hot_type_id' => 'integer',
        'hot_add_1' => 'string',
        'hot_add_2' => 'string',
        'hot_city' => 'string',
        'hot_country' => 'string',
        'hot_long' => 'string',
        'hot_lat' => 'string',
        'hot_phone' => 'string',
        'hot_mobile' => 'string',
        'hot_fax' => 'string',
        'hot_email' => 'string',
        'hot_website' => 'string',
        'hot_brief' => 'string',
        'hot_img' => 'string',
        'hot_desc' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
