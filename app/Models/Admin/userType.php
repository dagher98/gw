<?php

namespace App\Models\Admin;

use Eloquent as Model;

/**
 * Class userType
 * @package App\Models\Admin
 * @version October 12, 2018, 12:54 pm UTC
 *
 * @property string ut_name
 */
class userType extends Model
{

    public $table = 'gw_usertype';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'ut_id';

    public $fillable = [
        'ut_name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'ut_id' => 'integer',
        'ut_name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
