<?php

namespace App\Models\Admin;

use Eloquent as Model;

/**
 * Class Speciality
 * @package App\Models\Admin
 * @version October 12, 2018, 9:13 am UTC
 *
 * @property string spec_name
 * @property string spec_img
 * @property string spec_desc
 */
class Speciality extends Model
{

    public $table = 'gw_speciality';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $primaryKey = 'spec_id';

    public $fillable = [
        'spec_name',
        'spec_img',
        'spec_desc'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'spec_id' => 'integer',
        'spec_name' => 'string',
        'spec_img' => 'string',
        'spec_desc' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
