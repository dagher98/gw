<?php

namespace App\DataTables\Admin;

use App\Models\Admin\Hotels;
use App\Models\Admin\Type;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class HotelsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'admin.hotels.datatables_actions');
                       
                           
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Hotels $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'desc']],
                'buttons' => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
   
    
        return [
            'hot_name',
            'hot_img'=>['data' => 'hot_img','render' => '"<img src=\"'.url('storage/images').'/"+data+"\" height=\"50\"/>"'],
           //'hot_type_id'=>['data'=>'hot_type_id','render' =>'"'.Type::all()['data']->type_name.'"'],
            'hot_city',
            'hot_phone',
            'hot_mobile',
            'hot_fax'=>['data'=>'hot_fax','render' =>'"\'"+data+"\'"'],
            'hot_email',
            'hot_website'
            
        ];
    }

    
    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'hotelsdatatable_' . time();
    }
}
