<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Posts\DoctorDB;
use DB;

class TourismController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post= DB::table('gw_hotels')
                  ->join('gw_type', 'gw_hotels.hot_type_id', '=', 'gw_type.type_id')
                ->get();
          $pagename="Hotels";
              $pagetitle="";
               
            
    return view('tourism.tourism',['database'=>$post])->with(['pagename'=>$pagename,'pagetitle'=>$pagetitle]);
              
      
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $posts= DB::table('gw_hotels')
             ->where('hot_id','=' ,$id)
              ->join('gw_type', 'gw_hotels.hot_type_id', '=', 'gw_type.type_id')
                ->get();
         $pagename="Hotels";
            foreach($posts as $post){
               $pagetitle=$post->hot_name;   
            }
               
            
    return view('tourism.tourismdetails')->with(['pagename'=>$pagename,'pagetitle'=>$pagetitle ,'database'=>$posts]);
              
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
