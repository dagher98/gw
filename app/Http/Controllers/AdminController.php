<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Posts\DoctorDB;
use App\Http\Controllers\Posts\SpecialityDB;
use App\Http\Controllers\Posts\HospitalDB;
use DB;
use Auth;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    $usertype='';        
    if(!isset(Auth::user()->id)){
        return redirect()->route('login');
    } 
    if(Auth::user()->usertypeid==1){
            $usertype='Patient';
    }
     if(Auth::user()->usertypeid==2){
            $usertype='Doctor';
    }  
           
    return view('admin.AdminIndex')->with(['usertype'=>$usertype,'userId'=>Auth::user()->id]);
              
     
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
                $usertype='';        
            if(!isset(Auth::user()->id)){
                return redirect()->route('login');
            } 
            if(Auth::user()->usertypeid==1){
                    $usertype='Patient';
            }
            if(Auth::user()->usertypeid==2){
                    $usertype='Doctor';
            }  
            
         return view('admin.AdminAdd')->with(['usertype'=>$usertype,'userId'=>Auth::user()->id]);;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
