<?php

namespace App\Http\Controllers;

use DB;

class PageController extends Controller
{
   
    public function getIndex() {
    
      
    return view('index.index');
        }

    public function getPatient() {

            $pagename="Patient";
            $pagetitle="";
            $database=DB::Table('gw_patient')->get();
   return view('pages.patient',['database'=>$database])->with(['pagename'=>$pagename,'pagetitle'=>$pagetitle]);
        } 

         public function getPatientdetails(Request $request) {
              $pagename="Patient";
              $pagetitle="Messages / Dr. Enzo Ferrai";
              
   return view('pages.patientdetails')->with(['pagename'=>$pagename,'pagetitle'=>$pagetitle]);
        } 
      

        
         public function getTourism() {
              $pagename="Hotels";
              $pagetitle="";
    return view('pages.tourism')->with(['pagename'=>$pagename,'pagetitle'=>$pagetitle]);
        }  
          public function getTourismdetails() {
               $pagename="Hotels";
               $pagetitle="Le Royal";   
    return view('pages.tourismdetails')->with(['pagename'=>$pagename,'pagetitle'=>$pagetitle]);
        } 
}
