<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\StaticPageDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateStaticPageRequest;
use App\Http\Requests\Admin\UpdateStaticPageRequest;
use App\Repositories\Admin\StaticPageRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class StaticPageController extends AppBaseController
{
    /** @var  StaticPageRepository */
    private $staticPageRepository;

    public function __construct(StaticPageRepository $staticPageRepo)
    {
        $this->staticPageRepository = $staticPageRepo;
    }

    /**
     * Display a listing of the StaticPage.
     *
     * @param StaticPageDataTable $staticPageDataTable
     * @return Response
     */
    public function index(StaticPageDataTable $staticPageDataTable)
    {
        return $staticPageDataTable->render('admin.static_pages.index');
    }

    /**
     * Show the form for creating a new StaticPage.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.static_pages.create');
    }

    /**
     * Store a newly created StaticPage in storage.
     *
     * @param CreateStaticPageRequest $request
     *
     * @return Response
     */
    public function store(CreateStaticPageRequest $request)
    {
        $input = $request->all();

        $staticPage = $this->staticPageRepository->create($input);

        Flash::success('Static Page saved successfully.');

        return redirect(route('admin.staticPages.index'));
    }

    /**
     * Display the specified StaticPage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $staticPage = $this->staticPageRepository->findWithoutFail($id);

        if (empty($staticPage)) {
            Flash::error('Static Page not found');

            return redirect(route('admin.staticPages.index'));
        }

        return view('admin.static_pages.show')->with('staticPage', $staticPage);
    }

    /**
     * Show the form for editing the specified StaticPage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $staticPage = $this->staticPageRepository->findWithoutFail($id);

        if (empty($staticPage)) {
            Flash::error('Static Page not found');

            return redirect(route('admin.staticPages.index'));
        }

        return view('admin.static_pages.edit')->with('staticPage', $staticPage);
    }

    /**
     * Update the specified StaticPage in storage.
     *
     * @param  int              $id
     * @param UpdateStaticPageRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStaticPageRequest $request)
    {
        $staticPage = $this->staticPageRepository->findWithoutFail($id);

        if (empty($staticPage)) {
            Flash::error('Static Page not found');

            return redirect(route('admin.staticPages.index'));
        }

        $staticPage = $this->staticPageRepository->update($request->all(), $id);

        Flash::success('Static Page updated successfully.');

        return redirect(route('admin.staticPages.index'));
    }

    /**
     * Remove the specified StaticPage from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $staticPage = $this->staticPageRepository->findWithoutFail($id);

        if (empty($staticPage)) {
            Flash::error('Static Page not found');

            return redirect(route('admin.staticPages.index'));
        }

        $this->staticPageRepository->delete($id);

        Flash::success('Static Page deleted successfully.');

        return redirect(route('admin.staticPages.index'));
    }
}
