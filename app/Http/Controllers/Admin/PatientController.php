<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\PatientDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreatePatientRequest;
use App\Http\Requests\Admin\UpdatePatientRequest;
use App\Repositories\Admin\PatientRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Support\Facades\Storage;
class PatientController extends AppBaseController
{
    /** @var  PatientRepository */
    private $patientRepository;

    public function __construct(PatientRepository $patientRepo)
    {
        $this->patientRepository = $patientRepo;
    }

    /**
     * Display a listing of the Patient.
     *
     * @param PatientDataTable $patientDataTable
     * @return Response
     */
    public function index(PatientDataTable $patientDataTable)
    {
        return $patientDataTable->render('admin.patients.index');
    }

    /**
     * Show the form for creating a new Patient.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.patients.create');
    }

    /**
     * Store a newly created Patient in storage.
     *
     * @param CreatePatientRequest $request
     *
     * @return Response
     */
    public function store(CreatePatientRequest $request)
    {
        $input = $request->all();
         
          if ($request->hasFile('pat_img')) {
         $file = $request->file('pat_img');
         $path = $file->store('images', ['disk' => 'public']);
         $fileName = explode("/",$path);
       
         $input['pat_img']=$fileName[1];
         }

        $patient = $this->patientRepository->create($input);

        Flash::success('Patient saved successfully.');

        return redirect(route('admin.patients.index'));
    }

    /**
     * Display the specified Patient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $patient = $this->patientRepository->findWithoutFail($id);

        if (empty($patient)) {
            Flash::error('Patient not found');

            return redirect(route('admin.patients.index'));
        }

        return view('admin.patients.show')->with('patient', $patient);
    }

    /**
     * Show the form for editing the specified Patient.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $patient = $this->patientRepository->findWithoutFail($id);

        if (empty($patient)) {
            Flash::error('Patient not found');

            return redirect(route('admin.patients.index'));
        }

        return view('admin.patients.edit')->with('patient', $patient);
    }

    /**
     * Update the specified Patient in storage.
     *
     * @param  int              $id
     * @param UpdatePatientRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePatientRequest $request)
    {
         $patient = $this->patientRepository->findWithoutFail($id);

        if (empty($patient)) {
            Flash::error('Patient not found');

            return redirect(route('admin.patients.index'));
        }


       $input=$request->all();
         if ($request->hasFile('pat_img')) {
        Storage::delete($patient->pat_img);
         $file = $request->file('pat_img');
         $path = $file->store('images', ['disk' => 'public']);
         $fileName = explode("/",$path);
       
         $input['pat_img']=$fileName[1];
         }

         $patient = $this->patientRepository->update($input, $id);

        Flash::success('Patient updated successfully.');

        return redirect(route('admin.patients.index'));
    }

    /**
     * Remove the specified Patient from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $patient = $this->patientRepository->findWithoutFail($id);

        if (empty($patient)) {
            Flash::error('Patient not found');

            return redirect(route('admin.patients.index'));
        }

        $this->patientRepository->delete($id);

        Flash::success('Patient deleted successfully.');

        return redirect(route('admin.patients.index'));
    }
}
