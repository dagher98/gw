<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\userTypeDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateuserTypeRequest;
use App\Http\Requests\Admin\UpdateuserTypeRequest;
use App\Repositories\Admin\userTypeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class userTypeController extends AppBaseController
{
    /** @var  userTypeRepository */
    private $userTypeRepository;

    public function __construct(userTypeRepository $userTypeRepo)
    {
        $this->userTypeRepository = $userTypeRepo;
    }

    /**
     * Display a listing of the userType.
     *
     * @param userTypeDataTable $userTypeDataTable
     * @return Response
     */
    public function index(userTypeDataTable $userTypeDataTable)
    {
        return $userTypeDataTable->render('admin.user_types.index');
    }

    /**
     * Show the form for creating a new userType.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.user_types.create');
    }

    /**
     * Store a newly created userType in storage.
     *
     * @param CreateuserTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateuserTypeRequest $request)
    {
        $input = $request->all();

        $userType = $this->userTypeRepository->create($input);

        Flash::success('User Type saved successfully.');

        return redirect(route('admin.userTypes.index'));
    }

    /**
     * Display the specified userType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $userType = $this->userTypeRepository->findWithoutFail($id);

        if (empty($userType)) {
            Flash::error('User Type not found');

            return redirect(route('admin.userTypes.index'));
        }

        return view('admin.user_types.show')->with('userType', $userType);
    }

    /**
     * Show the form for editing the specified userType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $userType = $this->userTypeRepository->findWithoutFail($id);

        if (empty($userType)) {
            Flash::error('User Type not found');

            return redirect(route('admin.userTypes.index'));
        }

        return view('admin.user_types.edit')->with('userType', $userType);
    }

    /**
     * Update the specified userType in storage.
     *
     * @param  int              $id
     * @param UpdateuserTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateuserTypeRequest $request)
    {
        $userType = $this->userTypeRepository->findWithoutFail($id);

        if (empty($userType)) {
            Flash::error('User Type not found');

            return redirect(route('admin.userTypes.index'));
        }

        $userType = $this->userTypeRepository->update($request->all(), $id);

        Flash::success('User Type updated successfully.');

        return redirect(route('admin.userTypes.index'));
    }

    /**
     * Remove the specified userType from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $userType = $this->userTypeRepository->findWithoutFail($id);

        if (empty($userType)) {
            Flash::error('User Type not found');

            return redirect(route('admin.userTypes.index'));
        }

        $this->userTypeRepository->delete($id);

        Flash::success('User Type deleted successfully.');

        return redirect(route('admin.userTypes.index'));
    }
}
