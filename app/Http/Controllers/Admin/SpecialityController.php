<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\SpecialityDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSpecialityRequest;
use App\Http\Requests\Admin\UpdateSpecialityRequest;
use App\Repositories\Admin\SpecialityRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Support\Facades\Storage;

class SpecialityController extends AppBaseController
{
    /** @var  SpecialityRepository */
    private $specialityRepository;

    public function __construct(SpecialityRepository $specialityRepo)
    {
        $this->specialityRepository = $specialityRepo;
    }

    /**
     * Display a listing of the Speciality.
     *
     * @param SpecialityDataTable $specialityDataTable
     * @return Response
     */
    public function index(SpecialityDataTable $specialityDataTable)
    {
        return $specialityDataTable->render('admin.specialities.index');
    }

    /**
     * Show the form for creating a new Speciality.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.specialities.create');
    }

    /**
     * Store a newly created Speciality in storage.
     *
     * @param CreateSpecialityRequest $request
     *
     * @return Response
     */
    public function store(CreateSpecialityRequest $request)
    {
        $input = $request->all();

         if ($request->hasFile('spec_img')) {
         $file = $request->file('spec_img');
         $path = $file->store('images', ['disk' => 'public']);
         $fileName = explode("/",$path);
         $input['spec_img']=$fileName[1];
        }

        $speciality = $this->specialityRepository->create($input);

        Flash::success('Speciality saved successfully.');

        return redirect(route('admin.specialities.index'));
    }

    /**
     * Display the specified Speciality.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $speciality = $this->specialityRepository->findWithoutFail($id);

        if (empty($speciality)) {
            Flash::error('Speciality not found');

            return redirect(route('admin.specialities.index'));
        }

        return view('admin.specialities.show')->with('speciality', $speciality);
    }

    /**
     * Show the form for editing the specified Speciality.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $speciality = $this->specialityRepository->findWithoutFail($id);

        if (empty($speciality)) {
            Flash::error('Speciality not found');

            return redirect(route('admin.specialities.index'));
        }

        return view('admin.specialities.edit')->with('speciality', $speciality);
    }

    /**
     * Update the specified Speciality in storage.
     *
     * @param  int              $id
     * @param UpdateSpecialityRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSpecialityRequest $request)
    {
        $speciality = $this->specialityRepository->findWithoutFail($id);

        if (empty($speciality)) {
            Flash::error('Speciality not found');

            return redirect(route('admin.specialities.index'));
        }
        $input=$request->all();

          if ($request->hasFile('spec_img')) {
        Storage::delete($speciality->spec_img);
         $file = $request->file('spec_img');
         $path = $file->store('images', ['disk' => 'public']);
         $fileName = explode("/",$path);
       
         $input['spec_img']=$fileName[1];
         }

        $speciality = $this->specialityRepository->update($input, $id);

        Flash::success('Speciality updated successfully.');

        return redirect(route('admin.specialities.index'));
    }

    /**
     * Remove the specified Speciality from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $speciality = $this->specialityRepository->findWithoutFail($id);

        if (empty($speciality)) {
            Flash::error('Speciality not found');

            return redirect(route('admin.specialities.index'));
        }

        $this->specialityRepository->delete($id);

        Flash::success('Speciality deleted successfully.');

        return redirect(route('admin.specialities.index'));
    }
}
