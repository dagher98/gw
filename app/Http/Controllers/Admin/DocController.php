<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\DocDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateDocRequest;
use App\Http\Requests\Admin\UpdateDocRequest;
use App\Repositories\Admin\DocRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Support\Facades\Storage;

class DocController extends AppBaseController
{
    /** @var  DocRepository */
    private $docRepository;

    public function __construct(DocRepository $docRepo)
    {
        $this->docRepository = $docRepo;
    }

    /**
     * Display a listing of the Doc.
     *
     * @param DocDataTable $docDataTable
     * @return Response
     */
    public function index(DocDataTable $docDataTable)
    {
        return $docDataTable->render('admin.docs.index');
    }

    /**
     * Show the form for creating a new Doc.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.docs.create');
    }

    /**
     * Store a newly created Doc in storage.
     *
     * @param CreateDocRequest $request
     *
     * @return Response
     */
    public function store(CreateDocRequest $request)
    {
        $input = $request->all();

        
    if ($request->hasFile('doc_img')) {
         $file = $request->file('doc_img');
         $path = $file->store('images', ['disk' => 'public']);
         $fileName = explode("/",$path);
         $input['doc_img']=$fileName[1];
    }
     if ($request->input('doc_hos_tags')<>"") {
        $doc_hos_tags = $request->input('doc_hos_tags');
        $doc_hos_tags = implode(',', $doc_hos_tags);
        $input['doc_hos_tags'] = $doc_hos_tags;
     }
        $doc = $this->docRepository->create($input);
        Flash::success('Doc saved successfully.');

        return redirect(route('admin.docs.index'));
    
}

    /**
     * Display the specified Doc.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $doc = $this->docRepository->findWithoutFail($id);

        if (empty($doc)) {
            Flash::error('Doc not found');

            return redirect(route('admin.docs.index'));
        }

        return view('admin.docs.show')->with('doc', $doc);
    }

    /**
     * Show the form for editing the specified Doc.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $doc = $this->docRepository->findWithoutFail($id);
      

        if (empty($doc)) {
            Flash::error('Doc not found');

            return redirect(route('admin.docs.index'));
        }

        return view('admin.docs.edit')->with('doc', $doc);
    }

    /**
     * Update the specified Doc in storage.
     *
     * @param  int              $id
     * @param UpdateDocRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDocRequest $request)
    {
        $doc = $this->docRepository->findWithoutFail($id);

        if (empty($doc)) {
            Flash::error('Doc not found');

            return redirect(route('admin.docs.index'));
        }

      

       $input=$request->all();
          if ($request->hasFile('doc_img')) {
        Storage::delete($doc->doc_img);
         $file = $request->file('doc_img');
         $path = $file->store('images', ['disk' => 'public']);
         $fileName = explode("/",$path);
       
         $input['doc_img']=$fileName[1];
         }

        
        $doc_hos_tags = $request->input('doc_hos_tags');
        $doc_hos_tags = implode(',', $doc_hos_tags);
        //Assign the "mutated" news value to $input
        $input['doc_hos_tags'] = $doc_hos_tags;
       


        $doc = $this->docRepository->update($input, $id);

       

        Flash::success('Doc updated successfully.');

        return redirect(route('admin.docs.index'));
    }

    /**
     * Remove the specified Doc from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $doc = $this->docRepository->findWithoutFail($id);

        if (empty($doc)) {
            Flash::error('Doc not found');

            return redirect(route('admin.docs.index'));
        }

        $this->docRepository->delete($id);

        Flash::success('Doc deleted successfully.');

        return redirect(route('admin.docs.index'));
    }
}
