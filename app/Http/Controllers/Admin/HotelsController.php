<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Type;
use App\DataTables\Admin\HotelsDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateHotelsRequest;
use App\Http\Requests\Admin\UpdateHotelsRequest;
use App\Repositories\Admin\HotelsRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Support\Facades\Storage;

class HotelsController extends AppBaseController
{
    /** @var  HotelsRepository */
    private $hotelsRepository;

    public function __construct(HotelsRepository $hotelsRepo)
    {
        $this->hotelsRepository = $hotelsRepo;
    }

    /**
     * Display a listing of the Hotels.
     *
     * @param HotelsDataTable $hotelsDataTable
     * @return Response
     */
    public function index(HotelsDataTable $hotelsDataTable)
    {
        
        return $hotelsDataTable->render('admin.hotels.index');
    }

    /**
     * Show the form for creating a new Hotels.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.hotels.create');
    }

    /**
     * Store a newly created Hotels in storage.
     *
     * @param CreateHotelsRequest $request
     *
     * @return Response
     */
    public function store(CreateHotelsRequest $request)
    {
        $input = $request->all();

          if ($request->hasFile('hot_img')) {
         $file = $request->file('hot_img');
         $path = $file->store('images', ['disk' => 'public']);
         $fileName = explode("/",$path);
       
         $input['hot_img']=$fileName[1];
         }


        $hotels = $this->hotelsRepository->create($input);

        Flash::success('Hotels saved successfully.');

        return redirect(route('admin.hotels.index'));
    }

    /**
     * Display the specified Hotels.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $hotels = $this->hotelsRepository->findWithoutFail($id);

        if (empty($hotels)) {
            Flash::error('Hotels not found');

            return redirect(route('admin.hotels.index'));
        }

        return view('admin.hotels.show')->with('hotels', $hotels);
    }

    /**
     * Show the form for editing the specified Hotels.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $hotels = $this->hotelsRepository->findWithoutFail($id);

        if (empty($hotels)) {
            Flash::error('Hotels not found');

            return redirect(route('admin.hotels.index'));
        }

        return view('admin.hotels.edit')->with('hotels', $hotels);
    }

    /**
     * Update the specified Hotels in storage.
     *
     * @param  int              $id
     * @param UpdateHotelsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHotelsRequest $request)
    {
        $hotels = $this->hotelsRepository->findWithoutFail($id);

        if (empty($hotels)) {
            Flash::error('Hotels not found');

            return redirect(route('admin.hotels.index'));
        }

          $input=$request->all();
          if ($request->hasFile('hot_img')) {
        Storage::delete($hotels->hot_img);
         $file = $request->file('hot_img');
         $path = $file->store('images', ['disk' => 'public']);
         $fileName = explode("/",$path);
       
         $input['hot_img']=$fileName[1];
         }

        $hotels = $this->hotelsRepository->update($input, $id);

        Flash::success('Hotels updated successfully.');

        return redirect(route('admin.hotels.index'));
    }

    /**
     * Remove the specified Hotels from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $hotels = $this->hotelsRepository->findWithoutFail($id);

        if (empty($hotels)) {
            Flash::error('Hotels not found');

            return redirect(route('admin.hotels.index'));
        }

        $this->hotelsRepository->delete($id);

        Flash::success('Hotels deleted successfully.');

        return redirect(route('admin.hotels.index'));
    }
}
