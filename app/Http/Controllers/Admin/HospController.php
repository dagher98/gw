<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\HospDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateHospRequest;
use App\Http\Requests\Admin\UpdateHospRequest;
use App\Repositories\Admin\HospRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Support\Facades\Storage;

class HospController extends AppBaseController
{
    /** @var  HospRepository */
    private $hospRepository;

    public function __construct(HospRepository $hospRepo)
    {
        $this->hospRepository = $hospRepo;
    }

    /**
     * Display a listing of the Hosp.
     *
     * @param HospDataTable $hospDataTable
     * @return Response
     */
    public function index(HospDataTable $hospDataTable)
    {
        return $hospDataTable->render('admin.hosps.index');
        
    }

    /**
     * Show the form for creating a new Hosp.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.hosps.create');

    }

    /**
     * Store a newly created Hosp in storage.
     *
     * @param CreateHospRequest $request
     *
     * @return Response
     */
    public function store(CreateHospRequest $request)
    {
        $input = $request->all();

        
    if ($request->input('hos_specia_tags')<>"") {
        $hos_specia_tags = $request->input('hos_specia_tags');
        $hos_specia_tags = implode(',', $hos_specia_tags);
        $input['hos_specia_tags'] = $hos_specia_tags;
     }
     if($request->hasFile('hos_gallery_img'))
            {
             $fileNameArray=array();
            $allowedfileExtension=['pdf','jpg','png'];
            
            $files = $request->file('hos_gallery_img');

            foreach($files as $file){

            $extension = $file->getClientOriginalExtension();

            $check=in_array($extension,$allowedfileExtension);
            if($check)
            {
            // Storage::delete($hosp->hos_specia_tags);
            $path = $file->store('images', ['disk' => 'public']);
            $fileName = explode("/",$path);
            $fileNameArray[]=$fileName[1];
                     }
                 
             }
         
          $input['hos_gallery_img']= implode(",",$fileNameArray);
            }

        $hosp = $this->hospRepository->create($input);

        Flash::success('Hosp saved successfully.');

        return redirect(route('admin.hosps.index'));
    }

    /**
     * Display the specified Hosp.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $hosp = $this->hospRepository->findWithoutFail($id);

        if (empty($hosp)) {
            Flash::error('Hosp not found');

            return redirect(route('admin.hosps.index'));
        }

        return view('admin.hosps.show')->with('hosp', $hosp);
    }

    /**
     * Show the form for editing the specified Hosp.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $hosp = $this->hospRepository->findWithoutFail($id);

        if (empty($hosp)) {
            Flash::error('Hosp not found');

            return redirect(route('admin.hosps.index'));
        }

        return view('admin.hosps.edit')->with('hosp', $hosp);
    }

    /**
     * Update the specified Hosp in storage.
     *
     * @param  int              $id
     * @param UpdateHospRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHospRequest $request)
    {
        $hosp = $this->hospRepository->findWithoutFail($id);

        if (empty($hosp)) {
            Flash::error('Hosp not found');

            return redirect(route('admin.hosps.index'));
        }

        $input=$request->all();
        
       
         
         if ($request->input('hos_specia_tags')<>"") {
        $hos_specia_tags = $request->input('hos_specia_tags');
        $hos_specia_tags = implode(',', $hos_specia_tags);
        $input['hos_specia_tags'] = $hos_specia_tags;
            }

             if($request->hasFile('hos_gallery_img'))

            {
             $fileNameArray=array();
            $allowedfileExtension=['pdf','jpg','jpeg','png'];
            
            $files = $request->file('hos_gallery_img');

            foreach($files as $file){

            $extension = $file->getClientOriginalExtension();

            $check=in_array($extension,$allowedfileExtension);
            if($check)
            {
            Storage::delete($hosp->hos_specia_tags);
            $path = $file->store('images', ['disk' => 'public']);
            $fileName = explode("/",$path);
            $fileNameArray[]=$fileName[1];
                     }
                 
             }
         
         $input['hos_gallery_img']= implode(",",$fileNameArray);

            }
        $hosp = $this->hospRepository->update($input, $id);

        Flash::success('Hosp updated successfully.');

        return redirect(route('admin.hosps.index'));
    }

    /**
     * Remove the specified Hosp from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $hosp = $this->hospRepository->findWithoutFail($id);

        if (empty($hosp)) {
            Flash::error('Hosp not found');

            return redirect(route('admin.hosps.index'));
        }

        $this->hospRepository->delete($id);

        Flash::success('Hosp deleted successfully.');

        return redirect(route('admin.hosps.index'));
    }
}
