<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Posts\DoctorDB;
use DB;

class DoctorController extends AppBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts= DoctorDB::all();
        $pagename="Doctor";
        $pagetitle="";
         
               
            
    return view('doctor.doctor',['database'=>$posts])->with(['pagename'=>$pagename,'pagetitle'=>$pagetitle]);
              
      
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $posts= DoctorDB::find($id);
         
         foreach(explode(',',$posts->doc_specia_tags) as $info){
               
            $postspec[]= DB::table('gw_doctors')  
                         ->where('doc_id','=',$info)
                        ->first();
            
         }
            foreach(explode(',',$posts->doc_hos_tags) as $info){
               
            $posthosp[]= DB::table('gw_hospitals')  
                         ->where('hos_id','=',$info)
                        ->first();
            
         }  
    
     foreach(explode(',',$posts->doc_cert_tags) as $info){
               
            $postcert[]= DB::table('gw_hospitals')  
                         ->where('hos_id','=',$info)
                        ->first();
            
         }  
 
        $pagename="Doctor";
        $pagetitle=$posts->doc_name;
        
               
            
    return view('doctor.doctordetails',['database'=>$posts])->with(['pagename'=>$pagename,'pagetitle'=>$pagetitle,'postspec'=>$postspec,'posthosp'=>$posthosp,'postcert'=>$postcert]);
              
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
