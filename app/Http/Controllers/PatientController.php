<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Posts\DoctorDB;
use DB;
use DateTime;
use Auth;
use Illuminate\Support\Facades\Validator;
use \Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Input;
use App\Models\Admin\Doc;
class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

  
        $postConv= DB::table('gw_conversation')
                ->get();
         
          $pagename="Patient";
            $pagetitle="";
    //if user not signed in
    if(!isset(Auth::user()->id)){
          return view('patient.patient')->with(['pagename'=>$pagename,'pagetitle'=>$pagetitle]);           
        }
       //user type patient
     if(Auth::user()->usertypeid==1){  
         $postDoc= DB::table('gw_doctors')
                         ->where('doc_user_id','<>',0)
                         ->get();   
    if(count($postConv)==0){    
         return view('patient.patient',['databaseDoc'=>$postDoc])->with(['pagename'=>$pagename,'pagetitle'=>$pagetitle,'converstation'=>0,'usertype'=>Auth::user()->usertypeid]);
     }    
              
           $postConvUser= DB::table('gw_conv_users')
            ->select('cu_conv_id')
            ->where('cu_user_id','=' , Auth::user()->id)
            ->distinct()
            ->get();
           
      if(count($postConvUser)<>0){

         foreach($postConvUser as $key =>$post){
              
        $postCu[]= DB::table('gw_conv_users')
                 ->where('cu_conv_id','=' ,$post->cu_conv_id) 
                 ->join('gw_doctors', 'gw_conv_users.cu_user_id', '=', 'gw_doctors.doc_user_id')
                ->get(); 
          $postMsg2[]= DB::table('gw_message') 
            ->where('msg_conv_id','=' , $post->cu_conv_id)
            ->where('msg_type','<>' ,'2')
            ->first();
         $postConv2[]= DB::table('gw_conversation')     
                ->where('conv_id','=' , $post->cu_conv_id)
                ->first();
         }
        
   
          foreach($postCu as $post){ 
                $postMsg2= DB::table('gw_message') 
            //    ->where('msg_desc','<>','')
                ->where('msg_conv_id','=' , $post[0]->cu_conv_id)
                 ->where('msg_type','<>' ,'2')
                 ->get();
            foreach($postMsg2 as $post2){
                 $lastMsg=$post2->msg_desc;
                 $lastMsgTime=$post2->msg_datetime;
            }
           if(isset($lastMsg)){
             $lastMsgArray[]=$lastMsg;
             //get the date of last send msg
             $date = new DateTime($lastMsgTime);
             $myDatetime=new DateTime();
             $interval = $myDatetime->diff($date);
        if($interval->format("%y year")==0){
          if($interval->format("%m month")==0){
             if($interval->format("%d days")==0){
               if($interval->format("%h hours")==0){
                 if($interval->format("%i min")==0){
                      $lastMsgTimeArray[]="now";
                 }
                    else{
                  $lastMsgTimeArray[]=$interval->format("%i min");
                         }
                                                    }
                else{
                  $lastMsgTimeArray[]=$interval->format("%h hour");
                                                    }
          }
          else{
                   $lastMsgTimeArray[]=$interval->format("%d days");                                              
          }
        }
          else{
                  $lastMsgTimeArray[]=$interval->format("%m month");   
          }
        }
          else{
                  $lastMsgTimeArray[]=$interval->format("%y year");   
          }
      
          //end here 
         
               if(count($post)>1){
                  $namesgroup="";
                  $imagesgroup="";
                  $Index=0;
                  for($Index=0;$Index < count($post);$Index++){            
                      if($Index==count($post)-1){
                          $namesgroup.=$post[$Index]->doc_fullname;
                          $imagesgroup.=$post[$Index]->doc_img;
                      }else{
                          $namesgroup.=$post[$Index]->doc_fullname."/";  
                          $imagesgroup.=$post[$Index]->doc_img.',';  
                      }                                            
                  }
                          $NameArray[]=$namesgroup;
                          $ImgArray[]= $imagesgroup;
                        
              }
              else{
              $NameArray[]= $post[0]->doc_fullname;
              $ImgArray[]= $post[0]->doc_img;
              }
            
              
               
          }
          else{
            return view('patient.patient',['databaseConv'=>$postConv,'databaseDoc'=>$postDoc])->with(['pagename'=>$pagename,'pagetitle'=>$pagetitle,'converstation'=>0,'usertype'=>Auth::user()->usertypeid]);
      
        }
       }
        
          
        }
          else{
          
            $postMsg= DB::table('gw_message')
            ->select('msg_conv_id')
            ->where('msg_type','<>' ,'2')
            ->where('msg_user_id','=' , Auth::user()->id)
            ->distinct()
            ->get();
    
   
         
             return view('patient.patient',['databaseConv'=>$postConv,'databaseDoc'=>$postDoc,'databaseMsg'=>$postMsg])->with(['pagename'=>$pagename,'pagetitle'=>$pagetitle,'converstation'=>0,'usertype'=>Auth::user()->usertypeid]);
         
        }
   
      return view('patient.patient',['databaseConv'=>$postConv2,'databaseDoc'=>$postDoc,'databaseMsg'=>$postMsg2,'databaseCu'=>$postCu])->with(['pagename'=>$pagename,'pagetitle'=>$pagetitle,'lastMsg'=> $lastMsgArray,'lastMsgTime'=> $lastMsgTimeArray,'ImgArray'=>$ImgArray,'NameArray'=>$NameArray,'converstation'=>1,'usertype'=>Auth::user()->usertypeid]);
 
    
}
//user type doctor
else{
    
         //if user not signed in
    if(!isset(Auth::user()->id)){
          return view('patient.patient')->with(['pagename'=>$pagename,'pagetitle'=>$pagetitle]);           
        }
       //user type patient
     if(Auth::user()->usertypeid==2){  
         $postPat= DB::table('gw_patient')
                         ->where('pat_user_id','<>',0)
                         ->get();   
    if(count($postConv)==0){    
         return view('patient.patient',['databasePat'=>$postPat])->with(['pagename'=>$pagename,'pagetitle'=>$pagetitle,'converstation'=>0,'usertype'=>Auth::user()->usertypeid]);
     }  

           $postConvUser= DB::table('gw_conv_users')
            ->select('cu_conv_id')
            ->where('cu_user_id','=' , Auth::user()->id)
            ->distinct()
            ->get();
           
      if(count($postConvUser)<>0){

         foreach($postConvUser as $key =>$post){
              
        $postCu[]= DB::table('gw_conv_users')
                 ->where('cu_conv_id','=' ,$post->cu_conv_id) 
                  ->leftJoin('gw_patient', 'gw_conv_users.cu_user_id', '=', 'gw_patient.pat_user_id')
                 ->leftJoin('gw_doctors', 'gw_conv_users.cu_user_id', '=', 'gw_doctors.doc_user_id')
                ->get(); 
          $postMsg2[]= DB::table('gw_message') 
            ->where('msg_conv_id','=' , $post->cu_conv_id)
            ->where('msg_type','<>' ,'2')
            ->first();
         $postConv2[]= DB::table('gw_conversation')     
                ->where('conv_id','=' , $post->cu_conv_id)
                ->first();
         }
        
     
          
          foreach($postCu as $post){ 
                $postMsg2= DB::table('gw_message') 
              //  ->where('msg_desc','<>','')
                ->where('msg_conv_id','=' , $post[0]->cu_conv_id)
                ->where('msg_type','<>' ,'2')
                 ->get();
            foreach($postMsg2 as $post2){
                 $lastMsg=$post2->msg_desc;
                 $lastMsgTime=$post2->msg_datetime;
            }
            if(isset($lastMsg)){
             $lastMsgArray[]=$lastMsg;
             //get the date of last send msg
             $date = new DateTime($lastMsgTime);
             $myDatetime=new DateTime();
             $interval = $myDatetime->diff($date);
        if($interval->format("%y year")==0){
          if($interval->format("%m month")==0){
             if($interval->format("%d days")==0){
               if($interval->format("%h hours")==0){
                 if($interval->format("%i min")==0){
                      $lastMsgTimeArray[]="now";
                 }
                    else{
                  $lastMsgTimeArray[]=$interval->format("%i min");
                         }
                                                    }
                else{
                  $lastMsgTimeArray[]=$interval->format("%h hour");
                                                    }
          }
          else{
                   $lastMsgTimeArray[]=$interval->format("%d days");                                              
          }
        }
          else{
                  $lastMsgTimeArray[]=$interval->format("%m month");   
          }
        }
          else{
                  $lastMsgTimeArray[]=$interval->format("%y year");   
          }
      
          //end here 
           if(count($post)>2){
             $namesgroup="";
             $imagesgroup="";
             $Index=0;
             for($Index=0;$Index < count($post);$Index++){
               if($post[$Index]->pat_fullname==null){
                 if($Index==count($post)-1){
                    $namesgroup.=$post[$Index]->doc_fullname;
                    $imagesgroup.=$post[$Index]->doc_img;
                 }else{
                    $namesgroup.=$post[$Index]->doc_fullname."/";  
                    $imagesgroup.=$post[$Index]->doc_img.',';  
                 }
                  
               }else{
                 if($Index==count($post)-1){
                    $namesgroup.=$post[$Index]->pat_fullname;
                     $imagesgroup.=$post[$Index]->pat_img;
                 }else{
                    $namesgroup.=$post[$Index]->pat_fullname."/";
                     $imagesgroup.=$post[$Index]->pat_img.',';
                 }
                 
               }
                 
             }
                    $NameArray[]=$namesgroup;
                    $ImgArray[]= $imagesgroup;
                   
              }
              else{
             $NameArray[]= $post[0]->pat_fullname;
              $ImgArray[]= $post[0]->pat_img;
              }
             
               
          }
          else{
            return view('patient.patient',['databaseConv'=>$postConv,'databasePat'=>$postPat])->with(['pagename'=>$pagename,'pagetitle'=>$pagetitle,'converstation'=>0,'usertype'=>Auth::user()->usertypeid]);
          }
        }
          
        }
        else{
            $postMsg= DB::table('gw_message')
            ->select('msg_conv_id')
            ->where('msg_type','<>' ,'2')
            ->where('msg_user_id','=' , Auth::user()->id)
            ->distinct()
            ->get();
    
   
         if(count($postMsg)==0){  
             return view('patient.patient',['databaseConv'=>$postConv,'databasePat'=>$postPat,'databaseMsg'=>$postMsg])->with(['pagename'=>$pagename,'pagetitle'=>$pagetitle,'converstation'=>0,'usertype'=>Auth::user()->usertypeid]);
         }  
        }
   
      return view('patient.patient',['databaseConv'=>$postConv2,'databasePat'=>$postPat,'databaseMsg'=>$postMsg2,'databaseCu'=>$postCu])->with(['pagename'=>$pagename,'pagetitle'=>$pagetitle,'lastMsg'=> $lastMsgArray,'lastMsgTime'=> $lastMsgTimeArray,'ImgArray'=>$ImgArray,'NameArray'=>$NameArray,'converstation'=>1,'usertype'=>Auth::user()->usertypeid]);
 
         }
    }
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      

    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       
        $content= $request->input('content');
        $doctors= $request->input('doctors');
        $convid= $request->input('convid');
        $messageType=1;
      if($doctors==null){
            $doctors="";
      }
      else{
        $messageType=3;
        $Date=new DateTime();
        DB::table('gw_conv_users')->insert(
        ['cu_user_id'=>$doctors,'cu_conv_id'=>$convid,'cu_Datetime'=>$Date]
        );
        $doctorName=Doc::where('doc_user_id',$doctors)->first();

        DB::table('gw_message')->insert(
        ['msg_desc'=>$doctorName->doc_fullname." Joined the Coversation On ".$Date->format('Y-m-d H:i:s'),'msg_attach_name'=>"",'msg_datetime'=>$Date,'msg_attach_tags'=> "",'msg_conv_id'=>$convid,'msg_user_id'=> Auth::user()->id,'msg_type'=>$messageType]
        );
        return redirect()->action('PatientController@show',$convid);
      }

      if($content==null){
         $messageType=1;
            $content="";
      }
      
    
       if ($request->file('uploadfile') == null) {
            $path = "";
             $filename= "";
        }
       else{
             $file= Input::file('uploadfile')->getClientOriginalName();
             $filename = pathinfo($file, PATHINFO_FILENAME);
           
            $file = request()->file('uploadfile');
            $path = $file->store('attachements', ['disk' => 'public']);
        //    $path = $request->file('uploadfile')->store('public/attachements');  
        }
      
      //  $path = $request->file('upload')->store('attachements');
         DB::table('gw_message')->insert(
        ['msg_desc'=>$content,'msg_attach_name'=>$filename,'msg_datetime'=>new DateTime(),'msg_attach_tags'=> $path,'msg_conv_id'=>$convid,'msg_user_id'=> Auth::user()->id,'msg_type'=>$messageType]
        );
        
        return redirect()->action('PatientController@show',$convid);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $postConv= DB::table('gw_conversation')
                ->where('conv_id','=' ,$id)
                ->get();

        $postMsg= DB::table('gw_message')
                    ->join('users', 'gw_message.msg_user_id', '=', 'users.id')
                    ->where('msg_type','<>' ,'2')
                    ->orderBy('msg_datetime', 'asc')
                    ->where('msg_conv_id','=' ,$id)
                    ->get();   

         $postUs= DB::table('gw_conv_users')
                ->where('cu_conv_id','=' ,$id)
                 ->join('users', 'gw_conv_users.cu_user_id', '=', 'users.id')
                ->get();
       $postUsId = DB::table('gw_conv_users')
                ->where('cu_conv_id','=' ,$id)
                 ->join('users', 'gw_conv_users.cu_user_id', '=', 'users.id')
                ->pluck('id');
         $pagename="Patient";
         $pagetitle="";

          foreach($postMsg as $key=>$post){  
            
              if($post->usertypeid==1){   
              $postUser= DB::table('gw_patient')
                ->where('pat_user_id','=' ,$post->id)
                ->first();
                $postMsg[$key]->img=$postUser->pat_img;
                $postMsg[$key]->name=$postUser->pat_fullname;
             }  
               if($post->usertypeid==2){   
              $postUser= DB::table('gw_doctors')
                ->where('doc_user_id','=' ,$post->id)
                ->first();
                $postMsg[$key]->img=$postUser->doc_img;
                $postMsg[$key]->name=$postUser->doc_fullname;
             }     

         }
        

         foreach($postUs as $post){          
             if($post->id<>Auth::user()->id){   
                $pagetitle="Messages / ".$post->name; 
             }             
         }
          $doctors= Doc::whereNotIn('doc_user_id',$postUsId)->get();
       
                    
    return view('patient.patientdetails')->with(['pagename'=>$pagename,'pagetitle'=>$pagetitle ,'databaseConv'=>$postConv ,'databaseMsg'=>$postMsg,'doctorsList'=>$doctors,'auth_id'=> Auth::user()->id]);
              
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          $postCU= DB::table('gw_conv_users')
                 ->where('cu_user_id',Auth::user()->id)
                ->get();
        if(count($postCU)>0){
        foreach($postCU as $key => $dataCu) {
            $postConv= DB::table('gw_conv_users')
           // ->join('users', 'gw_message.msg_user_id', '=', 'users.id')
             ->where('cu_conv_id','=' ,$dataCu->cu_conv_id)
            ->get();
             foreach($postConv as $key => $dataConv) {
             
           
            if($dataConv->cu_user_id==$id ){
               
                 return redirect()->action(
            'PatientController@show', ['id' =>  $dataCu->cu_conv_id]
                                         );
             }
          
           }
       
        }
     //   return  $postConv;
        $conv_id = DB::table('gw_conversation')->insertGetId(
        ['conv_DateTime' => new DateTime()]
        );
         DB::table('gw_conv_users')->insert(
        ['cu_user_id'=>$id,'cu_conv_id'=>$conv_id,'cu_DateTime' => new DateTime()]
        );
       DB::table('gw_conv_users')->insert(
        ['cu_user_id'=>Auth::user()->id,'cu_conv_id'=>$conv_id,'cu_DateTime' => new DateTime()]
        );
          DB::table('gw_message')->insert(
        ['msg_desc'=>"",'msg_attach_name'=>"",'msg_datetime'=>new DateTime(),'msg_attach_tags'=> "",'msg_conv_id'=>$conv_id,'msg_user_id'=> Auth::user()->id,'msg_type'=>1]
        );
       return redirect()->action(
            'PatientController@show', ['id' =>  $conv_id]
        );
    }
      else{

      $conv_id = DB::table('gw_conversation')->insertGetId(
        ['conv_DateTime' => new DateTime()]
        );
         DB::table('gw_conv_users')->insert(
        ['cu_user_id'=>$id,'cu_conv_id'=>$conv_id,'cu_DateTime' => new DateTime()]
        );
       DB::table('gw_conv_users')->insert(
        ['cu_user_id'=>Auth::user()->id,'cu_conv_id'=>$conv_id,'cu_DateTime' => new DateTime()]
        );
          DB::table('gw_message')->insert(
        ['msg_desc'=>"",'msg_attach_name'=>"",'msg_datetime'=>new DateTime(),'msg_attach_tags'=> "",'msg_conv_id'=>$conv_id,'msg_user_id'=> Auth::user()->id,'msg_type'=>1]
        );
       return redirect()->action(
            'PatientController@show', ['id' =>  $conv_id]
        );
      }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
