<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Posts\HospitalDB;
use DB;

class HospitalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts= HospitalDB::all();
        $pagename="Hospital";
        $pagetitle="";
         
               
            
    return view('hospital.hospital',['database'=>$posts])->with(['pagename'=>$pagename,'pagetitle'=>$pagetitle]);
              

    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $posts= HospitalDB::find($id);
         
         foreach(explode(',',$posts->hos_specia_tags) as $info){
               
            $postspec[]= DB::table('gw_speciality')  
                         ->where('spec_id','=',$info)
                        ->first();
            
         }
              
    
   
 
        $pagename="Hospital";
        $pagetitle=$posts->hos_name;
        
               
            
    return view('hospital.hospitaldetails',['database'=>$posts])->with(['pagename'=>$pagename,'pagetitle'=>$pagetitle,'postspec'=>$postspec]);
              
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ShowLoc($id)
    {
     
        $posts= HospitalDB::where('hos_country',$id)->get();
        $pagename="Hospital";
        $pagetitle="";
         
          
            
     return view('hospital.hospital',['database'=>$posts])->with(['pagename'=>$pagename,'pagetitle'=>$pagetitle]);
              

    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
