<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Posts\DoctorDB;
use App\Http\Controllers\Posts\SpecialityDB;
use App\Http\Controllers\Posts\HospitalDB;
use DB;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $postdoc= DB::table('gw_doctors')
            ->join('gw_speciality', 'gw_doctors.doc_spec_id', '=', 'gw_speciality.spec_id')
             ->limit(4)
            ->get();
       $postspeciality= SpecialityDB::all();
       $posthos= HospitalDB::all();
           
      foreach($postspeciality as $post){       
    
             $postcountdoc[]= DB::table('gw_doctors')  
                         ->where('doc_spec_id','=' ,$post->spec_id)      
                            ->count();
            
        
    }
            
            
    return view('index.index')->with(['postdoc'=>$postdoc,'postspeciality'=>$postspeciality,'posthos'=>$posthos,'postcountdoc'=>$postcountdoc]);
              
     
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
