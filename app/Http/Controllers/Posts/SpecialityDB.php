<?php

namespace App\Http\Controllers\Posts;

use Illuminate\Database\Eloquent\Model;

class SpecialityDB extends Model
{
    protected $table="gw_speciality";
    public $primaryKey="spec_id";
    public $timestamps =true;
}
