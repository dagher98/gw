<?php

namespace App\Http\Controllers\Posts;

use Illuminate\Database\Eloquent\Model;

class DoctorDB extends Model
{
    protected $table="gw_doctors";
    public $primaryKey="doc_id";
    public $timestamps =true;
}
