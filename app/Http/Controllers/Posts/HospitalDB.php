<?php

namespace App\Http\Controllers\Posts;

use Illuminate\Database\Eloquent\Model;

class HospitalDB extends Model
{
    protected $table="gw_hospitals";
    public $primaryKey="hos_id";
    public $timestamps =true;
}
