<?php

namespace App\Repositories;

use App\Models\DoctorAdmin;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class DoctorAdminRepository
 * @package App\Repositories
 * @version October 10, 2018, 3:18 pm UTC
 *
 * @method DoctorAdmin findWithoutFail($id, $columns = ['*'])
 * @method DoctorAdmin find($id, $columns = ['*'])
 * @method DoctorAdmin first($columns = ['*'])
*/
class DoctorAdminRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'doc_spec_id',
        'doc_user_id',
        'doc_fullname',
        'doc_img',
        'doc_yoe',
        'doc_brief',
        'doc_add_1',
        'doc_add_2',
        'doc_city',
        'doc_state',
        'doc_country',
        'doc_long',
        'doc_lat',
        'doc_phone',
        'doc_mobile',
        'doc_fax',
        'doc_email',
        'doc_ps',
        'doc_specia_tags',
        'doc_edu',
        'doc_curri_tags',
        'doc_cert_tags',
        'doc_price',
        'doc_hos_tags'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DoctorAdmin::class;
    }
}
