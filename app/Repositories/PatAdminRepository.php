<?php

namespace App\Repositories;

use App\Models\PatAdmin;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PatAdminRepository
 * @package App\Repositories
 * @version October 10, 2018, 4:04 pm UTC
 *
 * @method PatAdmin findWithoutFail($id, $columns = ['*'])
 * @method PatAdmin find($id, $columns = ['*'])
 * @method PatAdmin first($columns = ['*'])
*/
class PatAdminRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pat_fullname',
        'pat_img',
        'pat_bithday',
        'pat_country',
        'pat_city',
        'pat_phone',
        'pat_email',
        'pat_mobile',
        'pat_fax',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PatAdmin::class;
    }
}
