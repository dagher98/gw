<?php

namespace App\Repositories;

use App\Models\DocAdminController;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class DocAdminControllerRepository
 * @package App\Repositories
 * @version October 10, 2018, 2:51 pm UTC
 *
 * @method DocAdminController findWithoutFail($id, $columns = ['*'])
 * @method DocAdminController find($id, $columns = ['*'])
 * @method DocAdminController first($columns = ['*'])
*/
class DocAdminControllerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'doc_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DocAdminController::class;
    }
}
