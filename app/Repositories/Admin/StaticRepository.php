<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Static;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class StaticRepository
 * @package App\Repositories\Admin
 * @version October 12, 2018, 12:54 pm UTC
 *
 * @method Static findWithoutFail($id, $columns = ['*'])
 * @method Static find($id, $columns = ['*'])
 * @method Static first($columns = ['*'])
*/
class StaticRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'st_type',
        'st_desc',
        'st_img'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Static::class;
    }
}
