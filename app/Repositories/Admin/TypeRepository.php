<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Type;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TypeRepository
 * @package App\Repositories\Admin
 * @version October 11, 2018, 11:59 am UTC
 *
 * @method Type findWithoutFail($id, $columns = ['*'])
 * @method Type find($id, $columns = ['*'])
 * @method Type first($columns = ['*'])
*/
class TypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'type_name',
        'type_img'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Type::class;
    }
}
