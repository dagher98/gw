<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Speciality;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SpecialityRepository
 * @package App\Repositories\Admin
 * @version October 12, 2018, 9:13 am UTC
 *
 * @method Speciality findWithoutFail($id, $columns = ['*'])
 * @method Speciality find($id, $columns = ['*'])
 * @method Speciality first($columns = ['*'])
*/
class SpecialityRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'spec_name',
        'spec_img',
        'spec_desc'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Speciality::class;
    }
}
