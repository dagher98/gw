<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Hotels;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class HotelsRepository
 * @package App\Repositories\Admin
 * @version October 11, 2018, 11:23 am UTC
 *
 * @method Hotels findWithoutFail($id, $columns = ['*'])
 * @method Hotels find($id, $columns = ['*'])
 * @method Hotels first($columns = ['*'])
*/
class HotelsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'hot_name',
        'hot_type_id',
        'hot_add_1',
        'hot_add_2',
        'hot_city',
        'hot_country',
        'hot_long',
        'hot_lat',
        'hot_phone',
        'hot_mobile',
        'hot_fax',
        'hot_email',
        'hot_website',
        'hot_brief',
        'hot_img',
        'hot_desc'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Hotels::class;
    }
}
