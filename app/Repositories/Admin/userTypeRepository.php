<?php

namespace App\Repositories\Admin;

use App\Models\Admin\userType;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class userTypeRepository
 * @package App\Repositories\Admin
 * @version October 12, 2018, 12:54 pm UTC
 *
 * @method userType findWithoutFail($id, $columns = ['*'])
 * @method userType find($id, $columns = ['*'])
 * @method userType first($columns = ['*'])
*/
class userTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ut_name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return userType::class;
    }
}
