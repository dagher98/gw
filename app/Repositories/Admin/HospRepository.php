<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Hosp;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class HospRepository
 * @package App\Repositories\Admin
 * @version October 11, 2018, 6:18 am UTC
 *
 * @method Hosp findWithoutFail($id, $columns = ['*'])
 * @method Hosp find($id, $columns = ['*'])
 * @method Hosp first($columns = ['*'])
*/
class HospRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'hos_name',
        'hos_since',
        'hos_brief',
        'hos_add_1',
        'hos_add_2',
        'hos_city',
        'hos_state',
        'hos_country',
        'hos_long',
        'hos_lat',
        'hos_phone',
        'hos_mobile',
        'hos_fax',
        'hos_email',
        'hos_workhour',
        'hos_website',
        'hos_specia_tags',
        'hos_gallery_img',
        'hos_doc_count',
        'hos_nurses_count',
        'hos_room_count'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Hosp::class;
    }
}
