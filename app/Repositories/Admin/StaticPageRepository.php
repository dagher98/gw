<?php

namespace App\Repositories\Admin;

use App\Models\Admin\StaticPage;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class StaticPageRepository
 * @package App\Repositories\Admin
 * @version October 12, 2018, 12:56 pm UTC
 *
 * @method StaticPage findWithoutFail($id, $columns = ['*'])
 * @method StaticPage find($id, $columns = ['*'])
 * @method StaticPage first($columns = ['*'])
*/
class StaticPageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'st_type',
        'st_desc',
        'st_img'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StaticPage::class;
    }
}
