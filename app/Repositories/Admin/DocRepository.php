<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Doc;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class DocRepository
 * @package App\Repositories\Admin
 * @version October 10, 2018, 4:19 pm UTC
 *
 * @method Doc findWithoutFail($id, $columns = ['*'])
 * @method Doc find($id, $columns = ['*'])
 * @method Doc first($columns = ['*'])
*/
class DocRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'doc_spec_id',
        'doc_user_id',
        'doc_fullname',
        'doc_img',
        'doc_yoe',
        'doc_brief',
        'doc_add_1',
        'doc_add_2',
        'doc_city',
        'doc_state',
        'doc_country',
        'doc_long',
        'doc_lat',
        'doc_phone',
        'doc_mobile',
        'doc_fax',
        'doc_email',
        'doc_ps',
        'doc_specia_tags',
        'doc_edu',
        'doc_curri_tags',
        'doc_cert_tags',
        'doc_price',
        'doc_hos_tags',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Doc::class;
    }
}
