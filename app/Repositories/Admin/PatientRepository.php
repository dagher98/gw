<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Patient;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PatientRepository
 * @package App\Repositories\Admin
 * @version October 11, 2018, 6:20 am UTC
 *
 * @method Patient findWithoutFail($id, $columns = ['*'])
 * @method Patient find($id, $columns = ['*'])
 * @method Patient first($columns = ['*'])
*/
class PatientRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pat_fullname',
        'pat_img',
        'pat_bithday',
        'pat_country',
        'pat_city',
        'pat_phone',
        'pat_email',
        'pat_mobile',
        'pat_fax',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Patient::class;
    }
}
