<?php

namespace App\Repositories;

use App\Models\testimage;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class testimageRepository
 * @package App\Repositories
 * @version October 11, 2018, 6:43 am UTC
 *
 * @method testimage findWithoutFail($id, $columns = ['*'])
 * @method testimage find($id, $columns = ['*'])
 * @method testimage first($columns = ['*'])
*/
class testimageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tstiameg'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return testimage::class;
    }
}
