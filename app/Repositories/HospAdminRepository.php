<?php

namespace App\Repositories;

use App\Models\HospAdmin;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class HospAdminRepository
 * @package App\Repositories
 * @version October 10, 2018, 3:23 pm UTC
 *
 * @method HospAdmin findWithoutFail($id, $columns = ['*'])
 * @method HospAdmin find($id, $columns = ['*'])
 * @method HospAdmin first($columns = ['*'])
*/
class HospAdminRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'hos_name',
        'hos_since',
        'hos_brief',
        'hos_add_1',
        'hos_add_2',
        'hos_city',
        'hos_state',
        'hos_country',
        'hos_long',
        'hos_lat',
        'hos_phone',
        'hos_mobile',
        'hos_fax',
        'hos_email',
        'hos_workhour',
        'hos_website',
        'hos_specia_tags',
        'hos_gallery_img',
        'hos_doc_count',
        'hos_nurses_count',
        'hos_room_count'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return HospAdmin::class;
    }
}
