<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('gw_doctors')->insert([
            'doc_fullname' => str_random(10),
            'doc_spec_id' => rand(1,8),
             'doc_img' => 'doctorimg.png',
            'doc_brief' => str_random(20)."<br/>".str_random(20)."<br/>".str_random(15),
             'doc_yoe' => rand(1,30),
             'doc_hos_tags' => rand(37,54).",".rand(37,54).",".rand(37,54).",".rand(37,54),
             'doc_cert_tags' => rand(37,54).",".rand(37,54).",".rand(37,54).",".rand(37,54),
            'doc_price' => rand(1,30)."$",
            'doc_phone' => "0".rand(1,9)." ".rand(000,999)." ".rand(000,999),
               
            
        ]);
    }
}
