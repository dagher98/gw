<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/tourismdetails', 'PageController@getTourismdetails');
Route::get('/tourism', 'PageController@getTourism');
Route::get('/doctordetails', 'PageController@getDoctordetails');
Route::get('/doctor', 'PageController@getDoctor');
Route::get('/hospitaldetails', 'PageController@getHospitaldetails');
Route::get('/hospital', 'PageController@getHospital');
Route::get('/patientdetails','PageController@getPatientdetails');
Route::get('/patient','PageController@getPatient');
Route::get('/plan','PlanController@index');


Route::resource('tourism', 'TourismController');
Route::resource('patient', 'PatientController');
Route::resource('hospital', 'HospitalController');
Route::resource('doctor', 'DoctorController');
Route::get('hospital/{hosps}/ShowLoc', 'HospitalController@ShowLoc');
Route::resource('', 'IndexController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('admin/hosps', ['as'=> 'admin.hosps.index', 'uses' => 'Admin\HospController@index']);
Route::post('admin/hosps', ['as'=> 'admin.hosps.store', 'uses' => 'Admin\HospController@store']);
Route::get('admin/hosps/create', ['as'=> 'admin.hosps.create', 'uses' => 'Admin\HospController@create']);
Route::put('admin/hosps/{hosps}', ['as'=> 'admin.hosps.update', 'uses' => 'Admin\HospController@update']);
Route::patch('admin/hosps/{hosps}', ['as'=> 'admin.hosps.update', 'uses' => 'Admin\HospController@update']);
Route::delete('admin/hosps/{hosps}', ['as'=> 'admin.hosps.destroy', 'uses' => 'Admin\HospController@destroy']);
Route::get('admin/hosps/{hosps}', ['as'=> 'admin.hosps.show', 'uses' => 'Admin\HospController@show']);
Route::get('admin/hosps/{hosps}/edit', ['as'=> 'admin.hosps.edit', 'uses' => 'Admin\HospController@edit']);


Route::get('admin/docs', ['as'=> 'admin.docs.index', 'uses' => 'Admin\DocController@index']);
Route::post('admin/docs', ['as'=> 'admin.docs.store', 'uses' => 'Admin\DocController@store']);
Route::get('admin/docs/create', ['as'=> 'admin.docs.create', 'uses' => 'Admin\DocController@create']);
Route::put('admin/docs/{docs}', ['as'=> 'admin.docs.update', 'uses' => 'Admin\DocController@update']);
Route::patch('admin/docs/{docs}', ['as'=> 'admin.docs.update', 'uses' => 'Admin\DocController@update']);
Route::delete('admin/docs/{docs}', ['as'=> 'admin.docs.destroy', 'uses' => 'Admin\DocController@destroy']);
Route::get('admin/docs/{docs}', ['as'=> 'admin.docs.show', 'uses' => 'Admin\DocController@show']);
Route::get('admin/docs/{docs}/edit', ['as'=> 'admin.docs.edit', 'uses' => 'Admin\DocController@edit']);



Route::get('admin/patients', ['as'=> 'admin.patients.index', 'uses' => 'Admin\PatientController@index']);
Route::post('admin/patients', ['as'=> 'admin.patients.store', 'uses' => 'Admin\PatientController@store']);
Route::get('admin/patients/create', ['as'=> 'admin.patients.create', 'uses' => 'Admin\PatientController@create']);
Route::put('admin/patients/{patients}', ['as'=> 'admin.patients.update', 'uses' => 'Admin\PatientController@update']);
Route::patch('admin/patients/{patients}', ['as'=> 'admin.patients.update', 'uses' => 'Admin\PatientController@update']);
Route::delete('admin/patients/{patients}', ['as'=> 'admin.patients.destroy', 'uses' => 'Admin\PatientController@destroy']);
Route::get('admin/patients/{patients}', ['as'=> 'admin.patients.show', 'uses' => 'Admin\PatientController@show']);
Route::get('admin/patients/{patients}/edit', ['as'=> 'admin.patients.edit', 'uses' => 'Admin\PatientController@edit']);




Route::get('admin/hotels', ['as'=> 'admin.hotels.index', 'uses' => 'Admin\HotelsController@index']);
Route::post('admin/hotels', ['as'=> 'admin.hotels.store', 'uses' => 'Admin\HotelsController@store']);
Route::get('admin/hotels/create', ['as'=> 'admin.hotels.create', 'uses' => 'Admin\HotelsController@create']);
Route::put('admin/hotels/{hotels}', ['as'=> 'admin.hotels.update', 'uses' => 'Admin\HotelsController@update']);
Route::patch('admin/hotels/{hotels}', ['as'=> 'admin.hotels.update', 'uses' => 'Admin\HotelsController@update']);
Route::delete('admin/hotels/{hotels}', ['as'=> 'admin.hotels.destroy', 'uses' => 'Admin\HotelsController@destroy']);
Route::get('admin/hotels/{hotels}', ['as'=> 'admin.hotels.show', 'uses' => 'Admin\HotelsController@show']);
Route::get('admin/hotels/{hotels}/edit', ['as'=> 'admin.hotels.edit', 'uses' => 'Admin\HotelsController@edit']);


Route::get('admin/types', ['as'=> 'admin.types.index', 'uses' => 'Admin\TypeController@index']);
Route::post('admin/types', ['as'=> 'admin.types.store', 'uses' => 'Admin\TypeController@store']);
Route::get('admin/types/create', ['as'=> 'admin.types.create', 'uses' => 'Admin\TypeController@create']);
Route::put('admin/types/{types}', ['as'=> 'admin.types.update', 'uses' => 'Admin\TypeController@update']);
Route::patch('admin/types/{types}', ['as'=> 'admin.types.update', 'uses' => 'Admin\TypeController@update']);
Route::delete('admin/types/{types}', ['as'=> 'admin.types.destroy', 'uses' => 'Admin\TypeController@destroy']);
Route::get('admin/types/{types}', ['as'=> 'admin.types.show', 'uses' => 'Admin\TypeController@show']);
Route::get('admin/types/{types}/edit', ['as'=> 'admin.types.edit', 'uses' => 'Admin\TypeController@edit']);


Route::get('admin/users', ['as'=> 'admin.users.index', 'uses' => 'Admin\UserController@index']);
Route::post('admin/users', ['as'=> 'admin.users.store', 'uses' => 'Admin\UserController@store']);
Route::get('admin/users/create', ['as'=> 'admin.users.create', 'uses' => 'Admin\UserController@create']);
Route::put('admin/users/{users}', ['as'=> 'admin.users.update', 'uses' => 'Admin\UserController@update']);
Route::patch('admin/users/{users}', ['as'=> 'admin.users.update', 'uses' => 'Admin\UserController@update']);
Route::delete('admin/users/{users}', ['as'=> 'admin.users.destroy', 'uses' => 'Admin\UserController@destroy']);
Route::get('admin/users/{users}', ['as'=> 'admin.users.show', 'uses' => 'Admin\UserController@show']);
Route::get('admin/users/{users}/edit', ['as'=> 'admin.users.edit', 'uses' => 'Admin\UserController@edit']);


Route::get('admin/specialities', ['as'=> 'admin.specialities.index', 'uses' => 'Admin\SpecialityController@index']);
Route::post('admin/specialities', ['as'=> 'admin.specialities.store', 'uses' => 'Admin\SpecialityController@store']);
Route::get('admin/specialities/create', ['as'=> 'admin.specialities.create', 'uses' => 'Admin\SpecialityController@create']);
Route::put('admin/specialities/{specialities}', ['as'=> 'admin.specialities.update', 'uses' => 'Admin\SpecialityController@update']);
Route::patch('admin/specialities/{specialities}', ['as'=> 'admin.specialities.update', 'uses' => 'Admin\SpecialityController@update']);
Route::delete('admin/specialities/{specialities}', ['as'=> 'admin.specialities.destroy', 'uses' => 'Admin\SpecialityController@destroy']);
Route::get('admin/specialities/{specialities}', ['as'=> 'admin.specialities.show', 'uses' => 'Admin\SpecialityController@show']);
Route::get('admin/specialities/{specialities}/edit', ['as'=> 'admin.specialities.edit', 'uses' => 'Admin\SpecialityController@edit']);


Route::get('admin/userTypes', ['as'=> 'admin.userTypes.index', 'uses' => 'Admin\userTypeController@index']);
Route::post('admin/userTypes', ['as'=> 'admin.userTypes.store', 'uses' => 'Admin\userTypeController@store']);
Route::get('admin/userTypes/create', ['as'=> 'admin.userTypes.create', 'uses' => 'Admin\userTypeController@create']);
Route::put('admin/userTypes/{userTypes}', ['as'=> 'admin.userTypes.update', 'uses' => 'Admin\userTypeController@update']);
Route::patch('admin/userTypes/{userTypes}', ['as'=> 'admin.userTypes.update', 'uses' => 'Admin\userTypeController@update']);
Route::delete('admin/userTypes/{userTypes}', ['as'=> 'admin.userTypes.destroy', 'uses' => 'Admin\userTypeController@destroy']);
Route::get('admin/userTypes/{userTypes}', ['as'=> 'admin.userTypes.show', 'uses' => 'Admin\userTypeController@show']);
Route::get('admin/userTypes/{userTypes}/edit', ['as'=> 'admin.userTypes.edit', 'uses' => 'Admin\userTypeController@edit']);



Route::get('admin/staticPages', ['as'=> 'admin.staticPages.index', 'uses' => 'Admin\StaticPageController@index']);
Route::post('admin/staticPages', ['as'=> 'admin.staticPages.store', 'uses' => 'Admin\StaticPageController@store']);
Route::get('admin/staticPages/create', ['as'=> 'admin.staticPages.create', 'uses' => 'Admin\StaticPageController@create']);
Route::put('admin/staticPages/{staticPages}', ['as'=> 'admin.staticPages.update', 'uses' => 'Admin\StaticPageController@update']);
Route::patch('admin/staticPages/{staticPages}', ['as'=> 'admin.staticPages.update', 'uses' => 'Admin\StaticPageController@update']);
Route::delete('admin/staticPages/{staticPages}', ['as'=> 'admin.staticPages.destroy', 'uses' => 'Admin\StaticPageController@destroy']);
Route::get('admin/staticPages/{staticPages}', ['as'=> 'admin.staticPages.show', 'uses' => 'Admin\StaticPageController@show']);
Route::get('admin/staticPages/{staticPages}/edit', ['as'=> 'admin.staticPages.edit', 'uses' => 'Admin\StaticPageController@edit']);

Route::get('elasticasearch',['uses'=>'SearchController@elasticsearchTest']);    
