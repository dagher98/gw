-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Nov 01, 2018 at 07:24 AM
-- Server version: 5.6.38
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `globalwellness_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `gw_conversation`
--

CREATE TABLE `gw_conversation` (
  `conv_id` int(11) NOT NULL,
  `conv_DateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gw_conversation`
--

INSERT INTO `gw_conversation` (`conv_id`, `conv_DateTime`) VALUES
(198, '2018-10-30 12:04:31'),
(199, '2018-10-30 12:04:34'),
(200, '2018-10-30 12:05:55'),
(201, '2018-10-30 12:09:55'),
(202, '2018-10-30 12:10:01');

-- --------------------------------------------------------

--
-- Table structure for table `gw_conv_users`
--

CREATE TABLE `gw_conv_users` (
  `cu_id` int(11) NOT NULL,
  `cu_user_id` int(11) NOT NULL,
  `cu_conv_id` int(11) NOT NULL,
  `cu_DateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gw_conv_users`
--

INSERT INTO `gw_conv_users` (`cu_id`, `cu_user_id`, `cu_conv_id`, `cu_DateTime`) VALUES
(338, 4, 198, '2018-10-30 12:04:31'),
(339, 1, 198, '2018-10-30 12:04:31'),
(340, 12, 199, '2018-10-30 12:04:34'),
(341, 1, 199, '2018-10-30 12:04:34'),
(342, 7, 200, '2018-10-30 12:05:55'),
(343, 4, 200, '2018-10-30 12:05:55'),
(344, 5, 201, '2018-10-30 12:09:55'),
(345, 1, 201, '2018-10-30 12:09:55'),
(346, 7, 202, '2018-10-30 12:10:01'),
(347, 12, 202, '2018-10-30 12:10:01');

-- --------------------------------------------------------

--
-- Table structure for table `gw_doctors`
--

CREATE TABLE `gw_doctors` (
  `doc_id` int(11) NOT NULL,
  `doc_spec_id` int(11) NOT NULL,
  `doc_user_id` int(11) NOT NULL,
  `doc_fullname` varchar(250) NOT NULL,
  `doc_img` varchar(250) DEFAULT NULL,
  `doc_yoe` int(11) DEFAULT NULL,
  `doc_brief` longtext,
  `doc_add_1` varchar(250) DEFAULT NULL,
  `doc_add_2` varchar(250) DEFAULT NULL,
  `doc_city` varchar(250) DEFAULT NULL,
  `doc_state` varchar(250) DEFAULT NULL,
  `doc_country` varchar(100) DEFAULT NULL,
  `doc_long` varchar(250) DEFAULT NULL,
  `doc_lat` varchar(250) DEFAULT NULL,
  `doc_phone` varchar(250) DEFAULT NULL,
  `doc_mobile` varchar(250) DEFAULT NULL,
  `doc_fax` varchar(250) DEFAULT NULL,
  `doc_email` varchar(250) DEFAULT NULL,
  `doc_ps` longtext,
  `doc_specia_tags` varchar(100) DEFAULT NULL,
  `doc_edu` longtext,
  `doc_curri_tags` varchar(250) DEFAULT NULL,
  `doc_cert_tags` varchar(250) DEFAULT NULL,
  `doc_price` varchar(250) DEFAULT NULL,
  `doc_hos_tags` varchar(250) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gw_doctors`
--

INSERT INTO `gw_doctors` (`doc_id`, `doc_spec_id`, `doc_user_id`, `doc_fullname`, `doc_img`, `doc_yoe`, `doc_brief`, `doc_add_1`, `doc_add_2`, `doc_city`, `doc_state`, `doc_country`, `doc_long`, `doc_lat`, `doc_phone`, `doc_mobile`, `doc_fax`, `doc_email`, `doc_ps`, `doc_specia_tags`, `doc_edu`, `doc_curri_tags`, `doc_cert_tags`, `doc_price`, `doc_hos_tags`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 4, 1, 'Dr. Enzo Ferrari', 'IeSakRt2IuNjzykjtezoHsMNRHadse4TNexqlVLa.jpeg', 12, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry', 'Kaslic ,Lebanon', 'Near Burger King', 'Kaslic', NULL, 'Lebanon', NULL, NULL, '961 (1) 537 892', NULL, '961 (1) 537 894', 'Enzo.Ferrari@doctor.com', 'Lorem Ipsum is simply dummy text of the printing and typesetting industryLorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum is simply dummy text of the printing and typesetting industry', 'Cardio,Abdominal', 'Lorem Ipsum is simply dummy text of the printing and typesetting industryLorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum is simply dummy text of the printing and typesetting industry', 'New york college', '1', '50$', '1', '2018-10-18 13:04:51', '2018-10-18 13:04:51', '2018-10-18 13:04:51'),
(4, 3, 7, 'Dr. Garry Peers', 'pFJOwKClgJ3iwPC29nxPQmmgNxsdN4mYrRudWT0g.jpeg', 19, 'Dr. Garry Peers is an urologist in Honolulu, Hawaii and is affiliated with Queen\'s Medical Center. He received his medical degree from University of the Witwatersrand and has been in practice for more than 20 years. Dr. Peers accepts several types of health insurance, listed below. He is one of 34 doctors at Queen\'s Medical Center who specialize in Urology.', '1329 Lusitana', 'St Suite 506', 'Lusitana', 'Honolulu', 'USA', NULL, NULL, '01 544 504', NULL, NULL, 'Garry.Peers@doctor.com', NULL, NULL, NULL, NULL, '41', '21$', '1,50,51,52', '2018-10-19 08:00:35', '2018-10-19 08:00:35', '2018-10-19 08:00:35'),
(15, 8, 8, 'Dr. Randal Aaberg', 'ytZS37YX5i3atLsEtKq6xcdpOyAPkf7uKOQVdjeb.png', 21, 'Dr. Randal Aaberg is an urologist in Honolulu, Hawaii and is affiliated with multiple hospitals in the area, including Kaiser Permanente Medical Center and Queen\'s Medical Center. He received his medical degree from David Geffen School of Medicine at UCLA and has been in practice for more than 20 years. He is one of 12 doctors at Kaiser Permanente Medical Center and one of 34 at Queen\'s Medical Center who specialize in Urology.', '38 Sunny Slope Rcho Sta Marg,', 'CA 92688-5556', 'Sunny Slope Rcho Sta Marg', 'CA', 'USA', NULL, NULL, '(808) 432-0000', '(808) 432-0000', '(808) 432-0000', 'Randal.Aaberg@doctor.com', NULL, NULL, NULL, NULL, NULL, '26$', '1,37,38,39', '2018-10-19 08:01:40', '2018-10-19 08:01:40', '2018-10-19 08:01:40'),
(16, 7, 9, 'Dr. Tahmina Aafreen', '9VZxubWWLXtJE1h8V3hh2zwjbWV5i6xuhem4ltFl.png', 32, 'Dr. Tahmina Aafreen is an obstetrician-gynecologist in Roseville, California and is affiliated with Kaiser Permanente Roseville Medical Center. She received her medical degree from Northwestern University Feinberg School of Medicine and has been in practice for more than 20 years. Dr. Aafreen accepts several types of health insurance, listed below. She is one of 76 doctors at Kaiser Permanente Roseville Medical Center who specialize in Obstetrics & Gynecology', '1600 Eureka Rd', 'Roseville, CA 95661', 'Roseville', 'CA', 'USA', NULL, NULL, '(916) 784-4050', NULL, '(916) 784-4050', 'TahminaAafreen@doctor.com', 'Obstetricians/gynecologists (OB-GYNs) specialize in the woman\'s reproductive tract, pregnancy and childbirth.', NULL, 'When choosing a doctor, it can be helpful to review other patients\' experiences. U.S. News publishes patient experience ratings from Binary Fountain, which aggregates patient reviews from over a hundred sites to compile information about 10 different patient experience metrics. These ratings are not intended as indicators of medical quality — how good a doctor is — but rather reflect patients\' feedback on factors such as good communication, clarity of instructions, etc. Patient experience ratings are issued in the context of a specific physician\'s specialty, as patients tend to rate types of doctors differently', NULL, '1', '32$', '1,37', '2018-10-19 12:15:59', '2018-10-19 12:15:59', '2018-10-19 12:15:59'),
(19, 5, 10, 'Dr. Subha Aahlad', '9BCdVkrhSyBQs7t6GH9nLVWVenZy8Lv41LbVgDGP.jpeg', 23, 'Dr. Subha Aahlad is a pediatrician in Foster City, California and is affiliated with Mills-Peninsula Health Services-Burlingame. She received her medical degree from Sri Venkatesvara Medical College NTR and has been in practice for more than 20 years. Dr. Aahlad accepts several types of health insurance, listed below. She is one of 70 doctors at Mills-Peninsula Health Services-Burlingame who specialize in Pediatrics.', '1295 E Hillsdale Blvd', 'Foster City, CA 94404', 'Foster City', 'CA', 'USA', NULL, NULL, '(650) 574-2774', NULL, '(650) 574-2774', 'SubhaAahlad@doctor.com', 'When choosing a doctor, it can be helpful to review other patients\' experiences. U.S. News publishes patient experience ratings from Binary Fountain, which aggregates patient reviews from over a hundred sites to compile information about 10 different patient experience metrics. These ratings are not intended as indicators of medical quality — how good a doctor is — but rather reflect patients\' feedback on factors such as good communication, clarity of instructions, etc. Patient experience ratings are issued in the context of a specific physician\'s specialty, as patients tend to rate types of doctors differently.', NULL, 'Mills-Peninsula Health Services-Burlingame\r\nBurlingame, CA\r\nMills-Peninsula Health Services-Burlingame in Burlingame, CA is not nationally ranked in any specialty. more', NULL, '38,51', '42$', '37', '2018-10-19 11:38:50', '2018-10-19 11:38:50', '2018-10-19 11:38:50');

-- --------------------------------------------------------

--
-- Table structure for table `gw_hospitals`
--

CREATE TABLE `gw_hospitals` (
  `hos_id` int(11) NOT NULL,
  `hos_name` varchar(250) NOT NULL,
  `hos_since` int(4) NOT NULL,
  `hos_brief` longtext NOT NULL,
  `hos_add_1` varchar(250) DEFAULT NULL,
  `hos_add_2` varchar(250) DEFAULT NULL,
  `hos_city` varchar(250) DEFAULT NULL,
  `hos_state` varchar(250) DEFAULT NULL,
  `hos_country` varchar(250) DEFAULT NULL,
  `hos_long` varchar(250) DEFAULT NULL,
  `hos_lat` varchar(250) DEFAULT NULL,
  `hos_phone` varchar(250) DEFAULT NULL,
  `hos_mobile` varchar(250) DEFAULT NULL,
  `hos_fax` varchar(250) DEFAULT NULL,
  `hos_email` varchar(250) DEFAULT NULL,
  `hos_workhour` varchar(250) DEFAULT NULL,
  `hos_website` varchar(250) DEFAULT NULL,
  `hos_specia_tags` varchar(250) DEFAULT NULL,
  `hos_gallery_img` varchar(1000) DEFAULT NULL,
  `hos_doc_count` int(11) DEFAULT NULL,
  `hos_nurses_count` int(11) DEFAULT NULL,
  `hos_room_count` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gw_hospitals`
--

INSERT INTO `gw_hospitals` (`hos_id`, `hos_name`, `hos_since`, `hos_brief`, `hos_add_1`, `hos_add_2`, `hos_city`, `hos_state`, `hos_country`, `hos_long`, `hos_lat`, `hos_phone`, `hos_mobile`, `hos_fax`, `hos_email`, `hos_workhour`, `hos_website`, `hos_specia_tags`, `hos_gallery_img`, `hos_doc_count`, `hos_nurses_count`, `hos_room_count`) VALUES
(1, 'Hotel Dieu de France', 1888, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum is simply dummy text of the printing and typesetting industryLorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum is simply dummy text of the printing and typesetting industry', 'Kaslic ,Lebanon', 'Near Burger King', 'Beirut', 'LB', 'Lebanon', NULL, NULL, '961 (1) 537 892', NULL, NULL, NULL, '7AM till 8PM', 'www.hdf.edu.lb', '1,2,3,4,5,6', 'ftZq6r6aP21mBxZ8EJbSCnwOhXYgaPpja6hjLfhh.png,cTo1D6E1hVce8tG3fRy1dfKxkLX2dpeUiENR2Mrc.png,kEjUlaIvvFwoqep8PSi4ryxnUUWJ2C9U7cTvStPY.png', 362, 432, 1000),
(37, 'Southwestern Medical', 1997, 'UT Southwestern Medical Center in Dallas, Texas is nationally ranked in 7 adult specialties specialties and rated high performing in 4 adult specialties and 5 procedures and conditions. It is a general medical and surgical facility. It is a teaching hospital.', '5323 Harry Hines Boulevard  Dallas', 'TX 75390-9265', 'Dallas', 'TX', 'USA', NULL, NULL, '(214) 645–8300', NULL, NULL, NULL, '10AM till7PM', 'www.utsouthwestern.edu', NULL, '6PCdLwCMpRH6t3dNqeyTYkFuSgXIaoUvO9iUU406.jpeg', 772, 1694, 1579),
(38, 'Tufts Medical Center', 1931, 'Tufts Medical Center in Boston, Mass. is rated high performing in 2 adult specialties and 3 procedures and conditions. It is a general medical and surgical facility. It is a teaching hospital.', 'Boston', 'MA 02111-1552', 'Boston', 'MA', 'USA', NULL, NULL, '(617) 636–5000', NULL, NULL, NULL, '5AM till2PM', 'www.tuftsmedicalcenter.org', '1,2,4,5,7', 'dLZ8q01nAud0ySj4FCTXSLuyHwF6QUi5AlHRE6x6.jpeg,TgLmpTmyqej91hzrh3vwDQgdMg5jMElTofF8OfM2.jpeg,PQ02pLkSVul0UV2nxXLFwb18LK1tzBGWOta9hBJK.jpeg', 742, 1301, 1890),
(39, 'Henry Ford Hospital', 1986, 'Henry Ford Hospital in Detroit, Mich. is nationally ranked in 1 adult specialty specialty and rated high performing in 5 adult specialties and 3 procedures and conditions. It is a general medical and surgical facility. It is a teaching hospital.', 'Detroit', 'MI 48202-2608', 'Detroit', 'MI', 'USA', NULL, NULL, '(313) 916–2600', NULL, NULL, NULL, '5AM till9PM', 'henryford.com', NULL, 'h6un7YBEFNbspaGB1XzBmve5QxfazXD12zZ3Jco2.jpeg', 1369, 1557, 1280),
(40, 'University of Michigan', 1932, 'University of Michigan Hospitals-Michigan Medicine in Ann Arbor, Mich. is ranked No. 5 on the Best Hospitals Honor Roll. It is nationally ranked in 14 adult and 10 pediatric specialties and rated high performing in 1 adult specialty and 9 procedures and conditions. It is a general medical and surgical facility. It scored high in patient safety, demonstrating commitment to reducing accidents and medical mistakes. It is a teaching hospital. The University of Michigan’s hospital opened in 1869 – 19 years after the university’s medical school – as the first university-owned medical facility in the country. The University of Michigan Medical Center complex in Ann Arbor is made up of eight buildings, including the University of Michigan C.S. Mott Children’s Hospital, Frankel Cardiovascular Center and Rogel Cancer Center. Beyond the main campus at the Medical Center, Michigan Medicine has 25 health centers in Ann Arbor and the region. At Rogel Cancer Center, the Patient and Family Support Services Program recognizes the needs of cancer patients go beyond just medical treatment. U-M’s Kellogg Eye Center is among the few sites in the country to perform a type of nerve transplant surgery to restore feeling to the cornea, a procedure that can help prevent blindness. The health system operates the Cancer AnswerLine, where callers can receive information about any aspect of cancer and be provided with physician referrals. At the University of Michigan C.S. Mott Children’s Hospital, two full-time service dogs are on duty to help calm and comfort patients. The main Medical Center campus includes the Med Inn, a 30-room hotel for use by surgical and intensive care patients and families or others with special needs. U-M has been the site of many medical firsts, such as the first dermatology department in the U.S., American introduction of the electrocardiogram, the country’s first human genetics program and the first comprehensive depression center in the U.S.', 'Ann Arbor', 'MI 48109', 'Ann Arbor', 'MI', 'USA', NULL, NULL, '06 575 544', NULL, NULL, NULL, '10AM till 7PM', 'PKFnJbkWpn.edu.lb', '1,4,6,8', 'uGeGYddkdDA7JhQ2zBmBbEswFJJwjOLLJmYVdudJ.jpeg,OR9ddCHG2mbqrv7zCSuiGrxC1CiIOARXTq1Xi1dk.jpeg,DDkoa7G4sTITpAaouUGi2KxrISf2QSAY7etQ4M4R.jpeg', 429, 479, 532),
(41, 'Mayo Clinic', 1947, 'Mayo Clinic in Rochester, Minn. is ranked No. 1 on the Best Hospitals Honor Roll. It is nationally ranked in 15 adult and 7 pediatric specialties and rated high performing in 1 adult specialty and 9 procedures and conditions. It is a general medical and surgical facility. What became the Mayo Clinic was founded in 1889 in Rochester, Minnesota. The Mayo Clinic opened hospitals in Jacksonville, Florida, in 1986 and Scottsdale, Arizona, in 1987. The Mayo Clinic Health System was established in 1992 and owns 19 hospitals in Minnesota, Wisconsin and Iowa. The Mayo Clinic School of Medicine is the teaching arm of the Mayo Clinic. The hospital\'s two Rochester campuses include the Mayo Eugenio Litta Children’s Hospital. The Mayo Clinic has a large integrated transplantation program. The Department of Endocrinology, which treats diabetes, is one of the world’s largest. The hospital offers a Healthy Living Program that is described as an “immersive wellness” experience, encompassing exercise, nutrition and mind-body practices. The Mayo Clinic’s Sports Medicine facilities in Rochester and Minneapolis serve athletes – from those at the elite level to the strictly recreational – with treatment and sports training. The Mayo Clinic has 57 research centers, such as those working on Alzheimer’s disease, multiple sclerosis and kidney disease. Innovations at the Mayo Clinic include the heart-lung bypass machine and total hip replacement.', 'Rochester', 'MN 55902-1906', 'Rochester', 'MN', 'USA', NULL, NULL, '(507) 405-0312', NULL, NULL, NULL, '3AM till10PM', 'www.mayoclinic.org', '2,3,5,6,7,8', 'zxsioc6zJCssJGVfGalSkV9Npxm98eNEo5mqxHqV.jpeg,SYywtqsl7sMdcckrOmEBCKpRmfNd3Eh1u5Atx6yz.jpeg', 1619, 893, 1562),
(44, 'Duke Hospital', 1929, 'Duke University Hospital in Durham, N.C. is ranked No. 19 on the Best Hospitals Honor Roll. It is nationally ranked in 11 adult and 9 pediatric specialties and rated high performing in 1 adult specialty and 8 procedures and conditions. It is a general medical and surgical facility. It scored high in patient safety, demonstrating commitment to reducing accidents and medical mistakes. It is a teaching hospital. The original Duke University Hospital, first known as the Duke Clinic, opened in 1930 with a grant from James B. Duke. He sought to help the Carolinas by providing an academic medical center for a region that was without hospitals and health care providers at the time. Duke University Hospital is the flagship of the Duke University Health System, which encompasses 3 million square feet of hospital and academic medical space. The health system has three hospitals for inpatient services and widespread outpatient services including the Duke Primary Care network. Duke University Health System’s outpatient facilities sees more than 2 million patients per year and has more than 68,000 inpatient stays.\r\n\r\nThe Duke Diet and Fitness Center help patients with weight loss, lifestyle change and obesity treatment. Minimally invasive weight loss surgery is offered, including sleeve gastrectomy, gastric banding, VBLOC therapy (an alternative to weight loss surgery) and other methods.\r\n\r\nFor a combination of medical practices and complementary therapies, the Duke Integrative Medicine facility gives patients personalized strategies to restore and maintain health. The facility offers massage therapy, yoga therapy, acupuncture, integrative nutrition, integrative health coaching and mindfulness-based stress reduction. Additionally, Duke Homecare & Hospice offers nurse services and therapy to patients at home or in assisted-living facilities.\r\n\r\nThe research arm of the Duke University School of Medicine is the Duke Clinical Research Institute. It conducts clinical trials, health services research and provides educational training. Some health services research areas include drug and device safety, medical device making, quality improvement and patient-reported outcomes.\r\n\r\nDuke University Hospital also provides classes and events to patients and loved ones. Classes include tai chi, open meditation, preparing for labor, chemotherapy education class, art therapy support group and diabetes education class.', 'Los Angeles', 'CA 90095-8358', 'Los Angeles,', 'CA', 'USA', NULL, NULL, '06 973 986', NULL, NULL, NULL, '7AM till 9PM', 'ohBCCJWwqA.edu.lb', '1,2', 'CesZzXHxAnvpLrxi12vi7Q96kHmqYWtQuIrD2vDf.jpeg,IdMd0QyszdwAGFql1CQfiDY0L8SeHgiO1FnhE1FT.jpeg,WwsuYkymxmyFs0px8FZwwUvPk9Hvbk8qjbZFbV2z.jpeg', 1597, 869, 721),
(45, 'Siteman Center', 1994, 'Barnes-Jewish Hospital in Saint Louis, Mo. is ranked No. 11 on the Best Hospitals Honor Roll. It is nationally ranked in 12 adult specialties and rated high performing in 2 adult specialties and 8 procedures and conditions. It is a general medical and surgical facility. It is a teaching hospital. Barnes-Jewish Hospital/Washington University, St. Louis was established when Barnes Hospital and The Jewish Hospital of St. Louis merged in 1996. Barnes-Jewish Hospital serves as a teaching hospital for Washington University School of Medicine in St. Louis. Its medical services include cancer care, dermatology, digestive health, and trauma and acute care surgery. \r\n\r\nThe Barnes-Jewish & Washington University Spine Center is one of the largest clinical spine care practices in the country. And the Alvin J. Siteman Cancer Center, the only National Cancer Institute-designated comprehensive cancer center within 240 miles of St. Louis, collaborates with other leading cancer centers to ensure its patients have access to the newest clinical trials. In addition to its main campus at Barnes-Jewish Hospital, the center provides services at four satellite locations in the area: Barnes-Jewish West County Hospital, Barnes-Jewish St. Peters Hospital, Christian Hospital and a facility in south St. Louis County.\r\n\r\nAfter performing its first kidney transplant in 1963, the hospital now conducts about 200 kidney transplants each year. At the Fertility & Reproductive Medicine Center, physicians treat infertility using in vitro fertilization as well as IVF alternatives. The hospital also offers bariatric surgery and minimally invasive procedures for patients who need to lose weight. Sports medicine specialists in the hospital’s orthopedics department provide care for St. Louis’ NHL team, the Blues. \r\n\r\nAmong the innovations at Barnes-Jewish Hospital/Washington University are the development of the sequential bilateral lung transplant, when surgeons transplant each lung separately, and the mini-nephrectomy procedure, a type of surgery to remove a donor’s kidney.', 'Saint Louis', 'MO 63110-1003', 'Saint Louis', 'MO', 'USA', NULL, NULL, '(314) 747–3000', NULL, NULL, NULL, '5AM till11PM', 'www.barnesjewish.org', '1,2,3,4,5,6,7,8', 'AXt2QegynX00QKtthg7gmheIFGmVAwtEBt21QJl5.jpeg,DLQN2lthNsVVEEjdlUfdziCNUsmlYMp32trFmC31.jpeg,3NPvddSSyahum1ax3XWd5kJJtWJPcwZ9wM8Da1QS.jpeg', 1488, 1280, 268),
(46, 'Allegiance Health', 1966, 'Hospital Services: Cardiology, Diet/Nutrition, Emergency Medicine, Family or General Practice, Gastroenterology, Hematology and Oncology, Infectious Disease, Internist or Critical Care, Medical Management, Nephrology, Neurology, Nursing, Obstetrics and Gynecology, Orthopedics, Pain Management, Pediatrics, Physical Medicine and Rehabilitation, Physical/Occupat Therapy, Practice Management, Psychiatry, Psychology, Radiology, Rheumatology, Surgery', '205 North East Avenue Jackson', 'MI 49201', 'Jackson', 'MI', 'USA', NULL, NULL, '(517)788-4800', NULL, NULL, NULL, '1AM till11PM', 'www.allegiancehealth.org', '2,3,4', NULL, 1875, 1908, 1200),
(47, 'NY Eye & Ear', 1923, 'To help patients decide where to receive care, U.S. News generates hospital rankings by evaluating data on nearly 5,000 hospitals in 16 adult specialties, 9 adult procedures and conditions and 10 pediatric specialties. To be nationally ranked in a specialty, a hospital must excel in caring for the sickest, most medically complex patients. The ratings in procedures and conditions, by contrast, focus on typical Medicare patients. Hospitals that do well in multiple areas of adult care may be ranked in their state and metropolitan area. New York Eye and Ear Infirmary of Mount Sinai is ranked nationally in 2 adult specialties . Read more about how we rank best hospitals.', 'New York', 'NY 10003-4201', 'New York', 'NY', 'USA', NULL, NULL, '877-828-1089', NULL, NULL, NULL, '4AM till10PM', 'www.nyee.edu', '1,2,3,5,6,7', 'FKvyjiXMsoXOMyQTiZAiovSiUSaon77jeUMznuGD.png,1ngnf5Oo2IdcbZKERl1QOwmnB0lhXa2HmN8DpHzW.png', 1756, 1503, 282),
(48, 'Aultman Hospital', 1952, 'Hospital Services: Cardiology, Dentistry, Diet/Nutrition, Emergency Medicine, Family or General Practice, Gastroenterology, Hematology and Oncology, Infectious Disease, Internist or Critical Care, Medical Genetics, Medical Management, Nephrology, Neurology, Nursing, Obstetrics and Gynecology, Orthopedics, Pain Management, Pediatrics, Phlebology, Physical Medicine and Rehabilitation, Physical/Occupat Therapy, Practice Management, Psychiatry, Psychology, Radiology, Surgery, Urology', '2600 Sixth Street Sw Canton', 'OH 44710', 'Canton', 'OH', 'USA', NULL, NULL, '(330)452-9911', NULL, NULL, NULL, '11AM till1PM', 'www.aultman.com', NULL, NULL, 515, 763, 1656),
(50, 'Colorado Hospital', 1961, 'University of Colorado Hospital in Aurora, Colo. is nationally ranked in 11 adult specialties specialties and rated high performing in 1 adult specialty and 6 procedures and conditions. It is a general medical and surgical facility. It is a teaching hospital. The University of Colorado Hospital providers are affiliated with or are faculty at the University of Colorado School of Medicine. The hospital is made up of two inpatient pavilions, an outpatient Pavilion, the Anschutz Cancer Pavilion and Rocky Mountain Lions Eye Institute.\r\n\r\nThe hospital’s Center for Dependency, Addiction and Rehabilitation treats addiction and other coexisting disorders. Medically managed detoxification and stabilization, intensive outpatient treatment, addiction psychiatry, integrated addiction medicine, and day treatment are some of the services the center offers. The intensive outpatient program involves group therapy, skill building and education. Patients have the opportunity to graduate from this program.\r\n\r\nThe University of Colorado Hospital is the major academic partner of the University of Colorado Cancer Center, which offers second opinion consultations in specialities like thoracic oncology, gastrointestinal oncology, breast cancer and sarcoma. In addition, the oncology rehabilitation program helps treat cancer-related side effects. The rehabilitation team is made up of experts, including physical therapists, occupational therapists and speech-language pathologists, who help patients with everything from balance to muscle pain to difficulty talking.\r\n\r\nThe level 3 neonatal intensive care unit is the largest of its kind in metro Denver. The NCIU is open 24/7 with specialists including neonatologists, neonatal nurse practitioners, respiratory therapists, developmental therapists, lactation specialists and social workers.\r\n\r\nUCHealth also works with sports teams in the area as official health care partners, including the Denver Broncos football team, Colorado Rockies baseball team, Colorado Avalanche hockey team and Colorado Mammoth lacrosse team. Because of UCHealth, Denver has lactation suites in all of its downtown professional sports stadiums.', 'Aurora', 'CO 80045-2545', 'Aurora', 'CO', 'USA', NULL, NULL, '(720) 848–0000', NULL, NULL, NULL, '4AM till11PM', 'www.uchealth.org', '1,2,3,4', 'lhdRB86z3b5Szqplpnkky1XQRUGCFudElLo5jJkJ.jpeg,nLeuECdAOVWC6If5GD7XD3ZtMvWYEbIAJ4Gba3AS.jpeg,W3vIrrlLOsYf6VMz3KLSy62Ev5FOkCWYQBkEmM4B.jpeg', 866, 270, 718),
(51, 'WILLS EYE HOSPITAL', 1986, 'Wills Eye Hospital, a specialty hospital of Thomas Jefferson University Hospitals, in Philadelphia is nationally ranked in ophthalmology. It specializes in cataract and primary eye care services, as well as contact lens, cornea, glaucoma, neuro-ophthalmology, oculo-pathology services and more.', 'Philadelphia', 'PA', 'Philadelphia', 'PA', 'USA', NULL, NULL, '04 512 711', NULL, NULL, NULL, '8AM till4PM', 'vmgkD2Q9Lg.edu.lb', '1,4,6,7', 'Jf8yxx9po6xiRNA0QgdczW7KpRfeBZzI1pgLlmMP.jpeg', 119, 811, 1666),
(52, 'ACMH Hospital', 1931, 'Rankings and Recognitions\r\nTo help patients decide where to receive care, U.S. News generates hospital rankings by evaluating data on nearly 5,000 hospitals in 16 adult specialties, 9 adult procedures and conditions and 10 pediatric specialties. To be nationally ranked in a specialty, a hospital must excel in caring for the sickest, most medically complex patients. The ratings in procedures and conditions, by contrast, focus on typical Medicare patients. Hospitals that do well in multiple areas of adult care may be ranked in their state and metropolitan area. Read more about how we rank best hospitals.', '1295 E Hillsdale Blvd', 'Foster City, CA 94404', 'Foster City', 'CA', 'USA', NULL, NULL, '05 508 325', NULL, NULL, NULL, '11AM till5PM', 'ACMH.com', '1,2,3,4', 'MDQHEUDKE5c2FgGw1dZLVx9rqJKbRQZH7eZK5ias.png,bz1GcraCRXYwilYQ9bLC9gJsyJViFvQr3AOCzIbf.png', 416, 856, 1880),
(53, 'Miami Hospital', 1995, 'University of Miami Hospital and Clinics-Bascom Palmer Eye Insititute in Miami, Fla. is nationally ranked in 1 adult specialty specialty. It is an eye, ear, nose and throat facility.', 'Miami', 'FL 33136-1199', 'Miami', 'FL', 'USA', NULL, NULL, '01 489 376', NULL, NULL, NULL, '4AM till1PM', '3gUeb4UCCz.edu.lb', '1,2,3,8', '6bEnw2Olop94muhybkFjWlPY2EhS7oH76QeqJFsu.jpeg', 632, 978, 1591),
(56, 'Notre Dame Hospital', 1930, 'Complete Technologies Medical Hospital to fit up to date Humane Health Care with concern', 'Fouad Chehab', 'Jounieh', 'Jounieh', NULL, 'Lebanon', NULL, NULL, '09 644 644', NULL, NULL, NULL, 'Open 24 Hours', NULL, '1,2,3,4', NULL, 70, 50, 90),
(57, 'Bellevue Medical', 2009, 'Bellevue Medical Center founded in July 2009, is a general hospital located in Mansourieh, Lebanon. It offers healthcare services in many specialties. The hospital has a capacity of 130 beds within its eight floors.', 'Mansourieh', NULL, 'Mansourieh', NULL, 'Lebanon', NULL, NULL, '01 682 666', NULL, NULL, NULL, 'Open 24 Hours', NULL, '2,3,4,6,7', NULL, 200, 100, 130),
(58, 'Hospital Geitaoui', 1980, 'While it does have some of the big medical names on its roster, the nursing staff is egregiously under qualified. They exhibit a nonchalant attitude towards their job and their patients’ welfare. Not a shred of professionalism in sight, neglecting their duties even in the ICU ward. We had to repeatedly ask for the most basic of care. When faced with a seizing patient, they stood there doing nothing. The attitude toward us was very hostile, even if we made a simple request such as to humidify the patient’s mouth. They are short on supplies (we were asked to get our own hydrating cream so they could use it on the patient). The doctors are seldom present or willing to give a meaningful update on the patient’s condition, instead dishing out the standard patronizing catch-all phrase: “kattir kheir Allah kello mni7”. Getting test results is done the old fashioned way: you need to chase the technician across floors and departments to piece together your medical file (and of course said technician would never be at his post, probably opting to have a prolonged coffee).', 'Beirut', NULL, 'Beirut', NULL, 'Lebanon', NULL, NULL, '01 590 000', NULL, NULL, NULL, 'Open 24 Hours', NULL, '2,3,4', NULL, 100, 10, 90),
(59, 'St. Louis', 1980, 'From the best hospitals in the area', 'Jounieh', 'Jounieh', 'Jounieh', NULL, 'Lebanon', NULL, NULL, '09 912 790', NULL, NULL, NULL, 'Open 24 Hours', NULL, NULL, NULL, 100, 80, 130);

-- --------------------------------------------------------

--
-- Table structure for table `gw_hotels`
--

CREATE TABLE `gw_hotels` (
  `hot_id` int(11) NOT NULL,
  `hot_name` varchar(250) NOT NULL,
  `hot_type_id` int(11) DEFAULT NULL,
  `hot_add_1` varchar(250) DEFAULT NULL,
  `hot_add_2` varchar(250) DEFAULT NULL,
  `hot_city` varchar(250) DEFAULT NULL,
  `hot_country` varchar(250) DEFAULT NULL,
  `hot_long` varchar(250) DEFAULT NULL,
  `hot_lat` varchar(250) DEFAULT NULL,
  `hot_phone` varchar(250) DEFAULT NULL,
  `hot_mobile` varchar(250) DEFAULT NULL,
  `hot_fax` varchar(250) DEFAULT NULL,
  `hot_email` varchar(250) DEFAULT NULL,
  `hot_website` varchar(250) DEFAULT NULL,
  `hot_brief` longtext,
  `hot_img` varchar(250) DEFAULT NULL,
  `hot_desc` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gw_hotels`
--

INSERT INTO `gw_hotels` (`hot_id`, `hot_name`, `hot_type_id`, `hot_add_1`, `hot_add_2`, `hot_city`, `hot_country`, `hot_long`, `hot_lat`, `hot_phone`, `hot_mobile`, `hot_fax`, `hot_email`, `hot_website`, `hot_brief`, `hot_img`, `hot_desc`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Le Royal', 1, 'Beirut-Lebanon', NULL, NULL, NULL, NULL, NULL, '961 01 537 892', NULL, '961 01 537 891', NULL, NULL, 'This is a wider card with supporting text below as a natural lead-in to additional content.', 'UHrQJ7OZLe3H9djJk4BsvtoIuDQbRTE0PHjVQSPT.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industryLorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum is simply dummy text of the printing and typesetting industry', '2018-10-12 13:06:24', '2018-10-12 10:06:24', '0000-00-00 00:00:00'),
(2, 'hot 1', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'cCH5FfbYN1nddshMSKb8uvifywNFk1DP0XtvhSLp.png', NULL, '2018-10-12 07:02:48', '2018-10-12 04:02:48', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `gw_message`
--

CREATE TABLE `gw_message` (
  `msg_id` int(11) NOT NULL,
  `msg_user_id` int(11) NOT NULL,
  `msg_desc` longtext NOT NULL,
  `msg_attach_tags` varchar(250) NOT NULL,
  `msg_attach_name` varchar(250) NOT NULL,
  `msg_datetime` datetime NOT NULL,
  `msg_conv_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gw_message`
--

INSERT INTO `gw_message` (`msg_id`, `msg_user_id`, `msg_desc`, `msg_attach_tags`, `msg_attach_name`, `msg_datetime`, `msg_conv_id`) VALUES
(258, 4, 'hi', '', '', '2018-10-30 12:01:37', 195),
(259, 12, 'hello', '', '', '2018-10-30 12:01:45', 196),
(260, 1, 'hi', '', '', '2018-10-30 12:04:37', 199),
(261, 1, 'hello', '', '', '2018-10-30 12:04:52', 199),
(262, 1, 'heoo', '', '', '2018-10-30 12:05:46', 198),
(263, 4, 'f', '', '', '2018-10-30 12:09:47', 200),
(264, 1, '', '', '', '2018-10-30 12:09:55', 201),
(265, 12, '', '', '', '2018-10-30 12:10:01', 202);

-- --------------------------------------------------------

--
-- Table structure for table `gw_patient`
--

CREATE TABLE `gw_patient` (
  `pat_id` int(11) NOT NULL,
  `pat_user_id` int(11) NOT NULL,
  `pat_fullname` varchar(250) NOT NULL,
  `pat_img` varchar(250) DEFAULT NULL,
  `pat_bithday` date DEFAULT NULL,
  `pat_country` varchar(250) DEFAULT NULL,
  `pat_city` varchar(250) DEFAULT NULL,
  `pat_phone` varchar(250) DEFAULT NULL,
  `pat_email` varchar(250) DEFAULT NULL,
  `pat_mobile` varchar(250) DEFAULT NULL,
  `pat_fax` varchar(250) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gw_patient`
--

INSERT INTO `gw_patient` (`pat_id`, `pat_user_id`, `pat_fullname`, `pat_img`, `pat_bithday`, `pat_country`, `pat_city`, `pat_phone`, `pat_email`, `pat_mobile`, `pat_fax`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 4, 'Georges', 'ukF6DfrPXUg8UMIjiCVMm4mfGEipYtOhB07MFqA5.jpeg', NULL, 'leb', 'jounieh', NULL, NULL, NULL, NULL, '2018-10-29 09:33:50', NULL, NULL),
(3, 5, 'Hajal', 'urRNmuxsDvhb9oTPSN1qpTu8jTq4N0K2HoGDge1U.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-19 06:30:05', NULL, NULL),
(5, 12, 'patient', 'o9d3dnwlv6UL6jyos7el4vfCnjASX5tHxGaIo92C.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-29 12:09:40', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gw_speciality`
--

CREATE TABLE `gw_speciality` (
  `spec_id` int(11) NOT NULL,
  `spec_name` varchar(250) NOT NULL,
  `spec_img` varchar(250) NOT NULL,
  `spec_desc` varchar(250) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gw_speciality`
--

INSERT INTO `gw_speciality` (`spec_id`, `spec_name`, `spec_img`, `spec_desc`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Neurlogy', '8purxhQhUyHPt0YuqYlN9n3UBT2UisiH5IC7oECP.png', NULL, '2018-10-16 10:20:28', '2018-10-16 07:20:28', '0000-00-00 00:00:00'),
(2, 'Cardiology', 'geySZZ7kmqvvqnL3CQfbRgm4tysrI1rqpUlvgyzK.png', NULL, '2018-10-16 10:20:53', '2018-10-16 07:20:53', '0000-00-00 00:00:00'),
(3, 'Physiology', 'fTlfp7qH472QWEljlOIzbgrq2a0LLty0S5avaLb1.png', NULL, '2018-10-16 10:20:07', '2018-10-16 07:20:07', '0000-00-00 00:00:00'),
(4, 'Dentist', '3XGMQt7SSQnOt0Si1ttkmfJwatGaWhAPZTrxjGpm.png', NULL, '2018-10-16 10:20:48', '2018-10-16 07:20:48', '0000-00-00 00:00:00'),
(5, 'Pediatrics', 'uAG2lElnTUiojnSxit8b7hXrynFCKfV1vTzDqnlm.png', NULL, '2018-10-16 10:20:17', '2018-10-16 07:20:17', '0000-00-00 00:00:00'),
(6, 'Psychology', 'KniG7whpSjWMLTdRUkSMWsD9cZrfVdgBgVUFfmw2.png', NULL, '2018-10-16 10:19:39', '2018-10-16 07:19:39', '0000-00-00 00:00:00'),
(7, 'Gynecology', 'g9pnfNdvIRGL7luNyvCj94n317sBWfArH1EUxC1V.png', NULL, '2018-10-16 10:20:40', '2018-10-16 07:20:40', '0000-00-00 00:00:00'),
(8, 'Pneumonology', 'amZEbn6JzfczMDMUSDPytiYWjqR1zwibNky9GSwH.png', NULL, '2018-10-16 10:19:54', '2018-10-16 07:19:54', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `gw_static`
--

CREATE TABLE `gw_static` (
  `st_id` int(11) NOT NULL,
  `st_type` varchar(250) NOT NULL,
  `st_desc` longtext NOT NULL,
  `st_img` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gw_type`
--

CREATE TABLE `gw_type` (
  `type_id` int(11) NOT NULL,
  `type_name` varchar(250) NOT NULL,
  `type_img` varchar(250) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gw_type`
--

INSERT INTO `gw_type` (`type_id`, `type_name`, `type_img`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Hotel and Restaurant', '', '2018-10-11 11:59:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Motels', 'zkj9qudsh9Z1eB3VZ4spDt4xn5tX4CAs1rtIxamt.jpeg', '2018-10-12 09:25:16', '2018-10-12 06:25:16', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `gw_usertype`
--

CREATE TABLE `gw_usertype` (
  `ut_id` int(11) NOT NULL,
  `ut_name` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gw_usertype`
--

INSERT INTO `gw_usertype` (`ut_id`, `ut_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Patient', '2018-10-12 09:35:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Doctor', '2018-10-12 09:35:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_09_19_130927_create_posts_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usertypeid` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `usertypeid`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'dr enzo', 'georgesdagher97@hotmail.com', '$2y$10$ff/DAezrL41RXInJn3YS..lcD0brl6klXbZHx76FOAgFmDnQRaW3y', 2, 'ksRuAHHQf2ZA042Ymj09n6gSNj8pjJoNH37uCufAsug5A7Xvo2uM5H7MeUPB', '2018-09-25 03:22:48', '2018-09-25 03:22:48'),
(4, 'georges', 'georgesdagher98@hotmail.com', '$2y$10$GfZ2W1a.81.Huz8VrQZ0eeB3ov1R715egw0y6Ivl56Qyyg0bM5ZMS', 1, '5pE28UWjIEbCWMjDpzb38EVe8HUIXAYhiI5R1D0YWO7sBINKP51f29ywOnA0', '2018-09-24 04:22:07', '2018-10-30 06:47:14'),
(5, 'hajal', 'georgesdagher99@hotmail.com', '$2y$10$uXE3N7lN1q7VvI6TBrJQruMjAHVlbwQcSXEUIN6t/hudJ2rxQUhGW', 1, 'qy4RSzFEeTpYeaoByldNUyCMke95g7lE1DwLw84zowqmfoMjAPmxm5jI7h26', '2018-09-24 08:11:12', '2018-09-24 08:11:12'),
(7, 'Dr. Garry Peers', 'georgesdagher96@hotmail.com', '$2y$10$MCWj7quZJ4zIBhKtnU2pSOLzLS3bVNR8zxPqd/A2.E03RpBgr3iWi', 2, '9cAGHSY5CEjMlXIAUROfBjGPspvse9SOimeeIefOy8Ut3ji2LnFEFAHNGceg', '2018-09-25 03:53:09', '2018-10-29 07:46:29'),
(8, 'Dr. Randal Aaberg', 'georgesdagher95@hotmail.com', '$2y$10$MCWj7quZJ4zIBhKtnU2pSOLzLS3bVNR8zxPqd/A2.E03RpBgr3iWi', 2, 'JkPqVGi0YRMLxSgkWMIB0SEAe7bY02W9FDrsOMn2yc4o9TFHrmAtUbWjEA8f', '2018-09-25 04:50:12', '2018-10-16 07:14:37'),
(9, 'Dr. Tahmina Aafreen', 'TahminaAafreen@hotmail.com', '$2y$10$MCWj7quZJ4zIBhKtnU2pSOLzLS3bVNR8zxPqd/A2.E03RpBgr3iWi', 2, NULL, '2018-10-18 06:26:03', '2018-10-18 06:26:03'),
(10, 'Dr. Subha Aahlad', 'subha.aahlad@hotmail.com', '$2y$10$MCWj7quZJ4zIBhKtnU2pSOLzLS3bVNR8zxPqd/A2.E03RpBgr3iWi', 2, 'WopFEnJNcJvdrEIPDIp4olcOCK8tHI9ENJITdjnayiv09KnEWVQHJ859gyhR', '2018-10-19 08:38:36', '2018-10-26 08:20:38'),
(11, 'joe', 'georgesdagher94@hotmail.com', '$2y$10$359zPw7cJc3yS6VK4yNo9.VFYwkHK.1xFjWotu6Odf8HeGOoCNIxm', 2, 'DTNwPbw9i5xPSYwMDu6GRygNum1KZPWGQztslPK3ArPMQHm0GHUy68iknZcw', '2018-10-29 07:19:17', '2018-10-29 07:19:17'),
(12, 'patient', 'patient@hotmail.com', '$2y$10$Zr9Sxn13SpJzonVQyHKQiuZ7ccD8Pcc0xxVizKMq3V3cuzta11yRK', 1, 'sTlE4Y9us9aBzBu6nvID7d1QwBhwcLDFGPztBp1lHO3Mn24ZsVlSvdygJAUx', '2018-10-29 08:19:39', '2018-10-29 10:13:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gw_conversation`
--
ALTER TABLE `gw_conversation`
  ADD PRIMARY KEY (`conv_id`);

--
-- Indexes for table `gw_conv_users`
--
ALTER TABLE `gw_conv_users`
  ADD PRIMARY KEY (`cu_id`);

--
-- Indexes for table `gw_doctors`
--
ALTER TABLE `gw_doctors`
  ADD PRIMARY KEY (`doc_id`);

--
-- Indexes for table `gw_hospitals`
--
ALTER TABLE `gw_hospitals`
  ADD PRIMARY KEY (`hos_id`);

--
-- Indexes for table `gw_hotels`
--
ALTER TABLE `gw_hotels`
  ADD PRIMARY KEY (`hot_id`);

--
-- Indexes for table `gw_message`
--
ALTER TABLE `gw_message`
  ADD PRIMARY KEY (`msg_id`);

--
-- Indexes for table `gw_patient`
--
ALTER TABLE `gw_patient`
  ADD PRIMARY KEY (`pat_id`);

--
-- Indexes for table `gw_speciality`
--
ALTER TABLE `gw_speciality`
  ADD PRIMARY KEY (`spec_id`);

--
-- Indexes for table `gw_static`
--
ALTER TABLE `gw_static`
  ADD PRIMARY KEY (`st_id`);

--
-- Indexes for table `gw_type`
--
ALTER TABLE `gw_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `gw_usertype`
--
ALTER TABLE `gw_usertype`
  ADD PRIMARY KEY (`ut_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gw_conversation`
--
ALTER TABLE `gw_conversation`
  MODIFY `conv_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=203;

--
-- AUTO_INCREMENT for table `gw_conv_users`
--
ALTER TABLE `gw_conv_users`
  MODIFY `cu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=348;

--
-- AUTO_INCREMENT for table `gw_doctors`
--
ALTER TABLE `gw_doctors`
  MODIFY `doc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `gw_hospitals`
--
ALTER TABLE `gw_hospitals`
  MODIFY `hos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `gw_hotels`
--
ALTER TABLE `gw_hotels`
  MODIFY `hot_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `gw_message`
--
ALTER TABLE `gw_message`
  MODIFY `msg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=266;

--
-- AUTO_INCREMENT for table `gw_patient`
--
ALTER TABLE `gw_patient`
  MODIFY `pat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `gw_speciality`
--
ALTER TABLE `gw_speciality`
  MODIFY `spec_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `gw_type`
--
ALTER TABLE `gw_type`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `gw_usertype`
--
ALTER TABLE `gw_usertype`
  MODIFY `ut_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
